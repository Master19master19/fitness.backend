@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Добавить корт</h1>
        <form class="form" method="POST" action="{{ route( 'courts.store' ) }}">
            @csrf
            <div class="form-group">
                <label>Название <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="100" required="" name="title" autofocus="" class="form-control" value="{{ old( 'title' ) }}" />
            </div>
            <div class="form-group">
                <label>Арена <span class="text-danger">*</span></label>
                <select required="" class="form-control" name="arena_id">
                    @foreach ( $arenas as $arena )
                        <option value="{{ $arena -> id }}">{{ $arena -> title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block"><i class="fa fa-save"></i></button>
            </div>
        </form>
    </div>
@endsection