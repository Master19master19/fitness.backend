@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>
                    Время:
                </th>
                @foreach ( $arenas as $arena )
                <th colspan="{{ count( $arena -> courts ) * 2 }}">
                    {{ $arena -> title }}
                </th>
                @endforeach
            </tr>
            <tr>
                <th>
                </th>
                @foreach ( $arenas as $arena )
                    @foreach ( $arena -> courts as $court )
                        <th colspan="2">
                            {{ $court -> title }}
                        </th>
                    @endforeach
                @endforeach
            </tr>
            <tr>
                <th>
                </th>
                @foreach ( $arenas as $arena )
                    @foreach ( $arena -> courts as $court )
                        <th>
                            Будни руб./час
                        </th>
                        <th>
                            Выходные
                        </th>
                    @endforeach
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach ( $times as $time )
                <tr>
                    <th>
                        {{ $time -> title }}
                    </th>
                    @foreach ( $arenas as $arena )
                    @foreach ( $arena -> courts as $court )
                        <td>
                            <input type="number" min="0" value="{{ ( isset ( $seats[ $arena -> id ][ $court -> id ] ) && isset( $seats[ $arena -> id ][ $court -> id ][ $time -> title ] ) ) ? $seats[ $arena -> id ][ $court -> id ][ $time -> title ][ 0 ] : '' }}" name="{{ $arena -> id }}-week-{{ $time -> title }}-{{ $court -> id }}" class="form-control" />
                        </td>
                        <td>
                            <input type="number" min="0" value="{{ ( isset ( $seats[ $arena -> id ][ $court -> id ] ) && isset( $seats[ $arena -> id ][ $court -> id ][ $time -> title ] ) ) ? $seats[ $arena -> id ][ $court -> id ][ $time -> title ][ 1 ] : '' }}" name="{{ $arena -> id }}-weekend-{{ $time -> title }}-{{ $court -> id }}" class="form-control" />
                        </td>
                    @endforeach
                    @endforeach
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection


@section ( 'scripts' )
    <script type="text/javascript">
        $( function() {
            $( 'input' ).on( 'change' , function ( e ) {
                // if ( e.which == 13 ) {
                    let val = $( this ).val();
                    let name = $( this ).attr( 'name' );
                    let argz = name.split( '-' );
                    let arena_id = argz[ 0 ];
                    let week_type = argz[ 1 ];
                    let time = argz[ 2 ];
                    let court_id = argz[ 3 ];
                    let data = { time , arena_id , court_id };
                    val = val.length > 1 ? val : 0;
                    if ( week_type == 'week' ) {
                        data[ 'price' ] = val;
                    } else{
                        data[ 'price_weekend' ] = val;
                    }
                    $.post ( '/api/editor' , data , function( res ) {
                        console.log( res );
                        toastr.success( 'Сохранено!' , val );

                    })
                    // alert( time + weekType + arenaId );
                // }
            });
            $( 'input' ).on( 'focus' , function ( e ) {
                $( this ).val( '' );
            });
        });
    </script>
@endsection