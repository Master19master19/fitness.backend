@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Добавить категорию турниров</h1>
        <form class="form" method="POST" action="{{ route( 'tourcategories.store' ) }}">
            @csrf
            <div class="form-group">
                <label>Название <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="100" required="" name="title" autofocus="" class="form-control" value="{{ old( 'title' ) }}" />
            </div>
            <div class="form-group">
                <label>Тип <span class="text-danger">*</span></label>
                <select required="" class="form-control" name="level">
                    <option value="mature" selected="">Взрослые</option>
                    <option value="immature">Дети</option>
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block"><i class="fa fa-save"></i></button>
            </div>
        </form>
    </div>
@endsection