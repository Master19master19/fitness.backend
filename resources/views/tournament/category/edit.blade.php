@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Редактировать категорию турниров</h1>
        <form class="form" method="POST" action="{{ route( 'tourcategories.update' , $tourCat -> id ) }}">
            @csrf
            @method( 'PUT' )
            <div class="form-group">
                <label>Название <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="100" required="" name="title" autofocus="" class="form-control" value="{{ $tourCat -> title }}" />
            </div>
            <div class="form-group">
                <label>Тип <span class="text-danger">*</span></label>
                <select required="" class="form-control" name="level">
                    <option value="mature" @if ( $tourCat -> level == 'mature' ) selected="" @endif>Взрослые</option>
                    <option value="immature" @if ( $tourCat -> level !== 'mature' ) selected="" @endif>Дети</option>
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block"><i class="fa fa-save"></i></button>
            </div>
        </form>
    </div>
@endsection