@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Редактировать анонс</h1>
        <form enctype="multipart/form-data" class="form" method="POST" action="{{ route( 'announcements.update' , $announcement -> id ) }}">
            @csrf
            @method( 'PUT' )
            <div class="form-group">
                <label>Название <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="100" required="" name="title" autofocus="" class="form-control" value="{{ $announcement -> title }}" />
            </div>
            <div class="form-group">
                <label>Категория <span class="text-danger">*</span></label>
                <select required="" class="form-control" name="category_id">
                    @foreach ( $tourCats as $tourCat )
                        <option value="{{ $tourCat -> id }}" @if ( $announcement -> category_id == $tourCat -> id ) selected="" @endif>{{ $tourCat -> title }} ({{ $tourCat -> getLevel }})</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Дата начала <span class="text-danger">*</span></label>
                <input type="date" required="" name="date_start" autofocus="" class="form-control" value="{{ $announcement -> date_start }}" />
            </div>
            <div class="form-group">
                <label>Дата конца <span class="text-danger">*</span></label>
                <input type="date" required="" name="date_end" autofocus="" class="form-control" value="{{ $announcement -> date_end }}" />
            </div>
            <div class="form-group">
                <label>Город <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="100" required="" name="city" class="form-control" value="{{ $announcement -> city }}" />
            </div>
            <div class="form-group">
                <label>Тип <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="100" required="" name="type" class="form-control" value="{{ $announcement -> type }}" />
            </div>
            <div class="form-group">
                <label>Покрытие корта <span class="text-danger">*</span></label>
                <input type="text" minlength="3" maxlength="100" required="" name="court_coverage" class="form-control" value="{{ $announcement -> court_coverage }}" />
            </div>
            <div class="form-group">
                <label>Официальный мяч турнира <span class="text-danger">*</span></label>
                <input type="text" minlength="3" maxlength="100" required="" name="official_ball" class="form-control" value="{{ $announcement -> official_ball }}" />
            </div>
            <div class="form-group">
                <label>Разряд <span class="text-danger">*</span></label>
                <textarea class="form-control" required="" minlength="5" maxlength="350" name="discharge">{{ $announcement -> discharge }}</textarea>
            </div>
            <div class="form-group">
                <label>Вступительный взнос <span class="text-danger">*</span></label>
                <textarea class="form-control" required="" minlength="5" maxlength="350" name="entrance_fee">{{ $announcement -> entrance_fee }}</textarea>
            </div>
            <div class="form-group">
                <label>Имя директора <span class="text-danger">*</span></label>
                <input type="text" minlength="3" maxlength="100" required="" name="director_name" class="form-control" value="{{ $announcement -> director_name }}" />
            </div>
            <div class="form-group">
                <label>Почта директора <span class="text-danger">*</span></label>
                <input type="text" minlength="3" maxlength="100" required="" name="director_email" class="form-control" value="{{ $announcement -> director_email }}" />
            </div>
            <div class="form-group">
                <label>Номер телефона директора <span class="text-danger">*</span></label>
                <input type="text" minlength="3" maxlength="100" required="" name="director_phone" class="form-control" value="{{ $announcement -> director_phone }}" />
            </div>
            <div class="form-group">
                <label>Место проведения <span class="text-danger">*</span></label>
                <textarea class="form-control" required="" minlength="5" maxlength="350" name="place">{{ $announcement -> place }}</textarea>
            </div>
            <div class="form-group">
                <label>Начало (час) <span class="text-danger">*</span></label>
                <input type="time" required="" name="start_time" class="form-control" value="{{ $announcement -> start_time }}" />
            </div>
            <div class="form-group">
                <label>Максимальное количество участников <span class="text-danger">*</span></label>
                <input type="number" min="5" max="55000" required="" name="maximum_participants" class="form-control" value="{{ $announcement -> maximum_participants }}" />
            </div>
            <div class="form-group">
                <label>Первая часть <span class="text-danger">*</span></label>
                <textarea class="form-control" required="" minlength="5" maxlength="350" name="first_part">{{ $announcement -> first_part }}</textarea>
            </div>
            <div class="form-group">
                <label>Вторая часть <span class="text-danger">*</span></label>
                <textarea class="form-control" required="" minlength="5" maxlength="350" name="second_part">{{ $announcement -> second_part }}</textarea>
            </div>
            <div class="form-group">
                <label>Призовой фонд <span class="text-danger">*</span></label>
                <textarea class="form-control" required="" minlength="5" maxlength="350" name="prize">{{ $announcement -> prize }}</textarea>
            </div>
            <img src="{{ $announcement -> img_url }}" class="mb-4 w-50" id="image-preview" />
            <div class="custom-file mb-4">
                <input id="img" accept="image/png,image/jpg,image/jpeg" type="file" class="form-control custom-file-input" name="img" id="customFile">
                <label class="custom-file-label" for="customFile">Картинка <span class="text-danger">*</span></label>
            </div>

            <div class="form-group">
                <button class="btn btn-info btn-block"><i class="fa fa-save"></i></button>
            </div>
        </form>
    </div>
@endsection