@extends( 'layouts.app' )

@section( 'content' )
<div class="container">
    <h1>Общие настройки</h1>
    <form class="form" method="POST" action="{{ route( 'app.info.post' ) }}">
        @csrf
        <div class="form-group">
            <label>Контактный телефон 1</label>
            <input type="text" name="phone_1" value="{{ isset ( $info[ 'phone_1' ] ) ? $info[ 'phone_1' ] : '' }}" autofocus="" class="form-control" />
        </div>
        <div class="form-group">
            <label>Whatsapp</label>
            <input type="text" name="whatsapp" value="{{ isset ( $info[ 'whatsapp' ] ) ? $info[ 'whatsapp' ] : '' }}" class="form-control" />
        </div>
        <div class="form-group">
            <label>Адрес</label>
            <input type="text" name="address" value="{{ isset ( $info[ 'address' ] ) ? $info[ 'address' ] : '' }}" class="form-control" />
        </div>
        <div class="form-group">
            <label>Метро</label>
            <input type="text" name="metro" value="{{ isset ( $info[ 'metro' ] ) ? $info[ 'metro' ] : '' }}" class="form-control" />
        </div>
        <div class="form-group">
            <label>Автобус</label>
            <input type="text" name="bus" value="{{ isset ( $info[ 'bus' ] ) ? $info[ 'bus' ] : '' }}" class="form-control" />
        </div>
        <div class="form-group">
            <label>E-mail</label>
            <input type="email" name="email" value="{{ isset ( $info[ 'email' ] ) ? $info[ 'email' ] : '' }}" class="form-control" />
        </div>
        <div class="form-group">
            <label>Адрес сайта</label>
            <input type="text" name="site_address" value="{{ isset ( $info[ 'site_address' ] ) ? $info[ 'site_address' ] : '' }}" class="form-control" />
        </div>
        <div class="form-group">
            <label>Facebook</label>
            <input type="text" name="fb_url" value="{{ isset ( $info[ 'fb_url' ] ) ? $info[ 'fb_url' ] : '' }}" class="form-control" />
        </div>
        <div class="form-group">
            <label>Instagram</label>
            <input type="text" name="instagram_url" value="{{ isset ( $info[ 'instagram_url' ] ) ? $info[ 'instagram_url' ] : '' }}" class="form-control" />
        </div>
        <div class="form-group">
            <label>Twitter</label>
            <input type="text" name="twitter_url" value="{{ isset ( $info[ 'twitter_url' ] ) ? $info[ 'twitter_url' ] : '' }}" class="form-control" />
        </div>
        <div class="form-group">
            <label>VK</label>
            <input type="text" name="vk_url" value="{{ isset ( $info[ 'vk_url' ] ) ? $info[ 'vk_url' ] : '' }}" class="form-control" />
        </div>
        <div class="form-group">
            <label>Почта ( почты через запятой ) для уведомлений</label>
            <input type="text" name="notification_emails" value="{{ isset ( $info[ 'notification_emails' ] ) ? $info[ 'notification_emails' ] : '' }}" class="form-control" />
        </div>
        <div class="form-group">
            <label>Аренда ракетки (руб.)</label>
            <input type="number" name="racket_price" value="{{ isset ( $info[ 'racket_price' ] ) ? $info[ 'racket_price' ] : '' }}" class="form-control" />
        </div>
        <div class="form-group">
            <label>Залог для аренды ракетки (руб.)</label>
            <input type="number" name="racket_deposit" value="{{ isset ( $info[ 'racket_deposit' ] ) ? $info[ 'racket_deposit' ] : '' }}" class="form-control" />
        </div>
        <div class="form-group">
            <label>Аренда полотенца (руб.)</label>
            <input type="number" name="towel_price" value="{{ isset ( $info[ 'towel_price' ] ) ? $info[ 'towel_price' ] : '' }}" class="form-control" />
        </div>
        <div class="form-group">
            <label>Залог для аренды полотенца (руб.)</label>
            <input type="number" name="towel_deposit" value="{{ isset ( $info[ 'towel_deposit' ] ) ? $info[ 'towel_deposit' ] : '' }}" class="form-control" />
        </div>
        <div class="form-group">
            <button class="btn btn-primary btn-block" type="submit">Сохранить</button>
        </div>
    </form>
</div>
@endsection


@section ( 'scripts' )
    <script type="text/javascript">
        $( function() {
            $( 'input' ).on( 'change' , function ( e ) {
                return;
                // if ( e.which == 13 ) {
                    let val = $( this ).attr( 'checked');
                    let arena_id = $( this ).data( 'arena' );
                    let seat_id = $( this ).data( 'seat' );
                    let time = $( this ).data( 'time' );
                    let price = $( this ).data( 'price' );
                    let court_id = $( this ).data( 'court' );
                    let data = {
                        court_id: court_id ,
                        price: price ,
                        time: time ,
                        arena_id: arena_id ,
                        seat_id: seat_id ,
                        checked: val,
                        date: date
                    };
                    // console.log(data)
                    $.post ( '/api/boughtSeats' , data , function( res ) {
                        console.log( res );
                        toastr.success( 'Сохранено!' , '' );
                    })
                    // alert( time + weekType + arenaId );
                // }
            });
            // $( 'input' ).on( 'focus' , function ( e ) {
            //     $( this ).val( '' );
            // });
        });
    </script>
@endsection