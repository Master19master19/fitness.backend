@extends( 'layouts.app' )

@section( 'content' )
    <div class="container-fluid">
        <h1 class="title mb-4">Добавить пользователя</h1>
        <form class="form" method="POST" action="{{ route( 'users.store' ) }}">
            @csrf
            <div class="form-group">
                <label>Имя <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="100" required="" name="name" autofocus="" class="form-control" value="{{ old( 'name' ) }}" />
            </div>
            <div class="form-group">
                <label>Тел. номер <span class="text-danger">*</span></label>
                <input type="text" minlength="17" maxlength="18" required="" name="phone" autofocus="" class="form-control" value="{{ old( 'phone' ) }}" />
            </div>
            <div class="form-group">
                <label>E-mail <span class="text-danger">*</span></label>
                <input type="email" minlength="5" maxlength="100" required="" name="email" autofocus="" class="form-control" value="{{ old( 'email' ) }}" />
            </div>
            <div class="form-group">
                <label>Пароль <span class="text-danger">*</span></label>
                <input type="text" minlength="8" maxlength="100" required="" name="password" autofocus="" class="form-control" value="{{ old( 'password' ) }}" />
            </div>
            <div class="form-group">
                <select class="form-control" name="type" required="">
                    <option value="1">Менеджер</option>
                    <option value="2" selected="">Админ</option>
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block"><i class="fa fa-save"></i></button>
            </div>
        </form>
    </div>
@endsection