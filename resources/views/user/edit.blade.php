@extends( 'layouts.app' )

@section( 'content' )
    <div class="container-fluid">
        <h1 class="title mb-4">Редактировать пользователя</h1>
        <form class="form" method="POST" action="{{ route( 'users.update' , $user -> id ) }}">
            @csrf
            @method( 'PUT' )
            <div class="form-group">
                <label>Имя <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="100" required="" name="name" autofocus="" class="form-control" value="{{ $user -> name }}" />
            </div>
            <div class="form-group">
                <label>Тел. номер <span class="text-danger">*</span></label>
                <input type="text" minlength="17" maxlength="18" required="" name="phone" autofocus="" class="form-control" value="{{ $user -> phone }}" />
            </div>
            <div class="form-group">
                <label>E-mail <span class="text-danger">*</span></label>
                <input type="email" minlength="5" maxlength="100" required="" name="email" autofocus="" class="form-control" value="{{ $user -> email }}" />
            </div>
            <div class="form-group">
                <label>Пароль <span class="text-danger">*</span></label>
                <input type="text" minlength="8" maxlength="100" name="password" autofocus="" class="form-control" value="{{ $user -> password_plain }}" />
            </div>
            <div class="form-group">
                <select class="form-control" name="type" required="">
                    <option value="0" {{ $user -> type == 0 ? 'selected' : '' }}>Пользователь</option>
                    <option value="1" {{ $user -> type == 1 ? 'selected' : '' }}>Менеджер</option>
                    <option value="2" {{ $user -> type == 2 ? 'selected' : '' }}>Админ</option>
                </select>
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck" name="banned" {{ $user -> banned == 1 ? 'checked' : '' }}>
                    <label class="custom-control-label" for="customCheck">Бан</label>
                </div>
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck1" name="verified" {{ $user -> verified == 1 ? 'checked' : '' }}>
                    <label class="custom-control-label" for="customCheck1">Верифицирован</label>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block"><i class="fa fa-save"></i></button>
            </div>
        </form>
    </div>
@endsection