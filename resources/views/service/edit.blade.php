@extends( 'layouts.app' )

@section( 'content' )
    <div class="container-fluid">
        <h1 class="title mb-4">Редактировать услугу</h1>
        <form class="form" method="POST" action="{{ route( 'services.update' , $service -> id ) }}">
            @csrf
            @method( 'PUT' )
            <!-- <div class="form-group">
                <label>Название <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="100" required="" name="title" autofocus="" class="form-control" value="{{ $service -> title }}" />
            </div> -->
            <div class="form-group">
                <label>Цена <span class="text-danger">*</span></label>
                <input autofocus="" type="number" value="{{ $service -> price }}" required="" min="1" max="500000" step="0.01" name="price" class="form-control" />
            </div>
            <div class="form-group">
                <label>Минимальный заказ <span class="text-danger">*</span></label>
                <input type="number" value="{{ $service -> min_order }}" required="" min="10" max="5000" name="min_order" class="form-control" />
            </div>
            <div class="form-group">
                <label>Максимальный заказ <span class="text-danger">*</span></label>
                <input type="number" value="{{ $service -> max_order }}" required="" min="1" max="1000000" name="max_order" class="form-control" />
            </div>
            <div class="form-group">
                <label>Цена за 1000 <span class="text-danger">*</span></label>
                <input type="number" value="{{ $service -> thousand_price }}" required="" min="1" max="500000" step="0.01" name="thousand_price" class="form-control" />
            </div>
            @include( 'partials.service-form-selects' , compact( 'service' ) )
            <!-- <div class="form-group">
                <label>Описание <span class="text-danger">*</span></label>
                <textarea required="" class="form-control" name="description" minlength="10" maxlength="200">{{ $service -> description }}</textarea>
            </div> -->
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck" name="active" {{ $service -> active == 1 ? 'checked' : '' }}>
                    <label class="custom-control-label" for="customCheck">Активно</label>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block"><i class="fa fa-save"></i></button>
            </div>
        </form>
    </div>
@endsection