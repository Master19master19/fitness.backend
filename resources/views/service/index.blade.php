@extends( 'layouts.app' )

@section( 'content' )
    <div class="container-fluid">
        <h1 class="title mb-4">Услуги</h1>
        <a href="{{ route( 'services.create' ) }}" class="btn btn-info mb-4"><i class="fa fa-plus"></i> Добавить новую</a>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Вид
                    </th>
                    <th>
                        Цена
                    </th>
                    <th>
                        Минимальный заказ
                    </th>
                    <th>
                        Максимальный заказ
                    </th>
                    <th>
                        Цена за 1000
                    </th>
                    <!-- <th>
                        Описание
                    </th> -->
                    <!-- <th>
                        Вид
                    </th> -->
                    <th>
                        Категория
                    </th>
                    <th>
                        Активно
                    </th>
                    <th>
                        <i class="fa fa-wrench"></i>
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse ( $services as $service )
                    <tr>
                        <td>{{ $service -> id }}</td>
                        <td>{{ $service -> type -> title }}</td>
                        <td>{{ $service -> price }}</td>
                        <td>{{ $service -> min_order }}</td>
                        <td>{{ $service -> max_order }}</td>
                        <td>{{ $service -> thousand_price }}</td>
                        <!-- <td>{{ $service -> description }}</td> -->
                        <td>{{ $service -> category -> title }}</td>
                        <td>{{ $service -> activee }}</td>
                        <td>
                            <a href="{{ route( 'services.edit' , $service -> id ) }}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                            <a href="{{ route( 'services.destroy' , $service -> id ) }}" onclick="return confirm( 'Вы уверены?' );" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10">Пусто</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection