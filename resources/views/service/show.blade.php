@extends( 'layouts.app' )

@section( 'content' )
    <div class="container-fluid">
        <h1 class="title mb-4">Услуга #{{ $service -> id }}</h1>
        <div class="card w-100">
            <div class="card-body">
                <h5 class="card-title">{{ $service -> title }}</h5>
                <small class="my-3 d-block">Цена: <b>{{ $service -> price }}</b></small>
                <small class="my-3 d-block">Минимальный заказ: <b>{{ $service -> min_order }}</b></small>
                <small class="my-3 d-block">Максимальный заказ: <b>{{ $service -> max_order }}</b></small>
                <small class="my-3 d-block">Цена за 1000: <b>{{ $service -> thousand_price }}</b></small>
                <small class="my-3 d-block">Вид: <b>{{ $service -> type -> title }}</b></small>
                <small class="my-3 d-block">Категория: <b>{{ $service -> category -> title }}</b></small>
                <small class="my-3 d-block">Активно: <b>{{ $service -> category -> activee }}</b></small>
                <small class="my-3 d-block">Дата: <b>{{ $service -> created_at }}</b></small>
                <hr>
                <p class="card-text">{{ $service -> description }}</p>
            </div>
        </div>
    </div>
@endsection