@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Редактировать тренера</h1>
        <form enctype="multipart/form-data" class="form" method="POST" action="{{ route( 'trainers.update' , $trainer -> id ) }}">
            @csrf
            @method( 'PUT' )
            <div class="form-group">
                <label>Имя <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="100" required="" name="name" autofocus="" class="form-control" value="{{ $trainer -> name }}" />
            </div>
            <div class="form-group">
                <label>Описание <span class="text-danger">*</span></label>
                <textarea name="description" minlength="5" maxlength="100" required="" class="form-control" >{{ $trainer -> description }}</textarea>
            </div>
            <div class="form-group">
                <label>Образование <span class="text-danger">*</span></label>
                <input type="text" minlength="3" maxlength="100" required="" name="education" class="form-control" value="{{ $trainer -> education }}" />
            </div>
            <div class="form-group">
                <label>Специальность <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="100" required="" name="speciality" class="form-control" value="{{ $trainer -> speciality }}" />
            </div>
            <div class="form-group">
                <label>Достижения <span class="text-danger">*</span></label>
                <div id="achievements-wrapper">
                    <textarea name="achievements[]" minlength="5" maxlength="150" required="" class="form-control" placeholder="Введите достижение">{{ $trainer -> achievemente[ 0 ] }}</textarea>
                    @foreach ( $trainer -> achievemente as $key => $ach )
                        @if ( $key != 0 )
                            <textarea name="achievements[]" minlength="5" maxlength="150" class="form-control mt-3" placeholder="Введите достижение">{{ $ach }}</textarea>
                        @endif
                    @endforeach
                </div>
                <button type="button" id="add-achievement" class="btn btn-sm btn-primary mt-2"><i class="fa fa-plus"></i> Добавить еще</button>
            </div>
            <div class="form-group">
                <label>Тренерский стаж <span class="text-danger">*</span></label>
                <div id="experience-wrapper">
                    <textarea name="experience[]" minlength="5" maxlength="150" required="" class="form-control" placeholder="Введите стаж">{{ $trainer -> experiencee[ 0 ] }}</textarea>
                    @foreach ( $trainer -> experiencee as $key => $exp )
                        @if ( $key != 0 )
                            <textarea name="experience[]" minlength="5" maxlength="150" class="form-control mt-3" placeholder="Введите стаж">{{ $exp }}</textarea>
                        @endif
                    @endforeach
                </div>
                <button type="button" id="add-experience" class="btn btn-sm btn-primary mt-2"><i class="fa fa-plus"></i> Добавить еще</button>
            </div>
            <img src="{{ $trainer -> image_url }}" class="mb-4 w-50" id="image-preview" />
            <div class="custom-file mb-4">
                <input id="img" accept="image/png,image/jpg,image/jpeg" type="file" class="form-control custom-file-input" name="img" id="customFile">
                <label class="custom-file-label" for="customFile">Картинка <span class="text-danger">*</span></label>
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block"><i class="fa fa-save"></i></button>
            </div>
        </form>
    </div>
@endsection