<!DOCTYPE html>
<html>
<head>
	<title>Tennis.ru</title>
	<meta charset="utf-8">
</head>
<body style="font-family: sans-serif;">
	<img style="width: 70px;height: 70px;" src="{{ env( 'APP_URL' ) }}/logo.png" />
	<h1 style="text-align: center;">
		Заказ {{ $data[ 'order' ][ 'key' ] }}
	</h1>
	@if ( count( $data[ 'boughtSubscriptions' ] ) )
		<div>
			<h2 style="padding: 5px;padding-top: 10px;">
				<b>Абонементы</b>
			</h2>
		</div>
		<table border="1" cellpadding="5" cellspacing="0" style="width: 100%;">
			<thead>
				<tr>
					<th style="font-size: 20px;padding: 10px;"><b>Абонемент</b></th>
					<th style="font-size: 20px;padding: 10px;"><b>Цена</b></th>
					<th style="font-size: 20px;padding: 10px;"><b>Абонент</b></th>
				</tr>
			</thead>
			<tbody>
				@foreach ( $data[ 'boughtSubscriptions' ] as $sub )
					<tr>
						<td style="font-size: 18px;padding: 10px;">{{ $sub[ 'subscription' ][ 'title' ] }}</td>
						<td style="font-size: 18px;padding: 10px;">{{ $sub[ 'price' ] }}</td>
						<td style="font-size: 18px;padding: 10px;">{{ $sub[ 'profile' ][ 'name' ] }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	@endif
	@if ( count( $data[ 'boughtSeats' ] ) )
	<div>
		<h2 style="padding: 5px;padding-top: 10px;">
			<b>Корты</b>
		</h2>
	</div>
	<table border="1" cellpadding="5" cellspacing="0" style="width: 100%;">
		<thead>
			<tr>
				<th style="font-size: 20px;padding: 10px;"><b>Зал</b></th>
				<th style="font-size: 20px;padding: 10px;"><b>Цена</b></th>
				<th style="font-size: 20px;padding: 10px;"><b>Время</b></th>
				<th style="font-size: 20px;padding: 10px;"><b>Дата</b></th>
			</tr>
		</thead>
		<tbody>
			@foreach ( $data[ 'boughtSeats' ] as $seat )
				<tr>
					<td style="font-size: 18px;padding: 10px;">{{ $seat[ 'hall' ][ 'title' ] }}</td>
					<td style="font-size: 18px;padding: 10px;">{{ $seat[ 'price' ] }}</td>
					<td style="font-size: 18px;padding: 10px;">{{ $seat[ 'time' ] }}</td>
					<td style="font-size: 18px;padding: 10px;">{{ date( 'd/m/Y' , strtotime( $seat[ 'date' ] ) ) }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	@endif
	<table border="0" cellpadding="5" cellspacing="0" style="width: 100%;margin-top: 10px;">
		<tfoot>
			<tr>
				<td style="font-size: 23px;padding: 10px;font-weight: bold;" colspan="1">
					Итого:
				</td>
				<td style="font-size: 23px;padding: 10px;font-weight: bold;text-align: center;" colspan="10">
					{{ round( $data[ 'order' ][ 'price' ] ) }} рублей
				</td>
			</tr>
		</tfoot>
	</table>
	<div>
		
		<div style="margin-top: 10px;font-size: 20px;">
			<p style="margin: 5px;"><b>ID</b> : {{ $data[ 'user' ][ 'id' ] }}</p>
			<p style="margin: 5px;"><b>Имя</b> : {{ $data[ 'user' ][ 'name' ] }}</p>
			<p style="margin: 5px;"><b>Телефон</b> : {{ $data[ 'user' ][ 'phone' ] }}</p>
			<p style="margin: 5px;"><b>Почта</b> : {{ $data[ 'user' ][ 'email' ] }}</p>
			<p style="margin: 5px;"><b>Дата</b> : {{ date( 'Y/m/d H:i:s' ) }}</p>
		</div>
	</div>
</body>
</html>