<!DOCTYPE html>
<html>
<head>
	<title>Tennis.ru</title>
	<meta charset="utf-8">
</head>
<body>
	<div style="text-align: center;">
		<img src="{{ env( 'APP_URL' ) }}/logo.png" />
	</div>
	<table border="1" cellpadding="5" cellspacing="0" style="width: 100%;">
		<thead>
			<tr>
				<th>{{ $title }} <b>{{ $desc ? $desc : '' }}</b></th>
			</tr>
		</thead>
		<!-- <tbody>
			<tr>
				<td>{{ $desc ? $desc : '' }}</td>
			</tr>
		</tbody> -->
	</table>
</body>
</html>