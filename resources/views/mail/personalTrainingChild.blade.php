<!DOCTYPE html>
<html>
<head>
	<title>Tennis.ru</title>
	<meta charset="utf-8">
</head>
<body>
	<h1 style="text-align: center;">{{ $title }}</h1>
	<div style="text-align: center;">
		<img src="{{ env( 'APP_URL' ) }}/logo.png" />
	</div>
	<table border="1" cellpadding="5" cellspacing="0" style="width: 100%;">
		<thead>
			<tr>
				<th style="font-size: 20px;padding: 10px;"><b>ФИО ребенка</b></th>
				<th style="font-size: 20px;padding: 10px;"><b>Дата рождения</b></th>
				<th style="font-size: 20px;padding: 10px;"><b>ФИО родителя</b></th>
				<th style="font-size: 20px;padding: 10px;"><b>Номер телефона</b></th>
				<th style="font-size: 20px;padding: 10px;"><b>Уровень</b></th>
				<th style="font-size: 20px;padding: 10px;"><b>Дополнительное</b></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="font-size: 18px;padding: 10px;">{{ $child_name ? $child_name : '' }}</td>
				<td style="font-size: 18px;padding: 10px;">{{ $child_birthday ? $child_birthday : '' }}</td>
				<td style="font-size: 18px;padding: 10px;">{{ $parent_name ? $parent_name : '' }}</td>
				<td style="font-size: 18px;padding: 10px;">{{ $phone_number ? $phone_number : '' }}</td>
				<td style="font-size: 18px;padding: 10px;">{{ $level ? $level : '' }}</td>
				<td style="font-size: 18px;padding: 10px;">{{ $additional ? $additional : '' }}</td>
			</tr>
		</tbody>
	</table>
</body>
</html>