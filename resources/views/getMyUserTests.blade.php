<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Тестовые заказы</title>

    <!-- Scripts -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" type="image/png" href="favicon.png" />
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('app.css') }}" rel="stylesheet">
</head>
<body class="container-fluid text-center bg-light">

    <h1 class="mt-5">Тестовые заказы</h1>
    <h2 class="mt-5 pt-5 mb-3">Корты</h2>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Название зала</th>
                <th>Бронирован на:</th>
                <th>Заказ создан</th>
            </tr>
        </thead>
        <tbody>
            @foreach( $seats as $sub )
                <tr>
                    <td>{{ $sub[ 'hall' ] }}</td>
                    <td>{{ $sub[ 'date' ] }}</td>
                    <td>{{ $sub[ 'date_created' ] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <h2 class="mt-5 pt-5 mb-3">Абонементы</h2>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Название</th>
                <th>Имя профиля</th>
                <th>Тип</th>
                <th>Телефон</th>
                <th>Дата рождения</th>
                <th>Абонемент истечет</th>
                <th>Заказ создан</th>
            </tr>
        </thead>
        <tbody>
            @foreach( $subscriptions as $sub )
                <tr>
                    <td>{{ $sub[ 'subTitle' ] }}</td>
                    <td>{{ $sub[ 'profileName' ] }}</td>
                    <td>{{ $sub[ 'profileType' ] }}</td>
                    <td>{{ $sub[ 'profilePhone' ] }}</td>
                    <td>{{ $sub[ 'profileBirthday' ] }}</td>
                    <td>{{ $sub[ 'expires' ] }}</td>
                    <td>{{ $sub[ 'date_created' ] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <h2 class="mt-5 pt-5 mb-3">Пользователь</h2>
    <div class="card">
        <p>Имя: {{ $user -> name }}</p>
        <p> Телефон: {{ $user -> phone }}</p>
        <p> Почта: {{ $user -> email }}</p>
    </div>
    <script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{ asset('app.js') }}" defer></script>
    @yield( 'scripts' )
</body>
</html>
