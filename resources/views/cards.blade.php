@extends( 'layouts.app' )

@section( 'content' )
<div class="container">
   <h3>Мои карты</h3>
    <h5 class="mb-4">Привязанные карты</h5>

    <style type="text/css">
      body {
        background-color: #fafafa;
      }
      .row {
        background-color: white;
        border-radius: 5px;
        height: 50px;
        margin-bottom: 1rem;
      }
      .row * {
        vertical-align: middle;
      }
    </style>
    <div class="row w-50 justify-content-center d-flex align-content-center align-items-center">
      <div class="col">
        <img src="/pay 1.png" />
      </div>
      <div class="col-9">
        **** **** **** 3355
      </div>
      <div class="col" onClick="confirm( 'Вы точно хотите удалить карту?' )">
        <img src="/delete icon.png" />
      </div>
    </div>
    <div class="row w-50 justify-content-center d-flex align-content-center align-items-center">
      <div class="col">
        <img src="/pay 1.png" />
      </div>
      <div class="col-9">
        **** **** **** 4522
      </div>
      <div class="col" onClick="confirm( 'Вы точно хотите удалить карту?' )">
        <img src="/delete icon.png" />
      </div>
    </div>
    <div class="w-50 text-center d-flex justify-content-center align-content-center align-items-center">
      <span>
        <img width="16" src="/Vector.png" />
      </span>
      <span class="ml-2">
        <b>
          
        Добавить новую карту
        </b>
      </span>
    </div>
</div>
@endsection


@section ( 'scripts' )
    <script type="text/javascript">
        $( function() {
            $( 'input' ).on( 'change' , function ( e ) {
                return;
                // if ( e.which == 13 ) {
                    let val = $( this ).attr( 'checked');
                    let arena_id = $( this ).data( 'arena' );
                    let seat_id = $( this ).data( 'seat' );
                    let time = $( this ).data( 'time' );
                    let price = $( this ).data( 'price' );
                    let court_id = $( this ).data( 'court' );
                    let data = {
                        court_id: court_id ,
                        price: price ,
                        time: time ,
                        arena_id: arena_id ,
                        seat_id: seat_id ,
                        checked: val,
                        date: date
                    };
                    // console.log(data)
                    $.post ( '/api/boughtSeats' , data , function( res ) {
                        console.log( res );
                        toastr.success( 'Сохранено!' , '' );
                    })
                    // confirm( time + weekType + arenaId );
                // }
            });
            // $( 'input' ).on( 'focus' , function ( e ) {
            //     $( this ).val( '' );
            // });
        });
    </script>
@endsection