@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <h1 class="title mb-4">Добавить абонемент</h1>
        <form class="form" method="POST" action="{{ route( 'subscriptions.store' ) }}">
            @csrf
            <div class="form-group">
                <label>Название <span class="text-danger">*</span></label>
                <input type="text" minlength="5" maxlength="100" required="" name="title" autofocus="" class="form-control" value="{{ old( 'title' ) }}" />
            </div>
            <div class="form-group">
                <label>Количество тренировок <span class="text-danger">*</span></label>
                <input type="number" min="1" max="100" minlength="1" maxlength="100" required="" name="weekCount" class="form-control" value="{{ old( 'weekCount' ) }}" />
            </div>
            <div class="form-group">
                <label>Продолжительность тренировки ( час ) <span class="text-danger">*</span></label>
                <input type="number" min="1" step="0.5" max="5" minlength="1" maxlength="100" required="" name="duration" class="form-control" value="{{ old( 'duration' ) }}" />
            </div>
            <div class="form-group">
                <label>Цена ( руб./мес. ) <span class="text-danger">*</span></label>
                <input type="number" min="200" required="" name="price" autofocus="" class="form-control" value="{{ old( 'price' ) }}" />
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block"><i class="fa fa-save"></i></button>
            </div>
        </form>
    </div>
@endsection