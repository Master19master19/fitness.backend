<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
</head>
<body style="margin-top: 20px;">
    <img src="/load.gif" id="loader" style="position: fixed;
    left: 50%;
    transform: translateX(-50%);height: 33vw;top: 15vw;z-index: 0;">
    @if ( $isCard )
        <h4 style="text-align: center;margin: auto; width: 70%;position: relative;z-index: 5;">Мы спишем 1 рубль, чтобы привязать вашу карту</h4>
    @endif


<script src="https://yookassa.ru/checkout-widget/v1/checkout-widget.js"></script>

<!-- <script src="https://kassa.yandex.ru/checkout-ui/v2.js"></script> -->

<div id="payment-form" style="z-index: 2; position: relative;background: white;margin-top: 5px;"></div>

<script>
//Инициализация виджета. Все параметры обязательные.
const checkout = new window.YooMoneyCheckoutWidget({
    confirmation_token: "{!! $confirmation_token !!}", //Токен, который перед проведением оплаты нужно получить от ЮKassa
    return_url: "{!! env( 'KASSA_RETURN_URL' ) !!}", //Ссылка на страницу завершения оплаты
    error_callback(error) { console.log({error})},
        customization: {
        //Настройка цветовой схемы, минимум один параметр, значения цветов в HEX
        colors: {
            //Цвет акцентных элементов: кнопка Заплатить, выбранные переключатели, опции и текстовые поля
            controlPrimary: '#7DC82F',
            controlPrimaryContent: '#ffffff'
        }
    },
});

//Отображение платежной формы в контейнере
checkout.render('payment-form')
//После отображения платежной формы метод render возвращает Promise (можно не использовать).
  .then(() => {
    document.getElementById( 'loader' ).style.display = 'none'
     //Код, который нужно выполнить после отображения платежной формы.
  });
</script>


<!-- <script>
const checkout = new window.YandexCheckout({
    confirmation_token: "{!! $confirmation_token !!}", //Токен, который перед проведением оплаты нужно получить от Яндекс.Кассы
    return_url: "{!! env( 'KASSA_RETURN_URL' ) !!}",
    error_callback(error) { console.log({error})},
        customization: {
        //Настройка цветовой схемы, минимум один параметр, значения цветов в HEX
        colors: {
            //Цвет акцентных элементов: кнопка Заплатить, выбранные переключатели, опции и текстовые поля
            controlPrimary: '#7DC82F',
            controlPrimaryContent: '#ffffff'
        }
    },

});
checkout.render( "payment-form" );
</script> -->


</body>
</html>