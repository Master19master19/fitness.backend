@extends('layouts.app')

@section('content')
    <style type="text/css">
        table tr th {
            color: white!important;
        }
        table tr th {
            position: sticky;
            top: 0;
        }
        tr.sec th {
            position: sticky;
            top: 50px;
        }
        table thead {
            position: relative;
        }
    </style>
<div class="container-fluid">
    <div class="my-3 d-flex text-center">
        <a href="{{ route( 'boughtSeats' , date( 'Y-m-d' , strtotime( '-1 day' , strtotime( $date ) ) ) ) }}" class="mr-auto"><i class="fa fa-arrow-left fa-2x"></i></a>
        <span class="mx-auto0" style="font-size: 25px;"><b>Бронирования кортов {{ date( 'd/m/Y' , strtotime( $date ) ) }}</b></span>
        <a href="{{ route( 'boughtSeats' , date( 'Y-m-d' , strtotime( '+1 day' , strtotime( $date ) ) ) ) }}" class="ml-auto"><i class="fa fa-arrow-right fa-2x"></i></a>
    </div>
    <table class="table table-striped table-bordered">
        <tbody>
            
                @foreach ( $arenas as $arena )
                    @foreach ( $arena[ 'courts' ] as $keyeee => $court )
            <tr>
                @if ( $keyeee == 0 )
                    <th style="background-color: {{ $arena[ 'cell_color' ] }};vertical-align: middle;width: 200px;" colspan="1" rowspan="{{ count( $arena[ 'courts' ] ) }}">{{ $arena[ 'title' ] }}</th>
                    @endif
                    <td colspan="1" style="background-color: {{ $arena[ 'cell_color' ] }};color:white;font-weight: bold;vertical-align: middle;">{{$court['title']}}</td>
                    @foreach($court['seats'] as $seatKey => $seat )
                        <td colspan="10" class="text-center @if ( isset( $arena[ 'unAvailableSeats' ][ $court[ 'id' ] ] ) && in_array( $seat[ 'id' ] , $arena[ 'unAvailableSeats' ][ $court[ 'id' ] ] ) )
                                bg-danger text-white
                                    @endif">
                                    <b>{{ $seat[ 'time' ] }}</b> <br>
                                    @if ( isset( $arena[ 'unAvailableSeats' ][ $court[ 'id' ] ] ) && in_array( $seat[ 'id' ] , $arena[ 'unAvailableSeats' ][ $court[ 'id' ] ] ) )
                                    @else
                                        <input type="checkbox" data-seat="{{ $seat[ 'id' ] }}" data-court="{{ $court[ 'id' ] }}" data-arena="{{ $arena[ 'id' ] }}" data-time="{{ $seat[ 'time' ] }}" data-price="{{ $isWeekend ? $arena[ 'seats' ][ $seatKey ][ 'price_weekend' ] : $arena[ 'seats' ][ $seatKey ][ 'price' ] }}" class="form-control" />
                                    @endif
                                    <!-- <small>{{ $isWeekend ? $arena[ 'seats' ][ $seatKey ][ 'price_weekend' ] : $arena[ 'seats' ][ $seatKey ][ 'price' ] }}</small> -->
                                </td>
                    @endforeach
            </tr>
                    @endforeach
                @endforeach
        </tbody>
    </table>
</div>
@endsection


@section ( 'scripts' )
    <script type="text/javascript">
        $( function() {
            $( 'input' ).on( 'change' , function ( e ) {
                // if ( e.which == 13 ) {
                    let val = $( this ).attr( 'checked');
                    let arena_id = $( this ).data( 'arena' );
                    let seat_id = $( this ).data( 'seat' );
                    let time = $( this ).data( 'time' );
                    let price = $( this ).data( 'price' );
                    let court_id = $( this ).data( 'court' );
                    let date = "{{ $date }}";
                    let data = {
                        court_id: court_id ,
                        price: price ,
                        time: time ,
                        arena_id: arena_id ,
                        seat_id: seat_id ,
                        checked: val,
                        date: date
                    };
                    // console.log(data)
                    $.post ( '/api/boughtSeats' , data , function( res ) {
                        console.log( res );
                        toastr.success( 'Сохранено!' , '' );
                    })
                    // alert( time + weekType + arenaId );
                // }
            });
            // $( 'input' ).on( 'focus' , function ( e ) {
            //     $( this ).val( '' );
            // });
        });
    </script>
@endsection