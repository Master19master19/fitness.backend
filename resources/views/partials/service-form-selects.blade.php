<!-- <div class="form-group">
    <label>Качество: <span class="text-danger">*</span></label>
    <select class="form-control" name="quality_id" required="">
        @forelse ( $qualities as $quality )
            <option {{ ( isset( $service ) && $service -> quality_id == $quality -> id ) ? 'selected' : '' }} value="{{ $quality -> id }}">{{ $quality -> title }}</option>
        @empty
            <option disabled="" value="">Ничего не найдено</option>
        @endforelse
    </select>
</div> -->
<div class="form-group">
    <label>Вид: <span class="text-danger">*</span></label>
    <select class="form-control" name="type_id" required="">
        @forelse ( $types as $type )
            <option {{ ( isset( $service ) && $service -> type_id == $type -> id ) ? 'selected' : '' }} value="{{ $type -> id }}">{{ $type -> title }}</option>
        @empty
            <option disabled="" value="">Ничего не найдено</option>
        @endforelse
    </select>
</div>
<div class="form-group">
    <label>Категория: <span class="text-danger">*</span></label>
    <select class="form-control" name="category_id" required="">
        @forelse ( $categories as $category )
            <option {{ ( isset( $service ) && $service -> category_id == $category -> id ) ? 'selected' : '' }} value="{{ $category -> id }}">{{ $category -> title }}</option>
        @empty
            <option disabled="" value="">Ничего не найдено</option>
        @endforelse
    </select>
</div>