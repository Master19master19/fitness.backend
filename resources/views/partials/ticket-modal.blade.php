<div class="modal fade" id="ticket-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ответить</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form" id="form-answer" action="{{ route( 'tickets.answer' , $ticket -> id ) }}" method="POST">
          @csrf
          <div class="form-group">
            <label>Сообщение <i class="text-danger">*</i></label>
            <input type="text" required="" autofocus="" name="answer" class="form-control" />
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-primary" form="form-answer">Ответить</button>
      </div>
    </div>
  </div>
</div>
