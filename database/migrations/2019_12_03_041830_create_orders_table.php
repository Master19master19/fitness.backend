<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'orders' , function (Blueprint $table) {
            $table -> bigIncrements( 'id' );
            $table -> unsignedBigInteger( 'user_id' );
            // $table -> unsignedBigInteger( 'service_id' );
            // $table -> unsignedBigInteger( 'quantity' );
            $table -> unsignedDecimal( 'price' , 15 , 5 ) -> nullable();
            // $table -> unsignedDecimal( 'total_price' , 15 , 5 ) -> nullable();
            $table -> enum( 'paid' , [ 0 , 1 ] );
            $table -> string( 'key' ) -> nullable();
            $table -> string( 'unique' );
            $table -> string( 'link' ) -> nullable();    
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
