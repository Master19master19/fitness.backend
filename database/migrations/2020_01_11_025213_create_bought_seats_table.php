<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoughtSeatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bought_seats', function (Blueprint $table) {
            $table->bigIncrements( 'id' );
            $table -> tinyInteger( 'paid' ) -> default( 0 );
            $table -> unsignedBigInteger( 'seat_id' ) -> nullable();
            $table -> unsignedBigInteger( 'court_id' ) -> nullable();
            $table -> unsignedBigInteger( 'order_id' );
            $table -> unsignedBigInteger( 'arena_id' ) -> nullable();
            $table -> unsignedBigInteger( 'user_id' ) -> nullable();
            $table -> date( 'date' ) -> nullable(); 
            $table -> string( 'token' ) -> nullable();
            $table -> string( 'time' );
            $table -> unsignedInteger( 'price' );
            $table -> string( 'ip' ) -> nullable();
            $table -> tinyInteger( 'checked' ) -> default( 0 );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bought_seats');
    }
}
