<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoughtSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bought_subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table -> tinyInteger( 'paid' ) -> default( 0 );
            $table -> unsignedBigInteger( 'subscription_id' ) -> nullable();
            $table -> unsignedBigInteger( 'user_id' ) -> nullable();
            $table -> unsignedInteger( 'price' );
            $table -> unsignedBigInteger( 'order_id' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bought_subscriptions');
    }
}
