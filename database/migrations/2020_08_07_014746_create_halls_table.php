<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up () {
        Schema::create( 'halls' , function ( Blueprint $table ) {
            $table -> bigIncrements( 'id' );
            $table -> string( 'title' );
            $table -> unsignedInteger( 'price' );
            $table -> unsignedInteger( 'prime_price' );
            $table -> unsignedTinyInteger( 'active' ) -> default( 1 );
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('halls');
    }
}
