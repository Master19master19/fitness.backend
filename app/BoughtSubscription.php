<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoughtSubscription extends Model {
    //
	protected $guarded = [];
	protected $appends = [ 'expired' , 'date_from' , 'date_to' , 'expires_at' , 'date_to_iso' , 'date_from_iso' , 'has_discount','bought_at' , 'extension_prohibited' ];
	public function user () {
		return $this -> hasOne( 'App\User', 'id' , 'user_id' );
	}
	public function profile () {
		return $this -> hasOne( 'App\Profile', 'id' , 'profile_id' );
	}
	public function subscription () {
		return $this -> hasOne( 'App\Subscription', 'id' , 'subscription_id' );
	}
	public function getExtensionProhibitedAttribute () {
		$expiry_date = date( 'Y-m-d' ,  strtotime( $this -> expiration_time ) );
		$today = date( 'Y-m-d' );
		return $today < $expiry_date;
	}
	public function getExpiredAttribute () {
		$time = strtotime( $this -> expiration_time );
		// $timee = strtotime( "+1 month" , $time );
		return time() - $time > 0;
	}
	public function getExpiresAtAttribute () {
		$time = strtotime( $this -> expiration_time );
		// $timee = strtotime( "+1 month" , $time );
		return date( 'd.m' , $time );
	}
	public function getHasDiscountAttribute () {
		// return false;
		// $expTime = strtotime( $this -> expiration_time );
		if ( $this -> subscription -> has_discount != 1 ) return false;
		// if ( $this -> subscription -> has_discount != 1 ) return false;
		$expTime = date( 'Y-m-d' , strtotime( "+4 days" , strtotime( $this -> expiration_time ) ) );
		if ( date( 'Y-m-d' ) < $expTime && date( 'Y-m-d' ) >= date( 'Y-m-d' , strtotime( $this -> expiration_time ) ) ) {
			return true;
		}
		return false;
		// if ( strtotime( '+1 days' ) < $time ) {
		// 	return false;
		// } else if ( strtotime( '-1 days' ) > $time ) {
		// 	return false;
		// }
		// $timee = strtotime( "+1 month" , $time );
		return true;
	}
	// public function getTimeToPushDiscountAttribute () {
	// 	$expTime = date( 'Y-m-d' , strtotime( $this -> expiration_time ) );
	// 	if ( date( 'Y-m-d' ) == $expTime ) {
	// 		return true;
	// 	}
	// 	return false;
	// }
	public function getDateFromAttribute () {
		$time = strtotime( $this -> updated_at );
		return date( 'd.m.y' , $time );
	}
	public function getDateToAttribute () {
		$time = strtotime( $this -> expiration_time );
		// $timee = strtotime( "+1 month" , $time );
		return date( 'd.m.y' , $time );
	}
	public function getBoughtAtAttribute () {
		$time = strtotime( $this -> created_at );
		// $timee = strtotime( "+1 month" , $time );
		return date( 'd.m' , $time );
	}
	public function getDateFromIsoAttribute () {
		$time = strtotime( $this -> updated_at );
		return date( 'Y-m-d' , $time );
	}
	public function getDateToIsoAttribute () {
		$time = strtotime( $this -> expiration_time );
		// $timee = strtotime( "+1 month" , $time );
		return date( 'Y-m-d' , $time );
	}
}
