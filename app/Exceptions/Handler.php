<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Helpers\LogHelper;

class Handler extends ExceptionHandler {
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        try {
            parent::report($exception);
            LogHelper::send( $exception -> getMessage() );
        } catch ( Exception $e ) {
            file_put_contents( __DIR__ . '/fatalaftal' , $e -> getMessage() .PHP_EOL , FILE_APPEND );
        }
        if ( $exception instanceof \Illuminate\Database\QueryException ) {
            file_put_contents( __DIR__ . '/mysqlerror' , $exception -> getMessage() . PHP_EOL , FILE_APPEND );
            header('HTTP/1.1 400 Bad Request');
            exit( json_encode([
                'result' => false,
                'errorCode' => 400,
                'error' => "Проблема с подключением Mysql"
            ]));
        } else {

        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $ip = $request -> ip();
        if ( $ip == env( 'BITRIX_IP' ) ) {
            $code = $exception -> getCode();
            $message = $exception -> getMessage();
            parent::report( $exception );
            $data = [
                'result' => true,
                'errorCode' => 500,
                'errorMessage' => $message
            ];
            exit ( json_encode( $data ) );
        } else {
            return parent::render($request, $exception);
        }
    }
}
