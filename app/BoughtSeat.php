<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoughtSeat extends Model
{
    protected $guarded = [];
	public function hall () {
		return $this -> hasOne( 'App\Hall', 'id' , 'hall_id' );
	}
}
