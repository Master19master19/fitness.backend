<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;

class AuthenticateOnceWithBasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, $next)
    {
        if ( ! $this -> basic_validate ( $request -> header( 'PHP_AUTH_USER' ) , $request -> header( 'PHP_AUTH_PW' ) ) ) {
            $data = [
                'result' => false,
                'errorCOde' => 401,
                'error' => 'Неправильные логин и пароль'
            ];
            return response() -> json( $data , 200 , array() , JSON_PRETTY_PRINT );
        }
        return $next( $request );
    }

    private function basic_validate( $user , $password ) {
        if ( $user == env( 'BITRIX_LOCAL_USERNAME' ) && $password == env( 'BITRIX_LOCAL_PASSWORD' ) ) {
            return true;
        }
        return false;
    }

}
