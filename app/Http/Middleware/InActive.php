<?php

namespace App\Http\Middleware;

use Closure;

class Active {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( auth()->user() ) {
            if ( auth()->user()->type == 1 || auth()->user()->type == 2 ) {
                return $next( $request );
            } else {
                return response() -> view( 'errors.401' );
            }
        } else {
            return $next( $request );
        }
    }
}
