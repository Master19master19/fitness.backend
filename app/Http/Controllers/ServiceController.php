<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $services = Service::orderBy( 'active' , 'DESC' ) -> get();
        return view( 'service.index' , compact( 'services' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create () {
        return view( 'service.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $req ) {
        $data = $req -> only( 'title' , 'description' , 'price' , 'category_id' , 'type_id' , 'quality_id' , 'min_order' , 'max_order' , 'thousand_price' );
        $res = Service::create( $data );
        return redirect() -> route( 'services.index' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        return view( 'service.show' , compact( 'service' ) );
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit ( Service $service ) {
        return view( 'service.edit' , compact( 'service' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update ( Request $req , Service $service ) {
        $data = $req -> only( 'title' , 'description' , 'price' , 'category_id' , 'type_id' , 'quality_id' , 'min_order' , 'max_order' , 'thousand_price' );
        $data[ 'active' ] = $req -> has( 'active' ) ? 1 : 0;
        $res = $service -> update( $data );
        return redirect() -> route( 'services.index' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        Service::findOrFail( $id ) -> delete();
        return redirect() -> route( 'services.index' );
    }
}
