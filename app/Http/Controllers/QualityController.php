<?php

namespace App\Http\Controllers;

use App\Quality;
use App\Service;
use Illuminate\Http\Request;

class QualityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $qualities = Quality::all();
        return view( 'quality.index' , compact( 'qualities' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'quality.create' );
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req )
    {
        $data = $req -> only( 'title' );
        Quality::create( $data );
        return redirect() -> route( 'qualities.index' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Quality  $quality
     * @return \Illuminate\Http\Response
     */
    public function show(Quality $quality)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Quality  $quality
     * @return \Illuminate\Http\Response
     */
    public function edit(Quality $quality)
    {
        //
        return view( 'quality.edit' , compact( 'quality' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Quality  $quality
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req , Quality $quality)
    {
        $data = $req -> only( 'title' );
        $res = $quality -> update( $data );
        return redirect() -> route( 'qualities.index' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Quality  $quality
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        Quality::findOrFail( $id ) -> delete();
        Service::where( 'quality_id' , $id ) -> update( [ 'quality_id' => Quality::first() -> id ]);
        return redirect() -> route( 'qualities.index' );
    }
}
