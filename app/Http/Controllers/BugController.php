<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\BoughtSeat;
use App\ReservedSeat;
use App\In;
use App\BitrixIn;
use App\Order;
use App\BoughtSubscription;
use App\Seat;
use DB;
use App\Helpers\PaymentHelper;
use App\Helpers\BitrixReserveHelper;
use App\Helpers\BitrixSendPaymentHelper;
use App\Helpers\BitrixHelper;
use App\Helpers\BackupHelper;
use App\Helpers\BitrixCancelReserveHelper;
use App\Helpers\PushHelper;
use App\Helpers\DiscountHelper;


class BugController extends Controller {
    // public function cancelReserveByOrder( $order_id ) {
    //     BitrixCancelReserveHelper::cancelReserveByOrderId( $order_id );
    // }
    // public function cancelCheck() {
    //     BitrixCancelReserveHelper::check();
    // }
    // public function getPaymentInfo( $order_id ) {
    //     $res = PaymentHelper::getPaymentInfoStatus( $order_id );
    //     dd($res);
    // }
    public function sendByOrderId( $orderId ) {
        $order = Order::whereId ( $orderId )->first();
        // dd($order);
        // BitrixSendPaymentHelper::send( $order -> id , $order -> key );
        dd($orderId);
    }
     protected function sendMail ( $order ) {
        $user = User::findOrFail( $order -> user_id ) -> toArray();
        $finalData = [
            'boughtSubscriptions' => [],
            'boughtSeats' => [],
            'user' => $user,
            'order' => $order
        ];
        $boughtSubscriptions = BoughtSubscription::where( [ 'order_id' => $order -> id ] ) -> with( 'subscription' ) -> with( 'profile' ) -> get() -> toArray();
        if ( null !== $boughtSubscriptions ) {
            foreach ( $boughtSubscriptions as $key => $value ) {
                $finalData[ 'boughtSubscriptions' ][] = $value;
            }
        }
        $boughtSeats = BoughtSeat::where( [ 'order_id' => $order -> id ] ) -> orderBy( 'hall_id' , 'ASC' )-> orderBy( 'time' , 'ASC' ) -> with( [ 'hall' ] ) -> get() -> toArray();
        if ( null !== $boughtSeats ) {
            foreach ( $boughtSeats as $key => $value ) {
                $finalData[ 'boughtSeats' ][] = $value;
            }
        }
        // dd($finalData);
        \App\Helpers\Mail::sendToAdmin( $finalData );
    }



    public function someMailSend( Request $req , $id ) {
        $order = Order::find( $id );
        // BoughtSubscription::where( [ 'order_id' => $order -> id ] ) -> update([ 'paid' => 1 ]);
        // BitrixSendPaymentHelper::send( $order -> id , $order -> key );
        // $this -> sendMail($order);
    }
    public function sendMofo () {
        $data = json_decode( '{"class":"payment","phone":"+71111111111","guid":"27921788-5555-8888-9000-1755a71feryy","items":[{"itemType":"membership","membershipType":"adult","profileName":"Testing Profile","membershipId":"5a9c423f-aef4-11ea-80c1-ac1f6bb3f7d3","count":1,"sum":100,"expirationTime":"2021-02-12T11:41:43","phone":"+71111111111"}],"changeDate":"2021-01-14T11:42:01"}' , 1 );
        $res = BitrixHelper::send( $data , 'needed' , true );
        $res1 = BitrixHelper::send( $data , 'needed' , true );
        dd($res,$res1);

    }
    public function discount() {
        DiscountHelper::check();
    }
    public function push () {
        $data = [
            'title' => 'test',
            'body' => 'bodyTest'
        ];
        $users = User::whereNotNull( 'expo_token' ) -> get();
        foreach ( $users as $key => $user ) {
            $res = PushHelper::push( $user -> id , $data );
            var_dump($res);
        }
    }
    public function payStats () {
        $fuckOrders = [];
        $differences = [];
        $totalCount = Order::wherePaid( '1' ) -> count();
        $paidOrders = Order::wherePaid( '1' ) -> where( 'price' , '>' , 500 ) -> whereDate( 'created_at' , '>' , '2020-06-01 22:00:00' ) -> orderBy( 'id' , 'DESC' ) -> get() -> toArray();
        foreach ( $paidOrders as $key => $order ) {
            $difference = ceil ( ( strtotime( $order[ 'updated_at' ] ) - strtotime( $order[ 'created_at' ] ) ) / 60 );
            $paidOrders[ $key ][ 'timediff' ] = $difference;
            // if ( $key === 1500 ) {
                // dd($difference,$order);
            // }
            // if ( $difference > 10 ) {
                // dd($order,$difference);
            // }
            // if ( $difference > 56 &&  ) dd($order);
            if ( $difference < 1 ) dd($order);
            // if ( $difference > 56 && strpos( $order[ 'updated_at' ] , '22:' ) === false ) dd($order);
            if ( isset( $differences[ $difference ] ) ) {
                $differences[ $difference ]++;
            } else {
                $differences[ $difference ] = 1;
            }
            if ( $difference > 20 ) {
                $fuckOrders[] = $paidOrders[ $key ];
                // dd($order);
            }
        }
        foreach ( $differences as $key => $value ) {
            if ( $value < 1 ) {
                unset( $differences[ $key ] );
            }
        }
        ksort( $differences );
        // dd($fuckOrders);
        return view( 'errors.payStats' , compact( 'differences' , 'totalCount' , 'fuckOrders' ) );
    }
    public function checkWrongCourts() {
        $shit = [];
        $boughtSeats = BoughtSeat::orderBy( 'id' , 'DESC' ) -> offset( 2000 ) -> take( 2000 ) -> get();
        foreach ($boughtSeats as $bb ) {
            $seatRecord = Seat::where([
                'time' => $bb[ 'time' ],
                'arena_id' => $bb[ 'arena_id' ],
                'court_id' => $bb[ 'court_id' ],
            ]) -> firstOrFail();
            if ( $bb->seat_id != $seatRecord->id ) {
                $shit[] = [
                    'seat' => $seatRecord -> toArray(),
                    'bought_seat' => $bb -> toArray(),
                ];
            }
        }
        dd($shit);
    }
    public function test () {
        BackupHelper::backup( true );
        exit;
        $order = PaymentHelper::createPayment( 150 );
        dd($order);
        exit;
        $id = 2618;
            BitrixSendPaymentHelper::send( $id );
            exit;
        $orders = Order::where( 'paid' , 1 ) -> count();
        dd($orders);
        $orders = Order::where( 'paid' , '1' ) -> orderBy( 'id' , 'DESC' ) -> take( 2000 ) -> get();
        foreach ($orders as $key => $value) {
            usleep( 150000 );
            BitrixSendPaymentHelper::send( $value -> id );
        }
    }
    public function compareHistory() {
        $success = 0;
        $successs = 0;
        $bitrixIns = BitrixIn::all();
        // $ins = In::all();
        foreach ( $bitrixIns as $key => $value) {
            $check = In::where( 'message' , '=' , $value -> message ) -> first();
            if ( $check == null ) {
                    dd($value);
            } else {
                $success++;
            }
        }
        dd($success,$successs);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function users () {
    //     exit('done');
    //     $users = User::orderBy( 'id' , 'ASC' ) -> get();
    //     foreach ( $users as $key => $user ) {
    //         if ( $user -> id > 55 ) {
    //             // dd( ( $user -> id ) % 2 == 0 );
    //             BitrixHelper::createUser([
    //                 'name' => $user -> name,
    //                 'phone' => $user -> phone,
    //                 'email' => $user -> email
    //             ]);
    //             if ( ( $user -> id ) % 2 == 0 ) {
    //                 sleep( 1 );
    //             }
    //         }
    //     }
    // }
    public function canceled () {
        FixHelper::fixCancel();
    }
    public function reserved () {
        FixHelper::fixReserve();
    }
    public function paid () {
        FixHelper::fixPaid();
    }
    // public function reservesBak () {
    //     $reservedSeats = [];
    //     $bought = ReservedSeat::where( 'paid' , 1 ) -> where( 'created_at' , 'LIKE' , '%2020-05-29%' ) -> where( 'user_id' , '!=' , 0 ) -> take( 10 ) -> get();
    //     foreach ( $bought as $key => $bou ) {
    //         $reservedSeats = [
    //             'created_at' => $bou[ 'created_at' ],
    //             'court_id' => $bou[ 'court_id' ],
    //             'time' => $bou[ 'time' ],
    //             'date' => $bou[ 'date' ],
    //         ];
    //         $user = User::whereId( $bou[ 'user_id' ] ) -> first();
    //         if ( null == $user ) {
    //             // dd($bou);
    //             file_put_contents( __DIR__ . '/reserve_no_user' , json_encode( $bou ) . PHP_EOL , FILE_APPEND );
    //             continue;
    //         }
    //         $phone = $user -> phone;
    //         // dd('asdsd');
    //         $res = BitrixReserveHelper::sendSingle( $reservedSeats , $phone );
    //     }
    //     dd($reservedSeats);
    //     $users = User::orderBy( 'id' , 'ASC' ) -> get();
    //     foreach ( $users as $key => $user ) {
    //         if ( $user -> id > 55 ) {
    //             // dd( ( $user -> id ) % 2 == 0 );
    //             BitrixHelper::createUser([
    //                 'name' => $user -> name,
    //                 'phone' => $user -> phone,
    //                 'email' => $user -> email
    //             ]);
    //             if ( ( $user -> id ) % 2 == 0 ) {
    //                 sleep( 1 );
    //             }
    //         }
    //     }
    // }

    public function getMyUserTests() {
        $userId = '1784';
        $data = [
            'subscriptions' => [],
            'seats' => [],
            'user' => [],
        ];
        $user = User::findOrFail( $userId );
        $boughtSubscriptions = BoughtSubscription::whereUserId( $userId ) -> where( 'paid' , '1' )  -> with( 'subscription' ) -> with( 'profile' ) -> where( 'created_at' , '>' , '2021-01-01 00:00:00' ) -> get();
        $boughtSeats = BoughtSeat::whereUserId( $userId ) -> where( 'paid' , '1' ) -> with( [ 'hall' ] ) -> where( 'created_at' , '>' , '2021-01-01 00:00:00' ) -> get();

        foreach ( $boughtSubscriptions as $key => $sub ) {
            $data[ 'subscriptions' ][] = [
                'subTitle' => $sub -> subscription -> title,
                'profileName' => $sub -> profile -> name,
                'profileType' => $sub -> profile -> type,
                'profilePhone' => $sub -> profile -> phone,
                'profileBirthday' => $sub -> profile -> birthday,
                'expires' => date( 'Y-m-d H:i:s' , strtotime( $sub -> created_at ) ),
                'date_created' => date( 'Y-m-d H:i:s' , strtotime( $sub -> expiration_time ) ),
            ];
        }
        foreach ( $boughtSeats as $key => $seat ) {
            $data[ 'seats' ][] = [
                'date_created' => date( 'Y-m-d H:i:s' , strtotime( $seat -> created_at ) ),
                'date' => date( 'Y-m-d' , strtotime( $seat -> date ) ) . ' / ' . $seat -> time ,
                // 'date' => date( 'Y-m-d' , strtotime( $seat -> date ) ) . ' ' . $seat -> time . '-' . $seat -> time_to,
                'hall' => $seat -> hall -> title,
            ];
        }
        $data[ 'user' ] = $user;
        return view( 'getMyUserTests' , $data );
        // dd($data);
        // dd($boughtSubscriptions);
    }
}
