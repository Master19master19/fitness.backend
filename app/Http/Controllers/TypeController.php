<?php

namespace App\Http\Controllers;

use App\Type;
use App\Service;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $types = Type::all();
        return view( 'type.index' , compact( 'types' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'type.create' );
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req )
    {
        $data = $req -> only( 'title' );
        Type::create( $data );
        return redirect() -> route( 'types.index' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {
        //
        return view( 'type.edit' , compact( 'type' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req , Type $type)
    {
        $data = $req -> only( 'title' );
        $res = $type -> update( $data );
        return redirect() -> route( 'types.index' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        Type::findOrFail( $id ) -> delete();
        Service::where( 'type_id' , $id ) -> update( [ 'type_id' => Type::first() -> id ]);
        return redirect() -> route( 'types.index' );
    }
}
