<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Helpers\BitrixHelper;
use App\Http\Requests\Api\UpdatePasswordRequest;



class SettingController extends Controller {
    public function maintenance () {
        return response() -> json([ 'error' => 'Функция временно недоступна. Пожалуйста, попробуйте позже.' ]);
    }
    public function updatePassword( UpdatePasswordRequest $req ) {
        $user = User::whereId( Auth::user() -> id ) -> first();
        $user -> password = bcrypt( $req -> password );
        $user -> password_plain = $req -> password;
        $user -> save();
        return response() -> json([ 'user' => $user ]);
    }
    public function updateName ( Request $req ) {
        if ( env( 'BITRIX_DOWN' ) == true ) {
            return $this -> maintenance();
        }
        $validatedData = $req -> validate([
            'name' => 'required|min:4|max:15',
        ]);
        $user = User::whereId( Auth::user() -> id ) -> first();
        $user -> name = $req -> name;
        $res = BitrixHelper::updateUser( $user );
        if ( ! $res ) {
            return response() -> json( [ 'Произошла ошибка, пожалуйста попробуйте позже' ] , 422 );
        }
        $user -> save();
        return response() -> json( [ 'status' => 'ok' ] );
    }
    public function updateEmail ( Request $req ) {
        if ( env( 'BITRIX_DOWN' ) == true ) {
            return $this -> maintenance();
        }
        $validatedData = $req -> validate([
            'email' => 'required|email|min:7|max:40',
        ]);
        $user = User::whereId( Auth::user() -> id ) -> first();
        $user -> email = $req -> email;
        $res = BitrixHelper::updateUser( $user );
        if ( ! $res ) {
            return response() -> json( [ 'Произошла ошибка, пожалуйста попробуйте позже' ] , 422 );
        }
        $user -> save();
        return response() -> json( [ 'status' => 'ok' ] );
    }
    public function updatePhone ( Request $req ) {
        if ( env( 'BITRIX_DOWN' ) == true ) {
            return $this -> maintenance();
        }
        $validatedData = $req -> validate([
            'phone' => 'required|string|min:18|max:18|unique:users',
        ]);
        $user = User::whereId( Auth::user() -> id ) -> first();
        $oldPhone = $user -> phone;
        $user -> phone = $req -> phone;
        $res = BitrixHelper::updateUser( $user , $oldPhone );
        if ( ! $res ) {
            return response() -> json( [ 'Произошла ошибка, пожалуйста попробуйте позже' ] , 422 );
        }
        $user -> save();
        return response() -> json( [ 'status' => 'ok' ] );
    }

    public function checkEmailChangeCode ( Request $req ) {
        if ( env( 'BITRIX_DOWN' ) == true ) {
            return $this -> maintenance();
        }
        $validatedData = $req -> validate([
            'code' => 'required|numeric|digits:4',
        ]);
        $user = User::whereId( Auth::user() -> id ) -> first();
        if ( $req -> code == $user -> verify_code ) {
            $user -> email = $req -> email;
            $res = BitrixHelper::updateUser( $user );
            if ( ! $res ) {
                return response() -> json( [ 'Произошла ошибка, пожалуйста попробуйте позже' ] , 422 );
            }
            $user -> save();
            return response() -> json( [ 'status' => 'ok' ] );
        } else {
            return response() -> json( [ 'error' => __( 'Wrong code' ) ] , 422 );
        }
    }

    public function requestEmailChangeCode ( Request $req ) {
        if ( env( 'BITRIX_DOWN' ) == true ) {
            return $this -> maintenance();
        }
        $validatedData = $req -> validate([
            'email' => 'required|email|unique:users',
        ]);
        $email = $req -> email;
        $code = rand( 1000 , 9999 );
        \App\Helpers\Mail::sendMail( 'Код для смены почты:' , $code , $email );
        User::whereId( Auth::user() -> id ) -> update([ 'verify_code' => $code ]);
        return response() -> json( [ 'status' => 'ok' ] );
    }
    public function changePhone( Request $req ) {
        if ( env( 'BITRIX_DOWN' ) == true ) {
            return $this -> maintenance();
        }
        $validatedData = $req -> validate([
            'phone' => 'required|string|min:17|max:18',
        ]);
        $user = User::whereId( Auth::user() -> id ) -> first();
        $check = User::wherePhone( $req -> phone ) -> first();
        if ( null !== $check ) {
            return response() -> json( [ 'error' => 'Пользователь с таким номером телефона уже существует' ] , 422 );
        }
        $oldPhone = $user -> phone;
        $user -> phone = $req -> phone;
        $res = BitrixHelper::updateUser( $user , $oldPhone );
        if ( ! $res ) {
            return response() -> json( [ 'error' => 'Произошла ошибка, пожалуйста попробуйте позже' ] , 422 );
        }
        $user -> save();
        return response() -> json( [ 'status' => 'ok' ] );
    }
}