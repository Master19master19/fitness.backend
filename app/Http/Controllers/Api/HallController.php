<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\HallHelper;
use App\Hall;
use App\HallImages;

class HallController extends Controller
{
    public function index ( Request $req , $hallId , $dateString ) {
        $response = HallHelper::getHall( $hallId , $dateString );
        return response() -> json( $response );
    }

    public function getHalls() {
        $halls = Hall::whereActive( 1 ) -> get() -> toArray();
        foreach ( $halls as $key => $hall ) {
            $images = HallImages::whereHallId( $hall[ 'id' ] ) -> select( 'url' ) -> get() -> toArray();
            array_unshift( $images , [ 'url' => $hall[ 'image_url' ] ] );
            $halls[ $key ][ 'images' ] = $images;
        }
        return response() -> json( $halls );
        // $halls = Hall::whereActive( 1 ) -> get() -> toArray();
        // return response() -> json( $halls );
    }

    public function getHallImages( $hallId ) {
        $response = [[ 'url' => Hall::whereId( $hallId ) -> first() -> image_url ]];
        $halls = HallImages::whereHallId( $hallId ) -> select( 'url' ) -> get() -> toArray();
        foreach ( $halls as $hall ) {
            $response[] = [ 'url' => $hall[ 'url' ] ];
        }
        return response() -> json( $response );
    }
    public function getClubImages () {
        $response = [
            [
                'url' => 'https://fitness.tennis.ru/club/5.jpg',
                'thumb_url' => 'https://fitness.tennis.ru/club/thumb/5.jpg',
            ],
            [
                'url' => 'https://fitness.tennis.ru/club/1.jpg',
                'thumb_url' => 'https://fitness.tennis.ru/club/thumb/1.jpg',
            ],
            [
                'url' => 'https://fitness.tennis.ru/club/2.jpg',
                'thumb_url' => 'https://fitness.tennis.ru/club/thumb/2.jpg',
            ],
            [
                'url' => 'https://fitness.tennis.ru/club/3.jpg',
                'thumb_url' => 'https://fitness.tennis.ru/club/thumb/3.jpg',
            ],
            [
                'url' => 'https://fitness.tennis.ru/club/4.jpg',
                'thumb_url' => 'https://fitness.tennis.ru/club/thumb/4.jpg',
            ],
            [
                'url' => 'https://fitness.tennis.ru/club/6.jpg',
                'thumb_url' => 'https://fitness.tennis.ru/club/thumb/6.jpg',
            ],
            [
                'url' => 'https://fitness.tennis.ru/club/7.jpg',
                'thumb_url' => 'https://fitness.tennis.ru/club/thumb/7.jpg',
            ],
            [
                'url' => 'https://fitness.tennis.ru/club/8.jpg',
                'thumb_url' => 'https://fitness.tennis.ru/club/thumb/8.jpg',
            ],
            [
                'url' => 'https://fitness.tennis.ru/club/9.jpg',
                'thumb_url' => 'https://fitness.tennis.ru/club/thumb/9.jpg',
            ],
            [
                'url' => 'https://fitness.tennis.ru/club/10.jpg',
                'thumb_url' => 'https://fitness.tennis.ru/club/thumb/10.jpg',
            ],
            [
                'url' => 'https://fitness.tennis.ru/club/11.jpg',
                'thumb_url' => 'https://fitness.tennis.ru/club/thumb/11.jpg',
            ],
            [
                'url' => 'https://fitness.tennis.ru/club/12.jpg',
                'thumb_url' => 'https://fitness.tennis.ru/club/thumb/12.jpg',
            ],
            [
                'url' => 'https://fitness.tennis.ru/club/13.jpg',
                'thumb_url' => 'https://fitness.tennis.ru/club/thumb/13.jpg',
            ],
            [
                'url' => 'https://fitness.tennis.ru/club/14.jpg',
                'thumb_url' => 'https://fitness.tennis.ru/club/thumb/14.jpg'
            ],
            [
                'url' => 'https://fitness.tennis.ru/club/15.jpg',
                'thumb_url' => 'https://fitness.tennis.ru/club/thumb/15.jpg'
            ],
        ];
        return response() -> json( $response );
        $halls = Hall::whereActive( 1 ) -> select( 'id' , 'title' , 'image_url' ) -> get();
        foreach ( $halls as $hall ) {
            $response[] = [ 'url' => $hall -> image_url , 'id' => $hall -> id , 'hallTitle' => $hall -> title ];
        }
        return response() -> json( $response );
    }
    public function getHallMainImages() {
        $response = [];
        $halls = Hall::whereActive( 1 ) -> select( 'id' , 'title' , 'image_url' ) -> get();
        foreach ( $halls as $hall ) {
            $response[] = [ 'url' => $hall -> image_url , 'id' => $hall -> id , 'hallTitle' => $hall -> title ];
        }
        return response() -> json( $response );
    }
}