<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\BoughtSubscription;
use App\Profile;
use App\BoughtSeat;
use App\ReservedSeat;
use App\Seat;
use App\Subscription;
use App\Order;
use App\User;
use App\Helpers\Helper;
use App\Helpers\PaymentHelper;
use App\Helpers\BitrixSendPaymentHelper;
use App\Helpers\BitrixCancelReserveHelper;
use App\Helpers\BitrixReserveHelper;
use App\Helpers\DiscountHelper;
use App\AppPayment;
use App\Card;
use App\Helpers\PushHelper;

class PaymentController extends Controller {



    public function payCart ( Request $req ) {
        if ( env( 'BITRIX_DOWN' ) == true ) {
            return response() -> json( [ 'error' => 'Ведутся технические работы, приложение временно не принимает оплату. Пожалуйста, попробуйте позже.' ]);
        }
        // file_put_contents( __DIR__ . '/aaa' , json_encode( $req->all()));
        $this -> log( $req -> all() );
        $seats = $req -> cartTotal[ 'seats' ] ?? [];
        $subscriptions = $req -> cartTotal[ 'subscriptions' ] ?? [];
        $totalPrice = 0;
        $orderUniqueId = uniqid();
        $order = Order::create([
            'unique' => $orderUniqueId,
            'user_id' => Auth::user() -> id,
            'card_id' => $req -> cardId
        ]);

        // exit;
        // dd($seats);
        if ( count( $seats ) ) {
            $errors = $this -> checkIfErrorSeats( $seats );
            if ( $errors === 'wait_20_minutes' ) {
                return response() -> json( [ 'error' => $this -> getError( 'wait_20_minutes' ) ] );
            }
            if ( count( $errors ) ) {
                foreach ( $errors as $key => $error ) {
                    if ( isset( $error[ 'old_reserve' ] ) ) {
                        BitrixCancelReserveHelper::cancelReserve( $error[ 'old_reserve' ] , Auth::user() );
                        unset( $errors[ $key ] );
                    }
                }
                if ( count( $errors ) ) {
                    return response() -> json( [ 'error' => $this -> getError( 'bought' , $errors ) ] );
                }
            }
            $handleResp = $this -> handleSeats( $seats , $order -> id );
            if ( $handleResp == 'reserved_already' ) {
                return response() -> json( [ 'error' => $this -> getError( 'reserved_already' ) ] );
            }
            $totalPrice += $handleResp;
        }

        // DO NOT MOVE
        if ( count( $subscriptions ) ) {
            $subscriptionResponse = $this -> handleSubscriptions( $subscriptions , $order -> id , true );
            if ( (int)( $subscriptionResponse ) == 0 ) {
                return response() -> json( [ 'error' => $subscriptionResponse ] );
            } else {
                $totalPrice += $subscriptionResponse;
            }
        }
        // DO NOT MOVE

        if ( $req -> cardId !== 0 ) {
            Order::find( $order -> id ) -> update([
                'price' => $totalPrice,
            ]);
            $response = PaymentHelper::payWithCard( $totalPrice , Auth::user() -> phone , $req -> cardId , $order -> id , Auth::user() -> email );
            if ( $response ) {
                // $this -> orderSuccess( $order -> id );
                return response() -> json( [ 'done' => 1 ] );
            } else {
                return response() -> json( [ 'error' => $this -> getError( 'card_invalid' ) ] );
            }
        } else {
            $response = PaymentHelper::createPayment( $totalPrice , Auth::user() -> phone , false , Auth::user() -> email );
            Order::find( $order -> id ) -> update([
                'price' => $totalPrice,
                'key' => $response[ 'payment_id' ]
            ]);
        }
        return response() -> json( $response );
    }





    protected function handleSeats ( $seats , $orderId ) {
        $totalPrice = 0;
        $allSeats = [];
        foreach ( $seats as $weirdKey => $seat ) {
            $price = Helper::getPrice( $seat[ 'hallId' ] , $seat[ 'date' ] , $seat[ 'time' ] );
            $time_to = Helper::getTimeTo( $seat[ 'time' ] );
            $allSeats[] = [
                'order_id' => $orderId,
                'hall_id' => $seat[ 'hallId' ],
                'unique' => $seat[ 'unique' ],
                'time' => $seat[ 'time' ],
                'time_to' => $time_to,
                'date' => $seat[ 'date' ],
                'price' => $price,
                'ip' => $_SERVER[ 'REMOTE_ADDR' ],
                'user_id' => Auth::user() -> id,
                'reserved_until' => strtotime( env( 'RESERVE_TIME' ) ),
                'created_at' => date( 'Y-m-d H:i:s' ),
                'updated_at' => date( 'Y-m-d H:i:s' ),
            ];
            $totalPrice += ( float ) $price;
        }
        $res = BitrixReserveHelper::send( $allSeats );
        if ( ! $res ) {
            return 'reserved_already';
        }
        BoughtSeat::insert( $allSeats );
        return $totalPrice;
    }






    protected function checkIfErrorSeats ( $seats ) {
        $errors = [];
        foreach ( $seats as $weirdKey => $seat ) {
            $isBoughtCheck = [
                'unique' => $weirdKey,
                'paid' => 1
            ];

            // IF IS BOUGHT OR IS RESERVED
            $isBought = BoughtSeat::where( $isBoughtCheck ) -> count();
            if ( $isBought ) {
                $errors[] = $seat;
            } else {
                unset( $isBoughtCheck[ 'paid' ] );
                $isReserved = BoughtSeat::where( $isBoughtCheck ) -> orderBy( 'id' , 'DESC' ) -> first();
                if ( null !== $isReserved && $isReserved -> reserved_until - time() > 0 ) {
                    if ( $isReserved -> user_id != Auth::user() -> id ) {
                        $errors[] = $seat;
                    } else {
                        $errors[] = [ 'old_reserve' => $isReserved ];
                    }
                }
            }
            // IF IS BOUGHT OR IS RESERVED

            // IF IS BUYING
            $isBuying = BoughtSeat::where([
                'unique' => $weirdKey,
                'user_id' => Auth::user() -> id,
            ]) -> orderBy( 'id' , 'DESC' ) -> first();
            if ( $isBuying != null ) {
                if ( strtotime( $isBuying -> created_at ) - strtotime( '-15 minutes' ) > 0 ) {
                    return 'wait_20_minutes';
                }
            }
            // IF IS BUYING

        }
        return $errors;
    }



















































    public function showPayment ( Request $req , $confirmation_token ) {
        return view( 'payment.index' , [ 'confirmation_token' => $confirmation_token , 'isCard' => false ] );
    }

    public function showPaymentCard ( Request $req , $confirmation_token ) {
        return view( 'payment.index' , [ 'confirmation_token' => $confirmation_token , 'isCard' => true ] );
    }
    public function index ( Request $req ) {
        $response = PaymentHelper::createPayment( 10 );
        return $response;
    }
    public function loading( Request $req ) {
        file_put_contents(__DIR__.'/aa' , json_encode( $req->all()).PHP_EOL , FILE_APPEND);
        return view( 'loading' );
    }
    
    public function testMail ( Request $req ) {
        $order = Order::where( [ 'id' => $req -> id ] ) -> firstOrFail();
        $this -> sendMail( $order );
    }
    
    protected function sendMail ( $order ) {
        $user = User::findOrFail( $order -> user_id ) -> toArray();
        $finalData = [
            'boughtSubscriptions' => [],
            'boughtSeats' => [],
            'user' => $user,
            'order' => $order
        ];
        $boughtSubscriptions = BoughtSubscription::where( [ 'order_id' => $order -> id ] ) -> with( 'subscription' ) -> with( 'profile' ) -> get() -> toArray();
        if ( null !== $boughtSubscriptions ) {
            foreach ( $boughtSubscriptions as $key => $value ) {
                $finalData[ 'boughtSubscriptions' ][] = $value;
            }
        }
        $boughtSeats = BoughtSeat::where( [ 'order_id' => $order -> id ] ) -> orderBy( 'hall_id' , 'ASC' )-> orderBy( 'time' , 'ASC' ) -> with( [ 'hall' ] ) -> get() -> toArray();
        if ( null !== $boughtSeats ) {
            foreach ( $boughtSeats as $key => $value ) {
                $finalData[ 'boughtSeats' ][] = $value;
            }
        }
        // dd($finalData);
        \App\Helpers\Mail::sendToAdmin( $finalData );
    }


    protected function orderSuccess ( $orderId ) {
        $order = Order::where( [ 'id' => $orderId ] ) -> firstOrFail();
        // if ( $order -> paid == '1' ) {
            // file_put_contents( __DIR__.'/already.paid' , json_encode( [$response,date('Y-m-d H:i:s')] ) .PHP_EOL , FILE_APPEND );
            // return;
        // } 
        BoughtSeat::where( [ 'order_id' => $order -> id ] ) -> update([ 'paid' => 1 ]);
        BoughtSubscription::where( [ 'order_id' => $order -> id ] ) -> update([ 'paid' => 1 ]);
        BitrixSendPaymentHelper::send( $order -> id , $order -> key );
        $this -> sendMail( $order );
    }





    public function YAPayment ( Request $req ) {
        $response = $req -> object;
        file_put_contents( __DIR__.'/logs/kassa.' . date( 'm_d' ) , json_encode( [$req->all(),date('Y-m-d H:i:s')] ) .PHP_EOL , FILE_APPEND );
        // file_put_contents( __DIR__.'/kassa.jk' , json_encode( [$response,date('Y-m-d H:i:s')] ) .PHP_EOL , FILE_APPEND );
        // return;
        if ( $response[ 'paid' ] == true ) {
            $order = Order::where( [ 'key' => $response[ 'id' ] ] ) -> firstOrFail();
            if ( $order -> paid == '1' ) {
                return response() -> json( [ 'status' => 'success' ] , 200 );
            }
            $order -> paid = '1';
            $order -> save();
            if ( $response[ 'amount' ][ 'value' ] == env( 'ADD_CARD_PRICE' )  ) {
                $this -> saveCardIfNeeded( $response , $order );
            } else {
                $this -> orderSuccess( $order -> id );
                $this -> saveCardIfNeeded( $response , $order );
            }
        } else if ( $response[ 'paid' ] == false ) { // hahahaaaaa , its a boolean , but wait , its programming, everything is possible )))))))
            if ( $response[ 'amount' ][ 'value' ] > env( 'ADD_CARD_PRICE' )  ) {
                $order = Order::where( [ 'key' => $response[ 'id' ] ] ) -> firstOrFail();
                $this -> pushPaymentError( $order );
                BitrixCancelReserveHelper::cancelReserveByOrderId( $order -> id );
            }
        }
        return response()->json( [ 'status' => 'success' ] , 200 );
    }



    protected function saveCardIfNeeded ( $response , $order ) {
        if ( isset( $response[ 'payment_method' ] ) && $response[ 'payment_method' ][ 'saved' ] == true ) {
            $paymentMethod = $response[ 'payment_method' ];
            $title = $paymentMethod[ 'card' ][ 'first6' ] . "******" . $paymentMethod[ 'card' ][ 'last4' ];
            try {
                $card = Card::create([
                    'user_id' => $order -> user_id,
                    'order_id' => $order -> id,
                    'token' => $paymentMethod[ 'id' ],
                    'title' => $title,
                ]);
            } catch ( \Exception $e ) {
                file_put_contents( __DIR__ . '/somecardfail' , json_encode( $e -> getMessage() ) );
                return true;
            }
        } else if ( isset( $response[ 'payment_method' ] ) && $response[ 'payment_method' ][ 'type' ] == 'apple_pay' ) { // APPLE PAY NOTIFICATION HANDLER
			$data = [];
			$data[ 'title' ] = "Уведомление об успешной оплате";
			$data[ 'body' ] = "Ваша оплата прошла успешно";
			PushHelper::push( $order -> user_id , $data );
        	$user = User::whereId( $order -> user_id ) -> first();
        	$user -> clean_cart = 1;
        	$user -> save();
        }
    }
    protected function pushPaymentError ( $order ) {
        $data = [];
        $data[ 'title' ] = "Уведомление о неуспешной оплате";
        $data[ 'body' ] = "Нам не удалось обработать ваш платеж, пожалуйста попробуйте еще раз.";
        // PushHelper::push( $order -> user_id , $data );
    }


























    public function addCard ( Request $req ) {
        $totalPrice = env( 'ADD_CARD_PRICE' );
        $orderUniqueId = uniqid();
        $order = Order::create([
            'unique' => $orderUniqueId,
            'user_id' => Auth::user() -> id,
        ]);
        $response = PaymentHelper::createPayment( $totalPrice , Auth::user() -> phone , true );
        // $card = Card::create([
        //     'user_id' => Auth::user() -> id,
        //     'order_id' => $order -> id,
        // ]);
        Order::find( $order -> id ) -> update([
            'price' => $totalPrice,
            'key' => $response[ 'payment_id' ]
        ]);
        return response() -> json( $response );
    }


    protected function getError ( $code , $data = null ) {
        if ( $code == 'reserved_already' ) {
            // return 'reserved_already';
            return 'К сожалению в заданном промежутке времени уже есть забронированные занятия';
        } else if ( $code == 'wait_20_minutes' ) {
            // return '20 min';
            return "Убедитесь, что Вы только что не оплачивали данный заказ.\nЕсли Вы уже оплатили заказ, то в течение 15 минут забронированные залы отобразятся в приложении у Вас в профиле.";
        } else if ( $code == 'bought' ) {
            $message = '';
            foreach ( $data as $seat ) {
                $message .= $seat[ 'hallTitle' ] . " в " . $seat[ 'time' ] . " часа " . $seat[ 'date' ] . " больше недоступен\n\n";
            }
            $message .= "\nПожалуйста выберите другие часы и попробуйте снова";
            return $message;
        } else if ( $code == 'card_invalid' ) {
            return 'Недостаточно средств на карте';
        }
    }




    protected function log ( $input ) {
        if ( count( $input ) ) {
            $log = new AppPayment;
            $log -> message = json_encode( $input );
            $log -> user_id = Auth::user() -> id;
            $log -> save();
        }
    }



    protected function handleSubscriptions ( $subscriptions , $orderId , $isPayment = false ) {
        $allSubscriptions = [];
        $totalPrice = DiscountHelper::handleSubscriptions( $subscriptions , $orderId , $isPayment );
        return $totalPrice;
    }



}
