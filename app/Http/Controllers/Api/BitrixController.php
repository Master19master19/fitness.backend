<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\BitrixSeatHelper;
use App\Helpers\BitrixUserHelper;
use App\Helpers\BitrixSubscriptionHelper;
use App\Helpers\BitrixRentHelper;
use App\Helpers\LogHelper;


class BitrixController extends Controller {
    public function inhale ( Request $req ) {
        usleep( 700000 );
        file_put_contents( __DIR__ . '/inhale.json' , date( 'Y-m-d H:i:s') . ' ' . json_encode( $req -> all() ) . PHP_EOL , FILE_APPEND );
        $class = $req -> input( 'class' );
        $input = $req -> all();
        if ( env( 'BITRIX_DOWN' ) == true ) {
            exit('virus');
        }
        LogHelper::bitrix_in_init( $input );
        if ( $class === 'chargingRent' ) {
            $res = BitrixSeatHelper::handleRent( $input );
            return $this -> handleResponse( $res , $input );
        } else if ( $class === 'partner' ) {
            $res = BitrixUserHelper::handleUser( $input );
            return $this -> handleResponse( $res , $input );
        } else if ( $class == 'membership' ) {
            $res = BitrixSubscriptionHelper::handleSubscription( $input );
            return $this -> handleResponse( $res , $input );
        } else if ( $class == 'rent' ) {
            $res = BitrixRentHelper::handleRent( $input );
            return $this -> handleResponse( $res , $input );
        }
    }
    protected function handleResponse ( $data , $input ) {
        if ( $data[ 'result' ] == false ) {
            return $this -> throwErr( $data , $input );
        } else {
            return $this -> throwSuccess( $data , $input );
        }
    } 
    protected function throwErr ( $data , $input ) {
        LogHelper::bitrix_in( $input , 1  , $data[ 'error' ] );
        return response() -> json( $data );
    }
    protected function throwSuccess ( $data , $input ) {
        LogHelper::bitrix_in( $input );
        return response() -> json( $data );
    }
}