<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests\Api\FeedBackRequest;
use App\User;
use App\Subscription;
use App\Helpers\DiscountHelper;
use App\BoughtSubscription;
use App\PaymentLog;


class OtherController extends Controller {
    public function rules () {
        return view( 'mobile.rules' );
    }
    public function terms () {
        return view( 'mobile.terms' );
    }
    public function needCleanCart () {
        return response() -> json([ 'res' => Auth::user() -> clean_cart == 1 ]); 
    }
    public function clearCleanCart () {
        $user = User::whereId( Auth::user() -> id ) -> firstOrFail();
        $user -> clean_cart = 0;
        $user -> save();
        return response() -> json([ true ]); 
    }
    public function getSubscriptions() {
        $subscriptions = Subscription::whereActive( 1 ) -> orderBy( 'sort_order' ) -> get() -> toArray();
        // $subscriptions = Subscription::whereActive( 1 ) -> where( 'is_new_version' , 0 ) -> get() -> toArray();
        foreach ( $subscriptions as $key => $value ) {
            $subscriptions[ $key ][ 'includes' ] = json_decode( $value[ 'includes' ] , 1 );
        }
        return response() -> json( $subscriptions );
    }
    public function getSubscriptionsNewVersion() {
        $subscriptions = Subscription::whereActive( 1 ) -> orderBy( 'sort_order' ) -> get() -> toArray();
        foreach ( $subscriptions as $key => $value ) {
            $subscriptions[ $key ][ 'includes' ] = json_decode( $value[ 'includes' ] , 1 );
        }
        return response() -> json( $subscriptions );
    }
    public function canBuyKidSubscription() {
        $user = Auth::user();
        $subscriptionsCount = BoughtSubscription::whereUserId( $user -> id ) -> wherePaid( 1 ) ->
        whereHas( 'subscription' , function ( $query ) {
            return $query -> where( 'type' , 'LIKE', 'adult');
        }) -> count();
        return response() -> json( $subscriptionsCount > 0 ? true : false );
    }
    public function hasDiscountSubscription( Request $req ) {
        // dd($req->all());
        $data = $req -> all();
        $output = [];
        foreach ( $data as $key => $value ) {
            $output[ $value[ 'subscription' ][ 'id' ] ] = $value;
        }
        $response = DiscountHelper::handleSubscriptions( $output );
        return response() -> json( $response );
    }
    public function getUser () {
        $user = Auth::user();
        return response() -> json( [ $user ] );
    }

    // public function mobileMap ( Request $req ) {
    //     return view( 'mobileMap' );
    // }
    public function feedback ( FeedBackRequest $req ) {
        $data = $req -> all();
        $data[ 'created_at' ] = date( 'Y-m-d H:i:s' );
        \App\Feedback::insert( $data );
        \App\Helpers\Mail::sendFeedBackToAdmin( 'Новая заявка' , $data );
        return response() -> json( [ 'status' => 'ok' ] );
    }


    
    public function setExpoPushToken ( Request $req ) {
        $pushToken = $req -> pushToken;
        $os = $req -> os;
        $user = User::whereId( Auth::user() -> id ) -> firstOrFail();
        $user -> expo_token = $pushToken;
        $user -> os = $os;
        $user -> save();
        return response() -> json([ 'status' => 'ok' ]);
        // file_put_contents( __DIR__ . '/expotoken' , json_encode($req -> all()) );
    }
    public function logPaymentUrls ( Request $req ) {
        // file_put_contents( __DIR__ . '/paylog' , json_encode($req -> all()) );
        $data = $req -> all();
        $data[ 'created_at' ] = date( 'Y-m-d H:i:s' );
        $data[ 'user_id' ] = Auth::user() -> id;
        PaymentLog::insert( $data );
        return response() -> json( true );
    }
}
