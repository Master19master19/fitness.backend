<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests\Api\ProfileRequest;
use App\Profile;
use App\BoughtSeat;
use App\BoughtSubscription;
use App\Card;


class ProfileController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function createProfileForNewUser ( $user ) {
        $data = [
            'type' => 'adult',
            'user_id' => $user -> id,
            'is_self' => 1,
            'phone' => $user -> phone,
            // 'name' => 'Вы',
            'name' => $user -> name,
        ];
        $profile = Profile::create( $data );
        return $profile;
    } 
    public function anonymousProfiles ( $type ) {
        $profiles = Profile::where( 'type' , $type ) -> get();
        return response() -> json( $profiles );
    }
    public function createProfile ( ProfileRequest $req , $type ) {
        $data = $req -> all();
        $data[ 'type' ] = $type;
        $data[ 'user_id' ] = Auth::user() -> id;
        $profile = Profile::create( $data );
        return response() -> json( $profile );
    }
    public function getProfiles ( $type ) {
        $data = [
            'user_id' => Auth::user() -> id,
            'type' => $type,
            'deleted' => 0
        ];
        if ( $type == 'all' ) {
            unset( $data[ 'type' ] );
        }
        $profiles = Profile::where( $data ) -> orderBy( 'id' , 'DESC' ) -> get();
        return response() -> json( $profiles );
    }
    public function deleteProfile ( Request $req ) {
        $profile = Profile::whereId( $req -> id ) -> firstOrFail();
        if ( $profile -> is_self == 0 ) {
            $profile -> deleted = 1;
            $profile -> save();
        }
        return response() -> json( $profile );
    }
    

    public function purchaseHistory () {
        $response = [];
        $subscriptions = BoughtSubscription::where([
            'user_id' => Auth::user() -> id,
            'paid' => 1
        ]) -> with( 'subscription' ) -> orderBy( 'created_at' , 'DESC' ) -> get() -> toArray();
        $halls = BoughtSeat::where([
            'user_id' => Auth::user() -> id,
            'paid' => 1
        ]) -> with( 'hall' ) -> orderBy( 'created_at' , 'DESC' ) -> get() -> toArray();
        foreach ( $halls as $value ) {
            $index = ( int ) strtotime( $value[ 'created_at' ] );
            while ( isset( $response[ $index] ) ) {
                $index += 1;
            }
            $value[ 'type' ] = 'hall';
            $response[ $index ] = $value;
        }
        foreach ( $subscriptions as $value ) {
            $index = ( int ) strtotime( $value[ 'created_at' ] );
            while ( isset( $response[ $index] ) ) {
                $index += 1;
            }
            $value[ 'type' ] = 'subscription';
            $response[ $index ] = $value;
        }
        krsort( $response );
        $response = array_values( $response );
        return response() -> json( $response );
    }



    public function removeCard ( $cardId ) {
        Card::whereId( $cardId ) -> delete();
        $cards = Card::where([
            'user_id' => Auth::user() -> id
        ]) -> whereNotNull( 'token' ) -> orderBy( 'created_at' , 'DESC' ) -> get() -> toArray();
        return response() -> json( $cards );
    }

    public function cards () {
        $cards = Card::where([
            'user_id' => Auth::user() -> id
        ]) -> whereNotNull( 'token' ) -> orderBy( 'created_at' , 'DESC' ) -> get() -> toArray();
        return response() -> json( $cards );
    }

    public function subscriptionsHistory () {
        $subscriptions = BoughtSubscription::where([
            'user_id' => Auth::user() -> id,
            'paid' => 1
        ]) -> where( 'created_at' , '>' , date( 'Y-m-d H:i:s' , strtotime( '-2 months' ) ) ) -> with( 'subscription' ) -> with( 'profile' ) -> orderBy( 'created_at' , 'DESC' ) -> get() -> toArray();
        return response() -> json( $subscriptions );
    }

    public function hallsHistory () {
        $halls = BoughtSeat::where([
            'user_id' => Auth::user() -> id,
            'paid' => 1
        ]) -> with( 'hall' ) -> orderBy( 'date' , 'DESC' ) -> get();
        return response() -> json( $halls );
    }
}
