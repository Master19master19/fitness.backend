<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BoughtSeat;
use App\ReservedSeat;
use App\BitrixHistory;
use App\BitrixIn;
use App\BoughtSubscription;
use App\Helpers\SMS;
use App\Helpers\BitrixHelper;
use App\Helpers\DiscountHelper;
use App\Helpers\PaymentHelper;
use App\Helpers\BitrixSendPaymentHelper;
use App\Helpers\BitrixCancelReserveHelper;
use App\Helpers\PushHelper;
use App\Order;
use App\User;

class TestController extends Controller
{
	// public function sendFifteenPush() {
	// 	$text = "Бассейн открыт! Приходите плавать и загорать. Приобретайте карты в приложении.";
	// 	$users = User::whereNotNull( 'expo_token' ) -> where( 'fifteen_sent' , 0 )
 // // -> whereId( 1784 )
	// 	 -> get();
	// 	// dd($users);
	// 	foreach ($users as $key => $user ) {
	// 		$data = [];
	// 		$data[ 'title' ] = "Профилактика бассейна";
	// 		$data[ 'body' ] = $text;
	// 		$data[ 'data' ] = '';
	// 		$userId = $user -> id;
	// 		$user -> fifteen_sent = 1;
	// 		$user -> save();
	// 		$res = PushHelper::push( $userId , $data );
	// 		var_dump($res);
	// 		// exit;
	// 		usleep(500000);
	// 	}
	// 	exit;
	// }
	public function testPay() {
		PaymentHelper::payTest();
	}
	public function testNotification() {
		$data = [];
			$data[ 'title' ] = "Уведомление об успешной оплате";
			$data[ 'body' ] = "Ваша оплата прошла успешно";
			$data[ 'data' ] = 'clean_cart';
			PushHelper::push( 1784 , $data );
	}
	public function paymentConfirmRedir() {
		header( 'Location: https://fitness.tennis.ru/api/paymentConfirm' );
		exit;
	}
	public function applePay() {
	    $response = PaymentHelper::payTest( 10 );
	    header('Location: ' . $response );
	    exit;
	    dd($response);
	    return view( 'payment.index' , [ 'confirmation_token' => $response, 'isCard' => false ] );
	    exit;
	    return response() -> json(['url' => $response]);
	    dd($response);
		return view( 'test.apple' );
	}
	public function redoBitrixHistory () {
		exit;
        $client = new \GuzzleHttp\Client();
		$rows = BitrixIn::where( 'message' , 'LIKE' , "%chargingRent%" ) -> get();
		foreach ( $rows as $key => $row ) {
            sleep( 1 );
	        $response = $client -> request( 'POST' , 'https://fitness.tennis.ru/api/1c' , [
	        	'timeout' => 10,
	            'json' => json_decode( $row -> message ),
	        ]);
	        // dd($response);
			// dd($row -> message );
			// exit;
		}
		dd($rows);
	}
	public function testSub() {
		DiscountHelper::check();
		exit;
        $subs = BoughtSubscription::where( 'user_notified' , 0 ) -> wherePaid( 1 ) -> whereDate( 'expiration_time' , '=' , date( 'Y-m-d' ) ) -> get();
		// exit;
		echo "<pre>";
		echo date( 'Y-m-d ' );
		echo "<br>";
		$subsbak = BoughtSubscription::wherePaid( 1 )
		 // -> whereUserId( 1784 )
		 -> orderBy( 'id' , "DESC" ) -> take( 5 ) -> get();
		foreach ( $subs as $sub ) {
			echo '<br>';
			echo date( 'Y-m-d' , strtotime( $sub -> expiration_time ) );
			echo '<br>';
			echo $sub -> has_discount ? 'has' : 'diesnt have';
			echo '<br>';
		}
	}
	public function payee() {
        BitrixSendPaymentHelper::send( 6376 );
	}
	public function smss() {
		SMS::sendSMS( 'testasffsa' , 'descsss' , '+37477797590' );
	}
    public function mutate () {
        $boughtSeats = BoughtSeat::where([
            'paid' => 1,
            'checked' => 0
        ]) -> orderBy( 'id', 'DESC' ) -> take( 10 ) -> get();
        foreach ( $boughtSeats as $boughtSeat ) {
	        $res = BitrixSendPaymentHelper::send($boughtSeat->order_id);
	        if( $res == true ) {
	        	BoughtSeat::where( 'order_id' , $boughtSeat -> order_id ) -> update( [ 'checked' => 1 ]);
	        	// $boughtSeat -> checked = 1;
	        	// $boughtSeat -> save();
	        	// dd('cool',$res);
	        } else {
	        	dd('error',$res);
	        }
        }
        exit( 'done' );
    }
    //
    public function mutateeee () {
    	dd(date('Y-m-d H:i:s'));
    	$json = '{"class":"rent","newReserve":[{"courtId":"13","startTime":"2020-06-01T12:00:00","finishTime":"2020-06-01T12:59:59"},{"courtId":"13","startTime":"2020-06-01T13:00:00","finishTime":"2020-06-01T13:59:59"}],"phone":"+7916128","changeDate":"2020-05-30T13:03:53"}';
    	$res = json_decode( $json , 1 );
    	$res = BitrixHelper::send( $res );
    	dd('ok', $res );
    	// exit('pok');
        // BitrixCancelReserveHelper::check();
        // exit;
    	// $rr=\DB::getQueryLog();
    	// dd($rr);
       // exit();
    	$dat = '{"class":"rent","newReserve":[{"courtId":"9","startTime":"2020-06-01T14:00:00","finishTime":"2020-06-01T14:59:59"},{"courtId":"9","startTime":"2020-06-02T09:00:00","finishTime":"2020-06-02T09:59:59"},{"courtId":"9","startTime":"2020-06-03T14:00:00","finishTime":"2020-06-03T14:59:59"},{"courtId":"9","startTime":"2020-06-04T09:00:00","finishTime":"2020-06-04T09:59:59"},{"courtId":"9","startTime":"2020-06-05T14:00:00","finishTime":"2020-06-05T14:59:59"},{"courtId":"9","startTime":"2020-06-06T09:00:00","finishTime":"2020-06-06T09:59:59"}],"phone":"+79688676704","changeDate":"2020-05-29T16:10:47"}';
    	$res = json_decode( $dat , 1 );
    	// dd($res);
    	$ress = BitrixHelper::send( $res );
    	dd($ress);
    }
    public function mutateB () {
    	$orders = Order::wherePaid( '1' ) -> where( 'paidd' , 0 ) -> where( 'created_at' , '>' , '2020-05-29 15:00:44' ) -> get();
    	foreach ( $orders as $key => $order ) {
        	BitrixSendPaymentHelper::send( $order -> id );
        	Order::whereId( $order -> id ) -> update( [ 'paidd' => 1 ] );
        	dd( $order );
        	sleep( 1 );
    	}
    }
    public function mutateC() {
		$res = BitrixHistory::orderBy( 'id' , 'DESC' ) -> take( 10 ) -> get();
		foreach ( $res as $record ) {
			$data = $record -> data;
			$dat = json_decode( $data , 1 );
			if ( isset ( $dat[ 'items' ] ) ) {
				foreach ($dat['items'] as $key => $value) {
					$startTime = $value[ 'startTime' ];
					if ( strpos( $startTime , 'T' ) === false ) {
						$startTime = "0000:00:00T" . $startTime;
					}
					$finishTime = $value[ 'finishTime' ];
					if ( strpos( $finishTime , 'T' ) === false ) {
						$finishTime = "0000:00:00T" . $finishTime;
					}
					$value['startTime']=$startTime;
					$value['finishTime']=$finishTime;
					$dat[ 'items' ][ $key ] = $value;
				}	
			}
			// $record 
			$res = BitrixHelper::send( $dat , 'checking' );
			// dd($dat,$record);
			if ( $res === true ) {
				BitrixHistory::whereId( $record -> id ) -> delete();
			} else {
				dd($res);
				BitrixHistory::whereId( $record -> id ) -> update([ 'tries' => $record -> tries + 1 ]);
			}
			exit;
			sleep( 1 );
		}
    }
    public function tmail() {
    	$data = [
    		'title' => 'rest',
    		'desc' => 'restdesc',
    	];
		$rres = \Mail::send( 'mail.send' , $data , function( $message ) {
			$message -> to( 'vmxitaryan@gmail.com' );
			$message -> subject( 'subjecting@gmail.com' );
		});
		dd($rres);

			// $mailSMTP = new SendMailSmtpClass('app@tennis.ru', 'Gny39We6', 'ssl://smtp.yandex.ru', 465);
			// // $mailSMTP = new SendMailSmtpClass('логин', 'пароль', 'хост', 'имя отправителя');

			// // заголовок письма
			// $headers= "MIME-Version: 1.0\r\n";
			// $headers .= "Content-type: text/html; charset=utf-8\r\n"; // кодировка письма
			// // $headers .= "From: Tennis.ru <app@tennis.ru>\r\n"; // от кого письмо
			// $result =  $mailSMTP->send('vmxitaryan@gmail.com', 'Тема письма', 'Текст письма', $headers); // отправляем письмо
			// // $result =  $mailSMTP->send('Кому письмо', 'Тема письма', 'Текст письма', 'Заголовки письма');
			// if($result === true){
			// echo "Письмо успешно отправлено";
			// }else{
			// echo "Письмо не отправлено. Ошибка: " . $result;
			// }
    // 	  $to = "vmxitaryan@gmail.com";
		  // $subject = "Robot - Робот";
		  // $message = "Message,\n сообщение!";
		  // $headers = "From: Tennis.ru <app@tennis.ru>\r\nContent-type: text/plain; charset=utf-8 \r\n";
		  // mail ($to, $subject, $message, $headers);

    }
}
