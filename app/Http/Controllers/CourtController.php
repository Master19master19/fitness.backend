<?php

namespace App\Http\Controllers;

use App\Court;
use App\Seat;
use App\Arena;
use App\BoughtSeat;
use Illuminate\Http\Request;
use App\Helpers\BitrixHelper;

class CourtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function editor() {
        $seatz = Seat::all();
        $seats = [];
        foreach ( $seatz as $key => $value ) {
            $seats[ $value -> arena_id ][ $value -> court_id ][ $value -> time ] = [ $value[ 'price' ] , $value[ 'price_weekend' ] ];
        }
        return view( 'editor' , compact( 'seats' ) );
    }
    public function boughtSeatsPost ( Request $req ) {
        $data = $req -> all();
        $data[ 'created_at' ] = date( 'Y-m-d H:i:s' );
        $data [ 'paid' ] = 1;
        $res = BoughtSeat::insert( $data );
        // dd($res);
        return response() -> json( $res );
        $oldSeat = Seat::where([
            'time' => $req -> time,
            'arena_id' => $req -> arena_id,
        ]) -> first();
        if ( null === $oldSeat ) {
            $seat = Seat::create( $req -> all() );
        } else {
            $oldSeat -> update( $req -> all() );
        }
        return response() -> json([ 'status' => 1 ]);
    }
    public function editorPost ( Request $req ) {
        $oldSeat = Seat::where([
            'time' => $req -> time,
            'arena_id' => $req -> arena_id,
            'court_id' => $req -> court_id
        ]) -> first();
        if ( null === $oldSeat ) {
            $seat = Seat::create( $req -> all() );
        } else {
            $oldSeat -> update( $req -> all() );
            $seat = $oldSeat;
        }
        // $arena = Arena::findOrFail( $req -> arena_id );
        // BitrixHelper::updateSeat( $seat , $req -> has( 'price' ) ? 'week' : 'weekend' , $arena );
        return response() -> json([ 'status' => 1 ]);
    }
    public function index () {
        $courts = Court::all();
        return view( 'court.index' , compact( 'courts' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'court.create' );
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $data = $req -> only( 'title' , 'arena_id' );
        $res = Court::create( $data );
        return redirect() -> route( 'courts.index' );
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Court  $court
     * @return \Illuminate\Http\Response
     */
    public function show(Court $court)
    {
        //
        return view( 'court.show' , compact( 'court' ) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Court  $court
     * @return \Illuminate\Http\Response
     */
    public function edit(Court $court)
    {
        //
        return view( 'court.edit' , compact( 'court' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @param  \App\Court  $court
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, Court $court)
    {
        $data = $req -> only( 'title' , 'arena_id' );
        $res = $court -> update( $data );
        return redirect() -> route( 'courts.index' );
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Court  $court
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        Court::findOrFail( $id ) -> delete();
        return redirect() -> route( 'courts.index' );
        //
    }
}
