<?php

namespace App\Http\Controllers;

use App\Seat;
use Illuminate\Http\Request;

class SeatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index () {
        $seats = Seat::has( 'arena' ) -> get();
        return view( 'seat.index' , compact( 'seats' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'seat.create' );
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @return \Illuminate\Http\Response
     */
    protected function simplifyInput ( $time ) {
        if ( count( explode ( ":00" , $time ) ) == 1 ) {
            return $time . ":00";
        }
        return $time;
    }
    public function store(Request $req)
    {
        $data = $req -> only( 'time' , 'price' , 'price_weekend' , 'court_id' );
        $data[ 'time' ] = $this -> simplifyInput( $data[ 'time' ] );
        $res = Seat::create( $data );
        return redirect() -> route( 'seats.index' );
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Seat  $seat
     * @return \Illuminate\Http\Response
     */
    public function show(Seat $seat)
    {
        //
        return view( 'seat.show' , compact( 'seat' ) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Seat  $seat
     * @return \Illuminate\Http\Response
     */
    public function edit(Seat $seat)
    {
        //
        return view( 'seat.edit' , compact( 'seat' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $req
     * @param  \App\Seat  $seat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, Seat $seat)
    {
        $data = $req -> only( 'time' , 'price' , 'price_weekend' , 'court_id' );
        $data[ 'time' ] = $this -> simplifyInput( $data[ 'time' ] );
        $res = $seat -> update( $data );
        return redirect() -> route( 'seats.index' );
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Seat  $seat
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        Seat::findOrFail( $id ) -> delete();
        return redirect() -> route( 'seats.index' );
        //
    }
}
