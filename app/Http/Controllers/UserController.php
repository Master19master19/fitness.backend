<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Helpers\BitrixHelper;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $users = User::orderBy( 'type' , 'ASC' ) -> get();
        return view( 'user.index' , compact( 'users' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create () {
        return view( 'user.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store ( Request $req ) {
        $data = $req -> only( 'name' , 'email' , 'type' , 'password' , 'phone' );
        // BitrixHelper::createUser( $data ); // can only add admins / managers
        $data[ 'verified' ] = 1;
        $data[ 'password_plain' ] = $data[ 'password' ];
        $data[ 'password' ] = bcrypt( $data[ 'password' ] );
        $res = User::create( $data );
        return redirect() -> route( 'users.index' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit ( User $user ) {
        return view( 'user.edit' , compact( 'user' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update ( Request $req , User $user ) {
        if ( Auth::user() -> id == $user -> id && ( $req -> type != 2 || $req -> has( 'banned' ) ) ) {
        	return redirect() -> back() -> withErrors( 'Stay admin.' );
      	}
        $oldPhone = $user -> phone;
        $data = $req -> only( 'name' , 'email' , 'type' , 'phone' );
        foreach ( $data as $key => $value ) {
            $user -> $key = $value;
        }
        if ( ! empty ( $req -> password ) && strlen( $req -> password ) > 5 ) {
            $user -> password_plain = $req -> password;
            $user -> password = bcrypt( $req -> password );
        }
        $user -> banned = $req -> has( 'banned' ) ? 1 : 0;
        $user -> verified = $req -> has( 'verified' ) ? 1 : 0;
        $res = BitrixHelper::updateUser( $user , $user -> phone == $oldPhone ? null : $oldPhone );
        if ( ! $res ) {
            return response( 'Проблема с Битрикс' , 401 );
        }
        $res = $user -> save();
        return redirect() -> route( 'users.index' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy ( $id ) {
        $user = User::findOrFail( $id );
        // preventDumbAsses
        if ( Auth::user() -> id == $user -> id ) {
    		return redirect() -> back() -> withErrors( 'Stay admin.' );
        } else {
    		$user -> delete();
        }
        // preventDumbAsses
        return redirect() -> route( 'users.index' );
    }
}
