<?php

namespace App\Http\Controllers;

use App\TournamentCategory as TourCat;
use Illuminate\Http\Request;

class TournamentCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index () {
        $tourCats = TourCat::all();
        return view( 'tournament.category.index' , compact( 'tourCats' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create () {
        return view( 'tournament.category.create' );
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store ( Request $req ) {
        TourCat::insert( $req -> only( [ 'title' , 'level' ] ) );
        return redirect() -> route( 'tourcategories.index' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TournamentCategory  $tournamentCategory
     * @return \Illuminate\Http\Response
     */
    public function show(TourCat $tourCat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TournamentCategory  $tournamentCategory
     * @return \Illuminate\Http\Response
     */
    public function edit ( TourCat $tourCat , $id ) {
        $tourCat = TourCat::findOrFail( $id );
        return view( 'tournament.category.edit' , compact( 'tourCat' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TournamentCategory  $tournamentCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, TourCat $tournamentCategory , $id ) {
        $data = $req -> only( [ 'title' , 'level' ] );
        $res = TourCat::whereId( $id ) -> update( $data );
        return redirect() -> route( 'tourcategories.index' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TournamentCategory  $tournamentCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        TourCat::findOrFail( $id ) -> delete();
        return redirect() -> route( 'tourcategories.index' );
    }
}
