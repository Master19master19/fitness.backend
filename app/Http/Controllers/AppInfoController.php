<?php

namespace App\Http\Controllers;

use App\AppInfo;
use Illuminate\Http\Request;

class AppInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index () {
        $info = AppInfo::all() -> keyBy( 'key' );
        foreach ( $info as $key => $value ) {
            $info[ $key ] = $value[ 'value' ];
        }
        return view( 'appinfo' , compact( 'info' ) );
    }
    public function cards () {
        return view( 'cards' );
    }
    public function store ( Request $req ) {
        foreach ( $req -> except( '_token' ) as $key => $value ) {
            if ( null !== $value && strlen( $value ) > 1 ) {
                $oldRecord = AppInfo::where( 'key' , $key ) -> first();
                if ( null !== $oldRecord ) {
                    $oldRecord -> value = $value;
                    $oldRecord -> save();
                } else {
                    $newRecord = appInfo::insert([
                        'key' => $key,
                        'value' => $value,
                    ]);
                }
            }
        }
        return redirect() -> back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Display the specified resource.
     *
     * @param  \App\AppInfo  $appInfo
     * @return \Illuminate\Http\Response
     */
    public function show ( ) {
        $info = AppInfo::all() -> keyBy( 'key' );
        foreach ( $info as $key => $value ) {
            $info[ $key ] = $value[ 'value' ];
        }
        $info[ 'phone_2' ] = $this -> getPhone( $info[ 'phone_1' ] );
        return response() -> json( $info );
    }

    private function getPhone ( $number ) {
        $response = str_replace( '-' , '' , $number );
        $response = str_replace( ')' , '' , $response );
        $response = str_replace( '(' , '' , $response );
        $response = str_replace( ' ' , '' , $response );
        $response = str_replace( ' ' , '' , $response );
        $response = str_replace( ' ' , '' , $response );
        return $response;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppInfo  $appInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(AppInfo $appInfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppInfo  $appInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppInfo $appInfo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppInfo  $appInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppInfo $appInfo)
    {
        //
    }
}
