<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'phone' => substr($this->phone,0,18)
        ]);
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|string|min:17|max:18',
            'password' => 'required', 
        ];
    }
    public function messages()
    {
        return [
            'phone.min' => 'Проверьте правильность номера телефона',
            'phone.max' => 'Проверьте правильность номера телефона',
        ];
    }
}
