<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class FeedBackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:4|max:15', 
            'email' => 'required|email', 
            'phone' => 'required|string|min:18|max:18',
            'message' => 'required|string|min:10|max:500',
        ];
    }
    protected function prepareForValidation()
    {
        $this->merge([
            'phone' => substr($this->phone,0,18)
        ]);
    }
    public function messages()
    {
        return [
            'email.exists' => 'Пользователя с такой почтой не существует', 
        ];
    }
}
