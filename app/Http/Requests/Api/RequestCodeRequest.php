<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class RequestCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'phone' => substr($this->phone,0,18)
        ]);
    }
    public function rules()
    {
        return [
            'phone' => 'required|string|min:17|max:18|exists:users', 
        ];
    }
}
