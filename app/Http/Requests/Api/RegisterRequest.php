<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'phone' => substr($this->phone,0,18)
        ]);
    }
    public function rules()
    {
        return [
            'phone' => 'required|string|min:18|max:18|unique:users',
            'email' => 'nullable|email|unique:users', 
            'name' => 'required|string|min:4', 
            'password' => 'required|min:6', 
            'passwordRepeat' => 'required|same:password',
        ];
    }
    public function messages()
    {
        return [
            'phone.min' => 'Проверьте правильность номера телефона',
            'phone.max' => 'Проверьте правильность номера телефона',
            'phone.unique' => 'Пользователь с такими данными уже зарегистрирован, восстановите пароль',
            'email.unique' => 'Пользователь с таким E-mail уже зарегистрирован',
        ];
    }
}
