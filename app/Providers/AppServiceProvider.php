<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        try {
            // View::share( 'arenas' , \App\Arena::whereActive( 1 ) -> get() );
            // View::share( 'times' , \App\Time::all() );
            // View::share( 'courts' , \App\Court::all() );
        } catch ( \Exception $e ) {
            return response() -> json([
                'result' => false,
                'errorCode' => 400,
                'error' => "Проблема с подключением Mysql"
            ]);
            // echo $e -> getMessage();
            return false;
        }
    }
}
