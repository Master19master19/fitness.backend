<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $appends = [ 'isVerified' , 'role' ];
    public function getIsVerifiedAttribute () {
        return $this -> verified == 1 ? "Да" : "Нет";
    }
    public function getRoleAttribute () {
        return $this -> type == 0 ? "Пользователь" : ( $this -> type == 2 ? 'Админ' : 'Менеджер' );
    }

    public function getPhoneeAttribute () {
        $response = str_replace( '-' , '' , $this -> phone );
        $response = str_replace( ')' , '' , $response );
        $response = str_replace( '(' , '' , $response );
        $response = str_replace( ' ' , '' , $response );
        $response = str_replace( ' ' , '' , $response );
        $response = str_replace( ' ' , '' , $response );
        return $response;
    }
}
