<?php

namespace App\Helpers;
use App\Arena;
use App\Seat;
use App\Hall;
use App\Helpers\Helper;

// {"class":"chargingRent","courtId":"19","days":1,"startTime":"0001-01-01T09:00:00","finishTime":"0001-01-01T09:59:59","price":0}

class BitrixSeatHelper {
    private static $error = [];
	public static function handleRent ( $data ) {
        return self::updateSeatPrices( $data );
	}
    private static function isWeekend ( $days ) {
        if ( is_array( $days ) ) {
           $day = $days[ 0 ]; 
        } else {
            $day = $days;
        }
        if ( in_array( $day , [6,7,9] ) ) {
            return true;
        } else {
            return false;
        }
    }
    
    // {"class":"chargingRent","courtId":"7","days":"5","startTime":"0001-01-01T20:00:00","finishTime":"0001-01-01T20:59:59","price":"2000"}
    private static function updateSeatPrices ( $data ) {
        $court = Hall::whereId( $data[ 'courtId' ] ) -> first();
        if ( null == $court ) {
            self::$error = [
                'result' => false,
                'errorCode' => 500,
                'error' => "Зал " . $data[ 'courtId' ] . " не найден"
            ];
        } else {
            if ( $data[ 'days' ] == 6 && $data[ 'startTime' ] == '0001-01-01T09:00:00' ) {
                $court -> price = $data[ 'price' ];
                $court -> save();
            } else if ( $data[ 'days' ] == 6 && $data[ 'startTime' ] == '0001-01-01T10:00:00' ) {
                $court -> prime_price = $data[ 'price' ];
                $court -> save();
            }
        }
        if ( count( self::$error ) ) {
            return self::$error;
        } else {
            return [ 'result' => true ];
        }
    }
    private static function updateSeatPricesBakTemp ( $data ) {
        $court = Hall::whereId( $data[ 'courtId' ] ) -> first();
        if ( null == $court ) {
            self::$error = [
                'result' => false,
                'errorCode' => 500,
                'error' => "Зал " . $data[ 'courtId' ] . " не найден"
            ];
        } else {
            $isWeekend = self::isWeekend( $data[ 'days' ] );
            $start = str_replace( '0001-01-01T' , '' , $data[ 'startTime' ] );
            $start = str_replace( ':00:00' , '' , $start );
            $end = str_replace( '0001-01-01T' , '' , $data[ 'finishTime' ] );
            $end = str_replace( ':59:59' , '' , $end );
            $times = [];
            for ( $x = (int) $start; $x <= ( int ) $end; $x++ ) {
                if ( $x < 10 ) {
                    $time = "0" . $x . ":00";
                } else {
                    $time = $x . ":00";
                }
                $priceKey = Helper::priceKeyFromBitrix( $time , $isWeekend );
                if ( $priceKey == 'prime_price' ) {
                    $court -> prime_price = $data[ 'price' ];
                    $court -> save();
                } else {
                    $court -> price = $data[ 'price' ];
                    $court -> save();
                }
            }
        }
        //     // $searchData = [
        //     //     'time' => $time,
        //     //     'court_id' => $data[ 'courtId' ],
        //     // ];
        //     // dd($court);
        //     if ( null == $court ) {
        //         self::$error = [
        //             'result' => false,
        //             'errorCode' => 500,
        //             'error' => "Зал " . $data[ 'courtId' ] . " не найден"
        //         ];
        //     } else {
        //         $seat = Seat::where( $searchData ) -> first();
        //         if ( $seat == null ) {
        //             if ( $weekType == 'week' ) {
        //                 $searchData[ 'price' ] = $data[ 'price' ];
        //             } else if ( $weekType == 'all' ) {
        //                 $searchData[ 'price_weekend' ] = $data[ 'price' ];
        //                 $searchData[ 'price' ] = $data[ 'price' ];
        //             } else {
        //                 $searchData[ 'price_weekend' ] = $data[ 'price' ];
        //             }
        //             $res = Seat::create( $searchData );
        //         } else {
        //             $updateData = [];
        //             if ( $weekType == 'week' ) {
        //                 $updateData = ['price' => $data[ 'price' ]];
        //             } else if ( $weekType == 'all' ) {
        //                 $updateData = ['price_weekend' => $data[ 'price' ],'price' => $data[ 'price' ] ];
        //             } else {
        //                 $updateData = ['price_weekend' => $data[ 'price' ]];
        //             }
        //             $seat -> update( $updateData );
        //         }
        //     }
            if ( count( self::$error ) ) {
                return self::$error;
            } else {
                return [ 'result' => true ];
            }
        }
    }