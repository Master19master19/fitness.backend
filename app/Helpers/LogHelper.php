<?php

namespace App\Helpers;
use App\Log;
use App\In;
use App\BitrixIn;
use App\Out;

class LogHelper {
    public static function bitrix_in ( $data , $error = 0 , $errorMessage = '' ) {
        $record = new In;
        $record -> message = json_encode( $data );
        $record -> class = $data[ 'class' ];
        $record -> error_message = $errorMessage;
        $record -> error = $error;
        $record -> save();
        if ( strlen( $errorMessage ) > 1 ) {
            self::telegram( "Ошибка при получении пакета от 1С (id: {$record->id})\n" . $errorMessage );
        }
    }
    public static function bitrix_in_init ( $data , $error = 0 , $errorMessage = '' ) {
        $record = new BitrixIn;
        $record -> message = json_encode( $data );
        $record -> class = $data[ 'class' ];
        $record -> error_message = $errorMessage;
        $record -> error = $error;
        $record -> save();
    }
    public static function bitrix_out ( $data , $time = 0 ) {
        $record = new Out;
        $record -> message = json_encode( $data );
        $record -> class = $data[ 'class' ];
        $record -> time = $time;
        $record -> save();
    }
    public static function bitrix_out_error ( $data , $e , $action = '' , $time = 0 ) {
        $message = iconv( "windows-1251" , "utf-8" , $e -> getMessage() );
        $code = $e -> getCode();
        $record = new Out;
        $record -> message = json_encode( $data );
        $record -> error_message = $message;
        $record -> error = 1;
        $record -> code = $code;
        $record -> time = $time;
        $record -> action = $action;
        $record -> class = $data[ 'class' ];
        $record -> save();
        self::telegram( "(id: {$record->id})\n" . $message );
    }
    public static function telegram ( $str ) {
        $key = env( 'TELEGRAM_BOT_KEY' );
        $chat_id = env( 'TELEGRAM_CHAT_ID' );
        $message = urlencode( $str );
        $finalStr = "https://api.telegram.org/bot{$key}/sendMessage?chat_id={$chat_id}&parse_mode=html&text=" . $message;
        // file_put_contents(__DIR__ . '/ss.err.log' ,$finalStr);
        try {
            $res = file_get_contents( $finalStr );
        } catch ( \Exception $e ) {
            // exit(0)
            file_put_contents( __DIR__ . '/log.err.log' , $e -> getMessage() . PHP_EOL , FILE_APPEND );
            // dd($e);
        }
    }
    public static function send ( $str ) {
        $log = new Log;
        $log -> message = $str;
        $log -> save();
        return;
        self::telegram( $str );
        // file_put_contents( __DIR__ . '/log.log' , $str . PHP_EOL , FILE_APPEND );
        // return true;
        // $code = 'Код авторизации: ' . $code;
    }
}