<?php

namespace App\Helpers;
use App\BoughtSeat;
use App\ReservedSeat;
use App\Hall;
use App\Helpers\Helper;
use App\Seat;

// {"class":"rent","cancelReserve":{"hallId":"25","startTime":"2020-06-03T17:30:00","finishTime":"2020-06-03T21:29:59"},"newReserve":{"hallId":"25","startTime":"2020-06-03T17:00:00","finishTime":"2020-06-03T20:59:59"}}


class BitrixRentHelper {
    private static $error = [];
    private static function getSeatIds ( $info , $hallId ) {
        $startExplode = explode( 'T' , $info[ 'startTime' ] );
        $endExplode = explode( 'T' , $info[ 'finishTime' ] );
        $date = $startExplode[ 0 ];
        $response = [
            'date' => $date,
            'times' => []
        ];
        $startTime = str_replace( ':00:00' , '' , $startExplode[ 1 ] );
        $endTime = str_replace( ':59:59' , '' , $endExplode[ 1 ] );
        $endTime = str_replace( ':58:59' , '' , $endExplode[ 1 ] );
        for ( $x = (int) $startTime; $x <= ( int ) $endTime ; $x++ ) {
            if ( $x < 10 ) {
                $time = "0" . $x . ":00";
            } else {
                $time = $x . ":00";
            }
            $response[ 'times' ][] =  $time;
            
        }
        return $response;
    }
    
// {"class":"rent","newReserve":{"hallId":"9","startTime":"2020-06-04T11:00:00","finishTime":"2020-06-04T12:59:59"}}
    private static function handleNewReserve ( $newReserve ) {
        // if ( is_array( $newReserve ) && count ( $newReserve ) ) {
        //     foreach ( $newReserve as $value ) {
        //         $this -> 
        //     }
        // }
        $hallId = $newReserve[ 'courtId' ];
        $hall = Hall::whereId( $hallId ) -> first();
        if ( null == $hall ) {
            self::$error = [
                'result' => true,
                'errorCode' => 500,
                'error' => "Зал {$hallId} не найден"
            ];
            return;
        }
        $resp = self::getSeatIds ( $newReserve , $hallId );
        $date = $resp[ 'date' ];
        $times = $resp[ 'times' ];
        if ( count( $times ) ) {
            foreach ( $times as $time ) {
                $search = [
                    'hall_id' => $hallId,
                    'time' => $time,
                    'date' => $date,
                    'paid' => 1,
                ];
                $check = BoughtSeat::where( $search ) -> first();
                if ( null !== $check ) {
                    if ( $check -> user_id != 0 ) {
                        self::$error = [
                            'result' => true,
                            'errorCode' => 500,
                            'error' => "Время " . $time . " уже оплачено"
                        ];
                        return;
                    }
                } else {
                    unset( $search[ 'paid' ] );
                    $check = BoughtSeat::where( $search ) -> first();
                    if ( null !== $check ) {
                        $chicken = $search;
                        $chicken[ 'paid' ] = 0;
                        $chicken = boughtSeat::where( $search ) -> first();
                        // dd($chicken);
                        if ( null !== $chicken && $chicken -> reserved_until - time() > 0 ) {
                            self::$error = [
                                'result' => false,
                                'errorCode' => 400,
                                'error' => "Время " . $time . " забронировано"
                            ];
                            return;
                        }
                    }
                }
                $unique = "{$time}-{$hallId}-{$date}";
                $time_to = Helper::getTimeTo( $time );
                $fill = [
                    'hall_id' => $hallId,
                    'user_id' => 0,
                    'price' => 1500,
                    'date' => $date,
                    'unique' => $unique,
                    'created_at' => date( 'Y-m-d H:i:s' ),
                    'paid' => 1,
                    'time' => $time,
                    'time_to' => $time_to,
                ];
                $clean = $fill;
                unset( $clean[ 'created_at' ] );
                BoughtSeat::where( $clean ) -> delete();
                BoughtSeat::create( $fill );
            }
        }
    }
    private static function handleCancelReserve ( $cancelReserve ) {
        $hallId = $cancelReserve[ 'courtId' ];
        $hall = Hall::whereId( $hallId ) -> first();
        if ( null == $hall ) {
            self::$error = [
                'result' => true,
                'errorCode' => 500,
                'error' => "Зал {$hallId} не найден"
            ];
            return;
        }
        $resp = self::getSeatIds ( $cancelReserve , $hallId );
        $date = $resp[ 'date' ];
        $times = $resp[ 'times' ];
        if ( count( $times ) ) {
            foreach ( $times as $time ) {
                $search = [
                    'hall_id' => $hallId,
                    // 'user_id' => 0,
                    // 'price' => $seat[ 'price' ],
                    'date' => $date,
                    // 'created_at' => date( 'Y-m-d H:i:s' ),
                    // 'paid' => 1,
                    'time' => $time,
                ];
                $boughtSeat = BoughtSeat::where( $search ) -> delete();
            }
        }
    }
	public static function handleRent ( $data ) {
        // dd($data);
        if ( isset( $data[ 'cancelReserve' ] ) && is_array( $data[ 'cancelReserve' ] ) && count( $data[ 'cancelReserve' ] ) ) {
            self::handleCancelReserve( $data[ 'cancelReserve' ] );
        }
        if ( isset( $data[ 'newReserve' ] ) && is_array( $data[ 'newReserve' ] ) && count( $data[ 'newReserve' ] ) ) {
            // dd('pl');
            self::handleNewReserve( $data[ 'newReserve' ] );
        }
        if ( count( self::$error ) ) {
            return self::$error;
        } else {
            return [ 'result' => true ];
        }
	}
}