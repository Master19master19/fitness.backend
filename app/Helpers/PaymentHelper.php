<?php

namespace App\Helpers;
use YooKassa\Client;
use App\Card;
use App\Order;


class PaymentHelper {

    protected static function setReceiptCustomer ( $phone , $email ) {
        $customerr = [
            'phone' => $phone,
        ];
        $email = str_replace( ' ' , '' , $email );
        $email = strtolower( $email );
        if ( strlen( $email ) > 7 && ( strpos( $email , '@' ) !== false ) && filter_var( $email , FILTER_VALIDATE_EMAIL ) ) {
            $customerr[ 'email' ] = $email;
        }
        // $customerr = [ 'phone' => $phone ];
        file_put_contents(__DIR__ . '/as', json_encode( $customerr ) . PHP_EOL , FILE_APPEND );
        return $customerr;
    }
    private static function getPhone ( $number ) {
        $response = str_replace( '-' , '' , $number );
        $response = str_replace( ')' , '' , $response );
        $response = str_replace( '(' , '' , $response );
        $response = str_replace( ' ' , '' , $response );
        $response = str_replace( ' ' , '' , $response );
        $response = str_replace( ' ' , '' , $response );
        return $response;
    }
    public static function getPaymentInfo ( $payment_id ) {
        $client = new Client();
        $client -> setAuth( env( 'KASSA_MERCHANT_ID' ) , env( 'KASSA_SECRET' ) );
        try {
            $payment = $client -> getPaymentInfo( $payment_id );
        } catch ( \Exception $e ) {
            return null;
        }
        return $payment;
    }
    public static function getPaymentInfoStatus ( $payment_id ) {
        if ( null == $payment_id ) return 'canceled';
        $res = self::getPaymentInfo( $payment_id );
        if ( null !== $res ) {
            // dd($res);
            return $res -> _status;
        }
        return 'unknown';
    }
    public static function payWithCard ( $amount = 15 , $phone = '+74959801010' , $cardId , $orderId , $email = '' ) {
        // $amount = 15;
        if ( $phone == '+7 (111) 111-11-11' ) {
            $amount = 2;
        }
        $phone = self::getPhone( $phone );
        $client = new Client();
        $client -> setAuth( env( 'KASSA_MERCHANT_ID' ) , env( 'KASSA_SECRET' ) );
        $idempotenceKey = uniqid( '' , true );
        $card = Card::findOrFail( $cardId );
        $customerArray = self::setReceiptCustomer( $phone , $email );
        $data = [
                'amount' => [
                    'value' => $amount,
                    'currency' => 'RUB'
                ],
                'receipt' => [
                    'customer' => $customerArray,
                    "items" => [
                        [
                            "quantity" => 1,
                            'amount' => [
                                'value' => $amount
                            ],
                            'vat_code' => env( 'YAKASSA_VAT_CODE' ),
                            'description' => 'Оплата фитнес',
                            'paymentMethodType' => 'full_prepayment'
                        ]
                    ],
                ],
                'capture' => true,
                'payment_method_id' => $card -> token,
                'description' => 'Оплата корта(-ов)',
            ];
        try {
            $response = $client -> createPayment(
                $data,
                $idempotenceKey
            );
        } catch ( \Exception $e ) {
            file_put_contents( __DIR__ . '/cardErr' , json_encode( $e -> getMessage() ) . PHP_EOL , FILE_APPEND );
            return false;
        }
        file_put_contents( __DIR__ . '/card' , json_encode( $response ) . PHP_EOL , FILE_APPEND );
        if ( isset( $response -> status ) ) {
            if ( $response -> status == 'canceled' ) {
                return false;
            } else {
                Order::find( $orderId  ) -> update([
                    'key' => $response -> id
                ]);
                return true;
            }
        }
    }
	public static function createPayment ( $amount = 15 , $phone = '+74959801010' , $isCard = false , $email = '' ) {
        // file_put_contents( __DIR__ . '/phone' ,  $phone );
        if ( $phone == '+7 (111) 111-11-11' ) {
            $amount = 2;
        }
        $phone = self::getPhone( $phone );
        $client = new Client();
        $client -> setAuth( env( 'KASSA_MERCHANT_ID' ) , env( 'KASSA_SECRET' ) );
        if ( $isCard ) {
            $amount = env( 'ADD_CARD_PRICE' );
        }
        $idempotenceKey = uniqid( '' , true );
        $customerArray = self::setReceiptCustomer( $phone , $email );
        $data = [
                'receipt' => [
                    'customer' => $customerArray,
                    "items" => [
                        [
                            "quantity" => 1,
                            'amount' => [
                                'value' => $amount
                            ],
                            'vat_code' => env( 'YAKASSA_VAT_CODE' ),
                            'description' => 'Оплата фитнес',
                            'paymentMethodType' => 'full_prepayment'
                        ]
                    ],
                ],
                'amount' => [
                    'value' => $amount,
                    'currency' => 'RUB'
                ],
                // 'test' => 1,
                'capture' => true,
                'confirmation' => [
                    'type' => 'embedded' ,
                    'return_url' => env( 'KASSA_RETURN_URL' )
                ],
                // 'confirmation' => [
                //     'type' => 'redirect' ,
                //     'return_url' => env( 'KASSA_RETURN_URL' )
                // ],
    //         'payment_method_data' => [
    //             'type' => 'bank_card',
    //             'card' => [
    //                 'number' => '',
    //                 'expiry_year' => '2025',
    //                 'expiry_month' => '04',
    //                 'csc' => '',
    //             ]
    //         ],
                'description' => 'Оплата фитнес',
                // 'save_payment_method' => false,
            ];
        if ( $isCard ) {
            $data[ 'save_payment_method' ] = true;
            $data[ 'confirmation' ] = [
                'type' => 'embedded' ,
                'return_url' => env( 'KASSA_RETURN_URL' )
            ];
        }
        $response = $client -> createPayment(
            $data,
            $idempotenceKey
        );
        // $return_url = $response -> getConfirmation() -> __get( "return_url" );
        $payment_id = $response -> id;
        if ( $isCard ) {
            $confirmation_token = $response -> getConfirmation() -> __get( "confirmation_token" );
        } else {
            $confirmation_token = $response -> getConfirmation() -> __get( "confirmation_token" );
            // $confirmation_token = $response -> getConfirmation() -> __get( "confirmation_url" );
        }
        // file_put_contents(__DIR__.'/testa', $confirmation_token);
        // exit;
        return compact( 'confirmation_token' , 'payment_id' );
        return view( 'payment.index' , [ 'confirmation_token' => $confirmation_token ] );
    }




    public static function payTest ( $amount = 15 ) {
        $amount = 20;
        $client = new Client();
        $client -> setAuth( env( 'KASSA_MERCHANT_ID' ) , env( 'KASSA_SECRET' ) );
        $idempotenceKey = uniqid( '' , true );
        $data = [
                'receipt' => [
                    'customer' => [
                        'phone' => '+7111111111'
                    ],
                    "items" => [
                        [
                            "quantity" => 1,
                            'amount' => [
                                'value' => $amount
                            ],
                            'vat_code' => env( 'YAKASSA_VAT_CODE' ),
                            'description' => 'Оплата фитнес',
                            'paymentMethodType' => 'full_prepayment'
                        ]
                    ],
                ],
                'amount' => [
                    'value' => $amount,
                    'currency' => 'RUB'
                ],
                'capture' => true,
            'confirmation' => [
                'type' => 'embedded' ,
                'return_url' => env( 'KASSA_RETURN_URL' )
            ],
                'description' => 'Оплата корта(-ов)',
            ];
        $response = $client -> createPayment(
            $data,
            $idempotenceKey
        );
        $return_url = $response -> getConfirmation() -> __get( "confirmation_token" );
        $payment_id = $response -> id;
        $url = 'https://fitness.tennis.ru/api/pay/' . $return_url;
        return $url;
        exit(json_decode($url));
    }


}