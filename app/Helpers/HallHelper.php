<?php

    namespace App\Helpers;
    use App\Hall;
    use App\Time;
    use App\BoughtSeat;
    use App\Helpers\Helper;

    class HallHelper {
        public static function getHall ( $hallId , $dateString = '2020-08-08' ) {
            // $dateString = '2020-08-08';
            $date = strtotime( $dateString );
            $date = date( 'Y-m-d' , $date );
            $hall = Hall::whereActive( 1 ) -> whereId( $hallId ) -> firstOrFail() -> toArray();
            $times = Time::whereActive( 1 ) -> get() -> toArray();
            // $response = [];
            // CHECKING IF THE HALL IS ON THE STREET, THERE IS NO LIGHT ON THE STREET AFTER 21:00
            if ( in_array( $hall[ 'id' ] , [ 13 , 14 ] ) ) { 
                foreach ( $times as $key => $time ) {
                    if ( in_array( $time[ 'code' ] , [ '22:00' , '23:00' ] ) ) {
                        unset( $times[ $key ] );
                    }
                }
            }
            // CHECKING IF THE HALL IS ON THE STREET, THERE IS NO LIGHT ON THE STREET AFTER 21:00
            foreach ( $times as $key => &$time ) {
                $priceKey = Helper::priceKey( $date , $time[ 'code' ] );
                $time[ 'price' ] = $hall[ $priceKey ];
                $search = [
                    'hall_id' => $hall[ 'id' ],
                    'paid' => 1,
                    'date' => $date,
                    'time' => $time[ 'code' ],
                ];
                $boughtSeatPaid = BoughtSeat::where( $search ) -> first();
                if ( null == $boughtSeatPaid ) {
                    unset( $search[ 'paid' ] );
                    $boughtSeat = BoughtSeat::where( $search ) -> first();
                    if ( null !== $boughtSeat ) {
                        if ( $boughtSeat[ 'reserved_until' ] - time() > 0 ) {
                            $time[ 'unavailable' ] = true;
                        }
                    }
                } else {
                    $time[ 'unavailable' ] = true;
                }
            }
            $response = [
                'times' => $times,
                'hall' => $hall
            ];
            // foreach ( $times as $time ) {
                // $response
            // }
            // dd($hall,$times);
            // foreach ( $halls as $hall ) {
            //     $arenaRow = [];
            //     $unAvailableSeats = BoughtSeat::where( [ 'date' =>  $date , 'paid' => 1 , 'arena_id' => $arena[ 'id' ] ] ) -> get( [ 'seat_id' , 'court_id' ] ) -> mapToGroups( function ( $item , $key ) {
            //         return [ $item[ 'court_id' ] => $item[ 'seat_id' ] ];
            //     }) -> toArray();
            //     $arenaRow = [
            //         'title' => $arena[ 'title' ],
            //         'id' => $arena[ 'id' ],
            //         'cell_color' => $arena[ 'cell_color' ],
            //         'courts' => $arena[ 'courts' ],
            //         'unAvailableSeats' => $unAvailableSeats
            //     ];
            //     foreach ( $arena[ 'seats' ] as $seat ) {
            //         $arr = [
            //             'price' => $seat[ 'price' ],
            //             'id' => $seat[ 'id' ],
            //             'time' => $seat[ 'time' ],
            //             'price_weekend' => $seat[ 'price_weekend' ],
            //             'court_id' => $seat[ 'court_id' ]
            //         ];
            //         $arenaRow[ 'seats' ][] = $arr;
            //     }
            //     $response[] = $arenaRow;
            // }
            return $response;
        }
    }