<?php

namespace App\Helpers;
use App\Subscription;
use App\BoughSubscription;
use App\User;

class BitrixSubscriptionHelper {
	public static function handleSubscription ( $data ) {
		$externalId = $data[ 'id' ];
		$nowDate = date( 'Y-m-d H:i:s' );
		if ( $data[ 'isActive' ] == false ) {
			$subscription = Subscription::whereExternalId( $externalId ) -> first();
			if ( null == $subscription ) {
				return [
					'result' => true,
                    'errorCode' => 500,
                    'error' => "Абонемент " . $data[ 'id' ] . " не найден"
				];
			} else {
				$subscription -> active = 0;
				$subscription -> save();
		        return [
		            'result' => true
		        ];
			}
		} else {
			$info = [
				'title' => $data[ 'name' ],
				'secondary_title' => '',
				'price' => $data[ 'price' ],
				'type' => 'adult',
				'updated_at' => $nowDate,
				'active' => ( $data[ 'isActive' ] == 'true' ? 1 : 0 )
			];
		}
		$subscription = Subscription::whereExternalId( $externalId ) -> first();
		if ( $subscription === null ) {
			$info[ 'external_id' ] = $externalId;
			$subscription = Subscription::create( $info );
		} else {
        	$subscription -> update( $info );
		}
        return [
            'result' => true
        ];
	}
}