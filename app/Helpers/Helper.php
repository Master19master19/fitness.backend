<?php
    namespace App\Helpers;
    use App\Hall;
    
    class Helper {

        protected static function isWeekend ( $date ) {
            return ( date( 'N' , strtotime( $date ) ) >= 6 );
        }


        public static function priceKey ( $date , $time ) {
            if ( self::isWeekend( $date ) ) {
                if ( in_array( $time , [ '10:00' , '11:00' , '12:00' ] ) ) {
                    return 'prime_price';
                } else {
                    return 'price';
                }
            } else {
                if ( in_array( $time , [ '10:00' , '11:00' , '12:00' , '17:00' , '18:00' , '19:00' ] ) ) {
                    return 'prime_price';
                } else {
                    return 'price';
                }
            }
        }




        public static function priceKeyFromBitrix ( $time , $isWeekend ) {
            if ( $isWeekend ) {
                if ( in_array( $time , [ '10:00' , '11:00' , '12:00' ] ) ) {
                    return 'prime_price';
                } else {
                    return 'price';
                }
            } else {
                if ( in_array( $time , [ '10:00' , '11:00' , '12:00' , '17:00' , '18:00' , '19:00' ] ) ) {
                    return 'prime_price';
                } else {
                    return 'price';
                }
            }
        }
        
        public static function getTimeTo ( $time ) {
            $timeTo = ( (int) $time ) + 1;
            if ( $timeTo < 10 ) $timeTo = "0" . $timeTo;
            $timeTo = $timeTo . ":00";
            return $timeTo;
        }

        public static function getPrice ( $hallId , $date , $time ) {
            $priceKey = self::priceKey( $date , $time );
            $hall = Hall::whereId( $hallId ) -> firstOrFail() -> toArray();
            return $hall[ $priceKey ];
        }

    }