<?php

namespace App\Helpers;
use App\Helpers\LogHelper;


class SMS {
	protected static function aeroSms ( $sms , $to ) {
		$aeroLogin = env( 'SMS_AERO_LOGIN' );
		$aeroKey = env( 'SMS_AERO_KEY' );
		$to = str_replace( '+' , '' , $to );
    	$to = str_replace( '(' , '' , $to );
		$to = str_replace( ')' , '' , $to );
		$to = str_replace( ' ' , '' , $to );
		$to = str_replace( ' ' , '' , $to );
		$to = str_replace( ' ' , '' , $to );
		$url = "https://{$aeroLogin}:{$aeroKey}@gate.smsaero.ru/v2/sms/send?number={$to}&text={$sms}&sign=SMS Aero&channel=DIRECT";
		$output = file_get_contents( $url );
		$output = json_decode( $output , 1 );
		file_put_contents( __DIR__ . '/sms_aero' , json_encode(['output'=>$output,'url' => $url,'to'=>$to]).PHP_EOL,FILE_APPEND);
		if ( is_array( $output ) && isset( $output[ 'success' ] ) && $output[ 'success' ] == 'true' ) {
			return true;
		}
		return false;
	}
	protected static function smsc ( $sms , $to ) {
		$login = env( 'SMSC_LOGIN' );
		$sender = env( 'SMS_SENDER' );
		$password = env( 'SMSC_PASSWORD' );
		// $to = '+374773797590';
		// SENDER ERROR TEMP
		$url = "https://smsc.ru/sys/send.php?login={$login}&psw={$password}&phones={$to}&mes={$sms}&sender={$sender}";
		// $url = "https://smsc.ru/sys/send.php?login={$login}&psw={$password}&phones={$to}&mes={$sms}";
		// SENDER ERROR TEMP
		// $url = "https://smsc.ru/sys/send.php?login={$login}&psw={$password}&phones={$to}&mes={$sms}&charset=utf-8&sender={$sender}";
		// dd($url);
		// $url = "https://smsc.ru/sys/send.php?login={$login}&psw={$password}&phones={$to}&mes={$sms}&cost=1";
		$ch = curl_init();
        curl_setopt( $ch , CURLOPT_URL , $url );
        curl_setopt( $ch , CURLOPT_RETURNTRANSFER , 1 );
        $output = curl_exec( $ch );
        curl_close( $ch );
		file_put_contents( __DIR__ . '/smsc' , json_encode(['output'=>$output,'url' => $url,'to'=>$to]).PHP_EOL,FILE_APPEND);
        if ( strpos( $output , 'OK' ) !== false ) {
        	return true;
        } else {
        	$error = "Ошибка smsc, на телефон <b> {$to} </b> , \n " . $output;
        	LogHelper::send( $error );
        	return false;
        }
	}
	public static function sendSMS ( $title , $desc , $to ) {
		// $to = '89778699228';
		// return true;
		$sms = $title . " " . $desc;
		return self::smsc ( $sms , $to );
        return false;
	}
}