<?php

namespace App\Helpers;

use App\BoughtSeat;
use App\Order;
use App\User;
use App\Helpers\BitrixHelper;
use App\Helpers\PaymentHelper;

class BitrixCancelReserveHelper {
    private static $template = [
        'class' => 'rent',
        'cancelReserve' => [],
        'phone' => ''
    ];
    public static function cancelReserveByOrderId ( $orderId ) {
        $order = Order::whereId( $orderId ) -> firstOrFail();
        $user = User::whereId( $order -> user_id ) -> firstOrFail();
        $boughtSeats = BoughtSeat::where( [ 'order_id' => $orderId ] ) -> get();
        if ( null == $boughtSeats || ! count( $boughtSeats ) ) return true;
        foreach ( $boughtSeats as $seat ) {
            if ( $seat -> paid == 0 ) {
                self::cancelReserve( $seat , $user );
            }
        }
    }
    public static function cancelReserve ( $reserve , $user ) {
        $startHour = (int) $reserve -> time;
        if ( $startHour < 10 ) $startHour = '0' . $startHour;
        $startTime = $reserve -> date . "T" . $startHour . ":00:00";
        $finishTime = $reserve -> date . "T" . $startHour . ":59:59";
        self::$template[ 'cancelReserve' ] = [
            [
                'courtId' => strval( $reserve -> hall_id ),
                'startTime' => $startTime,
                'finishTime' => $finishTime,
            ]
        ];
        self::$template[ 'phone' ] = BitrixHelper::getPhone( $user -> phone );
        BoughtSeat::whereId( $reserve -> id ) -> delete();
        $res = BitrixHelper::send( self::$template , 'cancel_reserve' );
    }
    public static function cancelFixReserve ( $reserve , $user ) {
        $startHour = (int) $reserve -> time;
        if ( $startHour < 10 ) $startHour = '0' . $startHour;
        $startTime = $reserve -> date . "T" . $startHour . ":00:00";
        $finishTime = $reserve -> date . "T" . $startHour . ":59:59";
        self::$template[ 'cancelReserve' ] = [
            [
                'courtId' => strval( $reserve -> court_id ),
                'startTime' => $startTime,
                'finishTime' => $finishTime,
            ]
        ];
        self::$template[ 'phone' ] = BitrixHelper::getPhone( $user -> phone );
        // dd(json_encode( self::$template));
        BitrixHelper::send( self::$template , 'cancel_reserve' );
    }
    public static function fixHistory() {
        $boughtSeats = BoughtSeat::has( 'user' ) -> with( 'user' ) -> where( 'paid' , 0 ) -> where( 'user_id' , '!=' , 0 ) -> where( 'fixed' , 0 ) -> take( 5 ) -> get();
        foreach ( $boughtSeats as $key => $bou ) {
            self::cancelFixReserve( $bou , $bou -> user );
            // exit('ok');
            $bou -> fixed = 1;
            $bou -> save();
            // usleep( 250000 );
            // dd($bou->user);
            // exit( 'ok' );
        }
        dd($boughtSeats);
        // $reservedSeats = ReservedSeat::where( 'id' , '<' , '1650' ) -> whereChick( 0 ) -> take( 50 ) -> get();
        // foreach ( $reservedSeats as $key => $reserve ) {
        //     $boughtSeat = BoughtSeat::where([
        //         'paid' => 1,
        //         'seat_id' => $reserve -> seat_id,
        //         'court_id' => $reserve -> court_id,
        //         'arena_id' => $reserve -> arena_id,
        //         'user_id' => 0,
        //         'date' => $reserve -> date,
        //         'time' => $reserve -> time,
        //     ]) -> first();
        //     if ( null === $boughtSeat ) {
        //         $user = User::whereId( $reserve -> user_id ) -> first();
        //         if ( null == $user ) {
        //             file_put_contents( __DIR__ . '/cancel_no_user' , json_encode( $reserve ) . PHP_EOL , FILE_APPEND );
        //             continue;
        //         }
        //         // dd($reserve);
        //         self::cancelReserve ( $reserve , $user );
        //         // dd(self::$template,$reserve);
        //     }
        //     $reserve -> chicked = 1;
        //     $reserve -> save();
        // }
    }
    public static function check() {
        $reservedSeats = BoughtSeat::orderBy( 'id' , 'DESC' ) -> whereChecked( 0 ) -> wherePaid( 0 ) -> get();
        foreach ( $reservedSeats as $reserve ) {
            if ( $reserve -> reserved_until - time() < 0 ) {
                // usleep( 500000 );
                // dd($reserve);
                // $search = [
                //     'paid' => 1,
                //     'seat_id' => $reserve -> seat_id,
                //     'court_id' => $reserve -> court_id,
                //     'arena_id' => $reserve -> arena_id,
                //     'user_id' => $reserve -> user_id,
                //     'date' => $reserve -> date,
                //     'time' => $reserve -> time,
                // ];
                // $boughtSeat = BoughtSeat::where( $search ) -> first();
                    $user = User::whereId( $reserve -> user_id ) -> first();
                    if ( null == $user ) continue;



                    $order = Order::whereId( $reserve -> order_id ) -> first();
                    if ( null !== $order ) {
                        $key = $order -> key;
                        $status = PaymentHelper::getPaymentInfoStatus( $key );
                        $order -> status = $status;
                        $order -> save();
                        if ( $status == 'pending' ) {
                            continue;
                        }
                        // dd ( $status );
                    }
                    // dd($boughtSeat);
                    // dd($reserve);
                    self::cancelReserve ( $reserve , $user );
            } else {
                // file_put_contents( __DIR__ . '/aaaacancel' , json_encode( self::$template ) );
                // dd('no timeout yet',$reserve,$reserve -> reserved_until - time());
            }
            // dd($reserve);
        }
        // dd($reservedSeats);
    }
}