<?php

namespace App\Helpers;

class BackupHelper {
	public static function backup ( $public = false ) {
		$res = self::mysql( $public );
		if ( $public ) {
			// self::files();
			header( "Content-type: application/x-gzip" );
			header( 'Content-Disposition: attachment; filename=' . $res[ 1 ] );
			readfile( $res[ 0 ] );
			exit;
		}
		if ( true !== $res ) {
			exit( 'There was an error backing up db - ' . $res );
		}
		$res = self::files();
		exit( 'OK' );
	}
	protected static function mysql ( $public ) {
		$title = env( 'DB_DATABASE' ) . '_' . date( 'd' ) . '.sql.gz';
		$path = '/var/www/fitness/backups/' . $title;
		$cmd = "mysqldump --user=" . env( 'DB_USERNAME' ) . " --password=" . env( 'DB_PASSWORD' ) . " " . env( 'DB_DATABASE' ) . " | gzip > " . $path;
		$res = exec( $cmd );
		if ( $public ) {
			return [ $path , $title ];
		}
		return $res == '' ? true : $res;
	}
	protected static function files () {
		$path = '/var/www/fitness/backups/files_' . date( 'd' ) . '.gz';
		$path = '/var/www/fitness/backups/files.gz';
		$cmd = "tar --exclude='/var/www/fitness/vendor' --exclude='/var/www/fitness/.git' --exclude='/var/www/fitness/public' --exclude='/var/www/fitness/node_modules' --exclude='/var/www/fitness/storage' --absolute-names -cvf " . $path . " /var/www/fitness/ ";
		$res = exec( $cmd , $out , $oky );
		return true;
	}
}