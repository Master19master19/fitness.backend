<?php

namespace App\Helpers;
use App\BoughtSeat;
use App\Subscription;
use App\User;
use App\Hall;
use App\Profile;
use App\BoughtSubscription;
use App\Helpers\BitrixHelper;



class BitrixSendPaymentHelper {
    private static $template = [
        'class' => 'payment',
        'phone' => '',
        'items' => []
    ];
    private static $error = [];
    public static function send ( $orderId , $guid = null ) {
        if ( null == $guid ) {
            $guid = uniqid();
        }
        // return true;
        self::$template = [
            'class' => 'payment',
            'phone' => '',
            'guid' => '',
            'items' => []
        ];
        $boughtSeats = BoughtSeat::where([ 'order_id' => $orderId ]) -> get();
        if ( null !== $boughtSeats && count( $boughtSeats ) ) {
            self::handleSeats( $boughtSeats );
        }
        $boughtSubs = BoughtSubscription::where([ 'order_id' => $orderId ]) -> get();
        if ( null !== $boughtSubs && count( $boughtSubs ) ) {
            self::handleSubs( $boughtSubs );
        }
        // file_put_contents(__DIR__ . '/paymentSent' , json_encode( self::$template ) . PHP_EOL , FILE_APPEND );
        self::$template[ 'phone' ] = BitrixHelper::getPhone( self::$template[ 'phone' ] );
        self::$template[ 'guid' ] = $guid;
        // dd($orderId,$boughtSeats,self::$template);
        return BitrixHelper::send( self::$template , 'payment' );
    }
    private static function handleSubs ( $subs ) {
        foreach ( $subs as $key => $sub ) {
            $profile = Profile::whereId( $sub -> profile_id ) -> first();
            $subscription = Subscription::whereId( $sub -> subscription_id ) -> first();
            if ( null == $profile || null == $subscription ) {
                return true;
            }
            $row = [
                'itemType' => 'membership',
                'membershipType' => $subscription -> type,
                'profileName' => $profile -> name,
                'membershipId' => $subscription -> external_id,
                'count' => 1,
                'sum' => $sub -> price,
                'expirationTime' => date( 'Y-m-d\TH:i:s' , strtotime( $sub -> expiration_time ) )
            ];
            if ( $subscription -> type == 'kid' ) {
                $row[ 'birthday' ] = BitrixHelper::getBirthDay( $profile -> birthday );
            } else {
                $row[ 'phone' ] = BitrixHelper::getPhone( $profile -> phone );
            }
            self::$template[ 'items' ][] = $row;
            $user = User::whereId( $sub -> user_id ) -> firstOrFail();
            self::$template[ 'phone' ] = $user -> phone;
        }
        // dd(self::$template);
    }
    private static function handleSeats ( $seats ) {
        foreach ( $seats as $key => $seat ) {
            self::$template[ 'items' ][] = [
                'itemType' => 'rent',
                // 'courtName' => $court -> title,
                // 'profileName' => $user -> name,
                // 'phone' => $user -> phone,
                'courtId' => $seat -> hall_id,
                'sum' => $seat -> price,
                'startTime' =>  $seat -> date . "T" . $seat -> time . ":00",
                'finishTime' =>  $seat -> date . "T" . str_replace( ':00' , ':59' , $seat -> time ) . ":59",
            ];
            $user = User::whereId( $seat -> user_id ) -> firstOrFail();
            self::$template[ 'phone' ] = $user -> phone;
        }
    } 
}