<?php

namespace App\Helpers;
use App\Helpers\BitrixHelper;
use Auth;



class BitrixReserveHelper {
    private static $template = [
        'class' => 'rent',
        'newReserve' => [],
        'phone' => ''
    ];
    private static $error = [];
    public static function send ( $reservedSeats ) {
        // return true;
        foreach ( $reservedSeats as $key => $res ) {
            $startHour = (int) $res[ 'time' ];
            if ( $startHour < 10 ) $startHour = '0' . $startHour;
            $startTime = $res[ 'date' ] . "T" . $startHour . ":00:00";
            $finishTime = $res[ 'date' ] . "T" . $startHour . ":59:59";
            self::$template[ 'newReserve' ][] = [
                'courtId' => strval( $res[ 'hall_id' ] ),
                'startTime' => $startTime,
                'finishTime' => $finishTime,
            ];
        }
        self::$template[ 'phone' ] = BitrixHelper::getPhone( Auth::user() -> phone );
        // file_put_contents( __DIR__ . '/reserveSent' , json_encode( self::$template ) . PHP_EOL , FILE_APPEND );
        $res = BitrixHelper::send( self::$template , 'reserve' );
        // dd(self::$template);
        // file_put_contents( __DIR__ . '/reserveSentResp' , json_encode( $res ) . PHP_EOL , FILE_APPEND );

        return $res;
    }
    public static function fixHistory() {
        $reservedSeats = ReservedSeat::where( 'id' , '<' , '1650' ) -> where( 'chicken' , 0 ) -> take( 50 ) -> get();
        foreach ( $reservedSeats as $key => $reserve ) {
            $boughtSeat = BoughtSeat::where([
                'paid' => 1,
                'seat_id' => $reserve -> seat_id,
                'court_id' => $reserve -> court_id,
                'arena_id' => $reserve -> arena_id,
                'user_id' => $reserve -> user_id,
                'date' => $reserve -> date,
                'time' => $reserve -> time,
            ]) -> first();
            if ( null !== $boughtSeat ) {
                $user = User::whereId( $reserve -> user_id ) -> first();
                if ( null == $user ) {
                    file_put_contents( __DIR__ . '/newreserve_no_user' , json_encode( $reserve ) . PHP_EOL , FILE_APPEND );
                    continue;
                }
                $reservedSeats = [
                    // 'created_at' => $reserve ->,
                    'court_id' => $reserve -> court_id,
                    'time' => $reserve -> time,
                    'date' => $reserve -> date,
                ];
                $user = User::whereId( $bou[ 'user_id' ] ) -> first();
                if ( null == $user ) {
                    // dd($bou);
                    file_put_contents( __DIR__ . '/reserve_no_user' , json_encode( $bou ) . PHP_EOL , FILE_APPEND );
                    continue;
                }
                $phone = $user -> phone;
                // dd('asdsd');
                $res = self::sendSingle( $reservedSeats , $phone );
                // dd($reserve);
                // self::cancelReserve ( $reserve , $user );
                // dd(self::$template,$reserve);
            }
            $reserve -> chicked = 1;
            $reserve -> save();
        }
    }
    public static function sendSingle ( $reservedSeat , $phone ) {
        $startHour = (int) $reservedSeat[ 'time' ];
        if ( $startHour < 10 ) $startHour = '0' . $startHour;
        $startTime = $reservedSeat[ 'date' ] . "T" . $startHour . ":00:00";
        $finishTime = $reservedSeat[ 'date' ] . "T" . $startHour . ":59:59";
        self::$template[ 'newReserve' ][] = [
            'courtId' => strval( $reservedSeat[ 'court_id' ] ),
            'startTime' => $startTime,
            'finishTime' => $finishTime,
        ];
        self::$template[ 'phone' ] = BitrixHelper::getPhone( $phone );
        dd(self::$template);
        // file_put_contents( __DIR__ . '/reserveSent' , json_encode( self::$template ) . PHP_EOL , FILE_APPEND );
        $res = BitrixHelper::send( self::$template , 'reserve' );
        return $res;
    }
}