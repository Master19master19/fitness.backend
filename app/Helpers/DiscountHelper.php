<?php

namespace App\Helpers;
use App\User;
use App\BoughtSubscription;
use App\Subscription;
use App\Profile;
use Auth;
use App\Helpers\PushHelper;

class DiscountHelper {
    private static $usersNotified = [];
    private static $translations = [
            '',
            'Января',
            'Февраля',
            'Марта',
            'Апреля',
            'Мая',
            'Июня',
            'Июля',
            'Августа',
            'Сентября',
            'Октября',
            'Ноября',
            'Декабря'
        ];
    private static function getRudate( $date ) {
        $discountDate = date( 'Y-m-d' , strtotime( $date . " +5 days" ) );
        $enDate = date( 'd/m/Y' , strtotime( $discountDate ) );
        $xpl = explode( '/' , $enDate );
        $xpl[ 1 ] = self::$translations[ 0 + $xpl[ 1 ] ];
        return implode( ' ' , $xpl );
    }
    private static function getRudateAlt ( $datestr ) {
        $date = date( 'd/m/Y' , strtotime( $datestr ) );
        $translations = [
            '',
            'Января',
            'Февраля',
            'Марта',
            'Апреля',
            'Мая',
            'Июня',
            'Июля',
            'Августа',
            'Сентября',
            'Октября',
            'Ноября',
            'Декабря'
        ];
        $xpl = explode( '/' , $date );
        $xpl[ 1 ] = self::$translations[ 0 + $xpl[ 1 ] ];
        return implode( ' ' , $xpl ) . ' года';
    }
    private static function checkAndPush ( $sub , $user ) {
        if ( null !== $user ) {
            if ( in_array( $user -> id , self::$usersNotified ) ) {

            } else {
                if ( null !== $sub -> subscription && null !== $sub -> profile ) {
                    $data = [];
                    $data[ 'title' ] = "Акция";
                    // $data[ 'body' ] = "До " . self::getRudate( $sub -> expiration_time . " +5 days" ) . " продлите свой абонемент \"" . $sub -> subscription -> title . "\" для пользователя \"" . $sub -> profile -> name . "\" за " . ( $sub -> subscription -> price - 1000 ) . " рублей";
                    // $data[ 'body' ] = "До " . self::getRudate( $sub -> expiration_time ) . " продлите свой абонемент \"" . $sub -> subscription -> title . "\" для пользователя \"" . $sub -> profile -> name . "\" со скидкой 1000 рублей";
                    // dd($data['body']);
                    $data[ 'body' ] = "Сегодня последний день действия Вашего абонемента - продление в течение 3х дней со скидкой!";
                    PushHelper::push( $user -> id , $data );
                }
            }
            // var_dump($sub->id);
            self::$usersNotified[] = $user -> id;
            $sub -> user_notified = 1;
            $sub -> pushed_at = date( 'Y-m-d H:i:s' );
            // var_dump(self::$usersNotified);
            $sub -> save();
        } else {
            $sub -> pushed_at = date( 'Y-m-d H:i:s' );
            $sub -> user_notified = -1;
            $sub -> save();
        }
    }
    public static function check () {

        $boughtSubscriptions = BoughtSubscription::where( 'user_notified' , 0 ) -> wherePaid( 1 ) -> whereDate( 'expiration_time' , '=' , date( 'Y-m-d' ) ) -> get();
        foreach ( $boughtSubscriptions as $key => $sub ) {
            $user = User::whereId( $sub -> user_id ) -> whereNotNull( 'expo_token' ) -> whereRaw( 'LENGTH( `expo_token` ) > 15' ) -> first();
           self::checkAndPush( $sub , $user );
        }
    }


    public static function handleSubscriptions ( $subscriptions , $orderId = null , $isPayment = false ) {
        $response = [];
        $totalPrice = 0;
        foreach ( $subscriptions as $subscriptionId => $data ) {
            $subscriptionRecord = Subscription::whereId( $subscriptionId ) -> firstOrFail();
            $discountAmount = 0;
            $response[ $subscriptionId ] = [ 'price' => 0 , 'totalDiscount' => 0 ];
            foreach ( $data[ 'profiles' ] as $profile ) {
                $price = $subscriptionRecord -> price;
                $whereData = [
                    'user_id' => Auth::user() -> id,
                    'type' => $subscriptionRecord[ 'type' ]
                ];
                if ( $subscriptionRecord[ 'type' ] == 'kid' ) {
                    $whereData[ 'name' ] = $profile[ 'name' ];
                    $whereData[ 'birthday' ] = $profile[ 'secondaryText' ];
                } else {
                    $whereData[ 'phone' ] = $profile[ 'secondaryText' ];
                }
                $checkProfile = Profile::where( $whereData ) -> first();
                if ( null !== $checkProfile ) {
                    $checkProfile -> deleted = 0;
                    $checkProfile -> save();
                    $profileId = $checkProfile -> id;
                } else {
                    $whereData[ 'name' ] = $profile[ 'name' ];
                    $profile = Profile::create( $whereData );
                    $profileId = $profile -> id;
                }
                $checkExistsData = [
                    'profile_id' => $profileId,
                    'subscription_id' => $subscriptionId,
                    'user_id' => Auth::user() -> id,
                    'paid' => 1
                ];
                // checking if 15 minutes passed from last order 
                if ( $isPayment ) {
                    $checkIfTimePassedData = [
                        'profile_id' => $profileId,
                        'subscription_id' => $subscriptionId,
                        'user_id' => Auth::user() -> id,
                    ];
                    $checkIfTimePassed = BoughtSubscription::where( $checkIfTimePassedData ) -> where( 'created_at' , '>' , date( 'Y-m-d H:i:s' , strtotime( '-15 minutes' ) ) ) -> first();
                    if ( null !== $checkIfTimePassed ) {
                        return "Убедитесь, что Вы только что не оплачивали данный заказ.
Если Вы уже оплатили заказ, то в течение 15 минут купленные абонементы отобразятся в приложении у Вас в профиле.";
                    }
                }
                // checking if 15 minutes passed from last order 
                // checking if there is an active subscription
                if ( $isPayment ) {
                    $checkIfExistsActiveSubscription = BoughtSubscription::where( $checkExistsData ) -> where( 'expiration_time' , '>' , date( 'Y-m-d' , strtotime( '+1 day' ) ) ) -> first();
                    if ( $checkIfExistsActiveSubscription !== null ) {
                        return "Для профиля " . $profile[ 'name' ] . " активен абонемент до " . self::getRudateAlt( $checkIfExistsActiveSubscription -> expiration_time );
                    }
                }
                // checking if there is an active subscription

                $checkExists = BoughtSubscription::where( $checkExistsData ) -> get();
                $discount = 0;
                if ( null !== $checkExists ) {
                    foreach ( $checkExists as $soup ) {
                        if ( $soup -> has_discount ) {
                            $price = $price - 1000;
                            $discount = 1;
                            $discountAmount = 1000;
                            break;
                        }
                    }   
                }
                $expiration_time = self::getSubscriptionExpirationTime( $checkExistsData , $subscriptionRecord -> expiration_days );
                if ( null !== $orderId ) {
                    $sub = [
                        'order_id' => $orderId,
                        'profile_id' => $profileId,
                        'price' => $price,
                        'subscription_id' => $subscriptionId,
                        'user_id' => Auth::user() -> id,
                        'quantity' => 1,
                        'discount' => $discount,
                        'created_at' => date( 'Y-m-d H:i:s' ),
                        'expiration_time' => $expiration_time,
                        'updated_at' => date( 'Y-m-d H:i:s' ),
                    ];
                    $subRecord = BoughtSubscription::create( $sub );
                } else {
                    $response[ $subscriptionId ][ 'price' ] += ( float ) $price;
                    $response[ $subscriptionId ][ 'totalDiscount' ] += $discountAmount;
                }
                $totalPrice += ( float ) $price;
            }
        }
        if ( null == $orderId ) {
            return $response;
        }
        return $totalPrice;
    }



    protected static function getSubscriptionExpirationTime ( $data , $expiration_days ) {
        $oldSub = BoughtSubscription::where( $data ) -> orderBy( 'expiration_time' , 'DESC' ) -> first();
        if ( null == $oldSub ) {
            return date( 'Y-m-d H:i:s' , strtotime( '+' . $expiration_days . ' days' ) );
        } else if ( strtotime( $oldSub -> expiration_time ) < time() ) {
            return date( 'Y-m-d H:i:s' , strtotime( '+' . $expiration_days . ' days' ) );
        } else {
            return date( 'Y-m-d H:i:s' , strtotime( $oldSub -> expiration_time . ' +' . $expiration_days . ' days' ) );
        }
    }
}