<?php

namespace App\Helpers;
use Mailgun\Mailgun;

class Mail {
	protected static function getAdminMail () {
		$to = \App\AppInfo::where( 'key' , 'notification_emails' ) -> first()[ 'value' ];
		$xpl = explode( ',' , $to );
		$response = [];
		if ( count( $xpl ) > 1 ) {
			$response[ 0 ] = array_shift ( $xpl );
			$response[ 1 ] = $xpl;
		} else if ( count( $xpl ) == 1 ) {
			$response[ 0 ] = array_shift ( $xpl );
			$response[ 1 ] = [];

		} else {
			$response = [ $to , [] ];
		}
		return $response;
	}
	public static function sendMail ( $title , $desc , $to ) {
		$data = [
			'title' => $title ,
			'desc' => $desc
		];
		$res = \Mail::send( 'mail.send' , $data , function( $message ) use ( $to ) {
			$message -> to( $to );
			$message -> subject( 'Fitness.ru' );
		});
		return $res;
	}
	public static function sendToAdmin ( $data ) {
		$title = "Новая оплата";
		$data = [
			'title' => $title ,
			'data' => $data
		];
		// $message = view( 'mail.admin' ,  $data );
		// $message = $message -> render();
		// exit($message);
		$res = \Mail::send( 'mail.admin' , $data , function( $message ) {
			$emails = self::getAdminMail();
			$message -> to( $emails[ 0 ] );
			$message -> cc( $emails[ 1 ] );
			$message -> subject( 'Fitness.ru' );
		});
		return $res;
	}
	public static function sendFeedBackToAdmin ( $title , $info ) {
		$to = \App\AppInfo::where( 'key' , 'notification_emails' ) -> first()[ 'value' ];

		$data = [
			'title' => $title ,
			'name' => $info[ 'name' ],
			'email' => $info[ 'email' ],
			'phone' => $info[ 'phone' ],
			'messagee' => $info[ 'message' ],
		];
		$res = \Mail::send( 'mail.feedback' , $data , function( $message ) {
			$emails = self::getAdminMail();
			$message -> to( $emails[ 0 ] );
			$message -> cc( $emails[ 1 ] );
			$message -> subject( 'Fitness.ru' );
		});
		return $res;
	}
	public static function sendpersonalChildTrainingOrderToAdmin ( $info ) {
		$title = 'Новая заявка для школы тенниса';
		$to = \App\AppInfo::where( 'key' , 'notification_emails' ) -> first()[ 'value' ];
		$data = [
			'title' => $title ,
			'child_name' => $info[ 'child_name' ],
			'phone_number' => $info[ 'phone_number' ],
			'parent_name' => $info[ 'parent_name' ],
			'child_birthday' => $info[ 'child_birthday' ],
			'additional' => $info[ 'additional' ],
			'level' => $info[ 'level' ],
		];
		$res = \Mail::send( 'mail.personalTrainingChild' , $data , function( $message ) {
			$emails = self::getAdminMail();
			$message -> to( $emails[ 0 ] );
			$message -> cc( $emails[ 1 ] );
			$message -> subject( 'Fitness.ru' );
		});
		return $res;
	}
	public static function sendpersonalTrainingOrder ( $info ) {
		$title = 'Новая заявка для школы тенниса';
		$to = \App\AppInfo::where( 'key' , 'notification_emails' ) -> first()[ 'value' ];
		$data = [
			'title' => $title ,
			'name' => $info[ 'name' ],
			'phone' => $info[ 'phone' ],
			'email' => $info[ 'email' ],
			'additional' => $info[ 'additional' ],
		];
		$res = \Mail::send( 'mail.personalTraining' , $data , function( $message ) {
			$emails = self::getAdminMail();
			$message -> to( $emails[ 0 ] );
			$message -> cc( $emails[ 1 ] );
			// var_dump($emails);
			// exit;
			$message -> subject( 'Fitness.ru' );
		});
		return $res;
	}
}