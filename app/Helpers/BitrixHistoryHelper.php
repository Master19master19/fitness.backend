<?php

namespace App\Helpers;
use \GuzzleHttp\Exception\RequestException;
use App\BitrixHistory;
use App\Helpers\BitrixHelper;


class BitrixHistoryHelper {
	public static function createHistory( $data , $message ) {
		$fill = [
			'data' => json_encode( $data ),
			'error' => $message
		];
		BitrixHistory::create( $fill );
	}
	public static function clearHistory() {
        if ( env( 'BITRIX_DOWN' ) == true ) {
			return;
		}
		$res = BitrixHistory::where( 'done' , 0 ) -> whereRaw( "DATE_ADD(created_at,INTERVAL 2 MINUTE ) < DATE_ADD(NOW(),INTERVAL 2 HOUR )" ) -> take( 5 ) -> get();
		foreach ( $res as $record ) {
			$data = $record -> data;
			if ( strpos( $data , 'membershipType' ) !== false && $record -> tries >= 0 ) {
				// BitrixHistory::whereId( $record -> id ) -> update([ 'done' => 1 ]);
				// return;
			}

			$dat = json_decode( $data , 1 );
			$res = BitrixHelper::send( $dat , 'checking' );
			if ( $res === true ) {
				BitrixHistory::whereId( $record -> id ) -> update([ 'done' => 1 ]);
			} else {
				BitrixHistory::whereId( $record -> id ) -> update([ 'tries' => $record -> tries + 1 , 'error' => $res ]);
			}
            // usleep( 250000 );
            sleep( 3 );
		}
	}

}