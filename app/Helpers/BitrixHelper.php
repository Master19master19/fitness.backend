<?php
namespace App\Helpers;
use \GuzzleHttp\Exception\RequestException;
use App\Helpers\LogHelper;
use App\Helpers\BitrixHistoryHelper;

class BitrixHelper {

	public static function send ( $data , $class = 'not_needed' , $test = false ) {
		$startTime = time();
		$data[ 'changeDate' ] = date( "Y-m-d\TH:i:s" );
		if ( $class == 'checking' ) {
			return self::sendChecking( $data , $class );
			return true;
		}

        if ( env( 'BITRIX_DOWN' ) == true ) {
			BitrixHistoryHelper::createHistory( $data , 'virus' );
			return true;
		}
		// BitrixHistoryHelper::clearHistory();
        $client = new \GuzzleHttp\Client([
			// 'headers' => ['content-type' => 'application/json', 'Accept' => 'application/json', 'charset' => 'win-1251']
		]);
		$timeout = 5;
		if ( $class == 'payment' ) {
			$timeout = 3;
		}
		if ( $class == 'reserve' ) {
			$timeout = 7;
		}
        try {
            usleep( 50000 );
            if ( $test === true ) {
		        $response = $client -> request( 'POST' , env( 'BITRIX_URL_TEST' ) , [
		        	'timeout' => $timeout,
		            'json' => $data,
					'auth' => [
						env( 'BITRIX_USERNAME' ),
						env( 'BITRIX_PASSWORD' )
					]
		        ]);
		        return $response;
            } else {
		        $response = $client -> request( 'POST' , env( 'BITRIX_URL' ) , [
		        	'timeout' => $timeout,
		            'json' => $data,
					'auth' => [
						env( 'BITRIX_USERNAME' ),
						env( 'BITRIX_PASSWORD' )
					]
		        ]);
            }
	        LogHelper::bitrix_out( $data , time() - $startTime );
	        return true;
        } catch ( RequestException $e ) {
        	$message = iconv( "windows-1251" , "utf-8" , $e -> getMessage() );
        	if ( $test === true ) {
        		return $message;
        	}
        	$code = $e -> getCode();
        	LogHelper::bitrix_out_error( $data , $e  , '' , time() - $startTime);
        	if ( $code == 500 ) {
        		// file_put_contents( __DIR__ . '/critical.json' , date( 'Y-m-d H:i:s' ) . $message . PHP_EOL . json_encode( $data ) . PHP_EOL , FILE_APPEND );
        		return false;
        	}
			if ( $class == 'reserve' ) {
        		// file_put_contents( __DIR__ . '/bitrix.reserve.json' , $message . PHP_EOL , FILE_APPEND );
				return false;
			} else {
        		// file_put_contents( __DIR__ . '/bitrix.err.json' , $message . PHP_EOL , FILE_APPEND );
				BitrixHistoryHelper::createHistory( $data , $message );
				return true;
			}
        }
        return true;
	}


	protected static function sendChecking ( $data , $class = 'not_needed' ) {
		$startTime = time();
        $client = new \GuzzleHttp\Client();
        try {
            usleep( 50000 );
	        $response = $client -> request( 'POST' , env( 'BITRIX_URL' ) , [
	        	'timeout' => 5,
	            'json' => $data,
				'auth' => [
					env( 'BITRIX_USERNAME' ),
					env( 'BITRIX_PASSWORD' )
				]
	        ]);
        	return true;
        } catch ( RequestException $e ) {
        	$message = iconv( "windows-1251" , "utf-8" , $e -> getMessage() );
        	$code = $e -> getCode();
        	if ( $code == 500 ) {
        		LogHelper::bitrix_out_error( $data , $e , 'checking' , time() - $startTime );
        		return true;
        	}
        	return $message;
        }
	}


	// USERS
	protected static function getName ( $namee , $getSurName = false ) {
		$name = '';
		$surName = '';
		$xpl = explode( ' ' , $namee );
		if ( count( $xpl ) == 1 ) {
			$name = $xpl[ 0 ];
		} else {
			$name = $xpl[ 1 ];
			$surName = $xpl[ 0 ];
		}
		if ( $getSurName ) return $surName;
		return $name;
	}
	public static function createUser ( $userArr ) {
		$user = [
			'class' => 'partner',
			'name' => self::getName( $userArr[ 'name' ] ),
			'surName' => self::getName( $userArr[ 'name' ] , 1 ),
			'phone' => self::getPhone( $userArr[ 'phone' ] ) ,
			// 'phone' => $userArr[ 'phone' ],
			'email' => $userArr[ 'email' ] ?? '',
			'changeDate' => date( "Y-m-d\TH:i:s" ),
		];
        // file_put_contents( __DIR__ . '/userrrrr.json' , json_encode( $user ) . PHP_EOL , FILE_APPEND );
        // dd($user);
		return self::send( $user );
	}
	public static function getPhone ( $number ) {
		$response = str_replace( '-' , '' , $number );
		$response = str_replace( ')' , '' , $response );
		$response = str_replace( '(' , '' , $response );
		$response = str_replace( ' ' , '' , $response );
		$response = str_replace( ' ' , '' , $response );
		$response = str_replace( ' ' , '' , $response );
		return $response;
	}
	public static function getBirthDay ( $birth ) {
		// 30-12-2000 date( "Y-m-d\TH:i:s" )
		$xpl = explode( '-' , $birth );
		if ( count( $xpl ) != 3 ) {
			return $birth;
		}
		$y = $xpl[ 2 ];
		$m = $xpl[ 1 ];
		$d = $xpl[ 0 ];
		$res = "{$y}-{$m}-{$d}T00:00:00";
		if ( date( 'Y-m' , strtotime( $res ) ) === '1970-01' ) {
			$res = "2010-01-01T00:00:00";
		}
		return $res;
	}

	public static function updateUser ( $userObject , $oldPhone = null ) {
		$user = [
			'class' => 'partner',
			'name' => self::getName( $userObject -> name ),
			'surName' => self::getName( $userObject -> name , 1 ),
			'phone' => self::getPhone( $userObject -> phone ) ,
			'email' => $userObject -> email ?? '',
			'changeDate' => date( "Y-m-d\TH:i:s" ),
		];
		if ( $oldPhone ) {
			$user[ 'oldPhone' ] = self::getPhone( $oldPhone );
		}
		$res = self::send( $user );
        // file_put_contents( __DIR__ . '/userUpdate.json' , json_encode( $user ) . PHP_EOL , FILE_APPEND );
        return $res;
	}
	// USERS







	// SUBSCRIPTIONS
	public static function updateSubscription ( $subscription ) {
		$data = [
			'id' => $subscription -> id,
			'type' => 'membership',
			'name' => $subscription -> title,
			'price' => $subscription -> price
		];
        file_put_contents( __DIR__ . '/subscription.json' , json_encode( $data ) . PHP_EOL , FILE_APPEND );
	}
	// SUBSCRIPTIONS

}