<?php

namespace App\Helpers;
use App\User;
use App\Ban;
// {"class":"partner","email":"vmxitaryan@gmail.comyuy","phone":"+73636363636","name":"Sixxxx","surname":"Testing"}
// {"class":"partner","email":"Karpov-igor@mail.ru","phone":"+79106451383","banned":false,"name":"\u0418\u0433\u043e\u0440\u044c","surname":"\u041a\u0430\u0440\u043f\u043e\u0432"}

class BitrixUserHelper {
    private static function getPhone ( $number ) {
        $xpl = str_split ( $number );
        if ( ! isset( $xpl[ 2 ] ) ) return '000000000000';
        $newNum = '+7' . " (" . $xpl[ 2 ] . $xpl[ 3 ] . $xpl[ 4 ] . ") " . $xpl[ 5 ] . $xpl[ 6 ] . $xpl[ 7 ] . "-" . $xpl[ 8 ] . $xpl[ 9 ] . "-" . $xpl[ 10 ] . $xpl[ 11 ];
        return $newNum;
    }
    protected static function handleBan ( $data ) {
        $check = Ban::wherePhone( $data[ 'phonee' ] ) -> first();
        if ( $data[ 'banned' ] == 'true' ) {
            if ( ! $check ) {
                $ban = new Ban;
                $ban -> phone = $data[ 'phonee' ];
                $ban -> save();
            }
        } else {
            if ( $check ) {
                Ban::wherePhone( $data[ 'phonee' ] ) -> delete();
            }
        }
    }
	public static function handleUser ( $data ) {
        $data[ 'phonee' ] = $data[ 'phone' ];
        $data[ 'phone' ] = self::getPhone( $data[ 'phone' ] );
        $phone = $data[ 'phone' ];
        if ( isset( $data[ 'oldPhone' ] ) ) {
            $data[ 'oldPhone' ] = self::getPhone( $data[ 'oldPhone' ] );
            $user = User::wherePhone( $data[ 'oldPhone' ] ) -> first();
            if ( null == $user ) {
                self::handleBan( $data );
                return [ 'result' => true ];
            } else {
                if ( null != $data[ 'email' ] ) {
                    $user -> email = $data[ 'email' ];
                }
                $user -> phone = $data[ 'phone' ];
                $user -> name = $data[ 'name' ] . ' ' . $data[ 'surname' ];
                $user -> save();
                self::handleBan( $data );
                return [ 'result' => true ];
            }
        } else {
            $user = User::wherePhone( $data[ 'phone' ] ) -> first();
            $userEmail = User::whereEmail( $data[ 'email' ] ) -> first();
            if ( null !== $userEmail ) {
                $data[ 'email' ] = $data[ 'email' ] . uniqid();
                // file_put_contents(__DIR__.'/em', json_encode($data).PHP_EOL,FILE_APPEND);
                // return [
                //     'result' => false,
                //     'errorCode' => 500,
                //     'error' => "Пользователь с почтой " . $data[ 'email' ] . " уже существует"
                // ];
            }
            if ( null == $user ) {
                return [ 'result' => true ];
            } else {
                if ( null != $data[ 'email' ] ) {
                    $user -> email = $data[ 'email' ];
                }
                $user -> phone = $data[ 'phone' ];
                $user -> name = $data[ 'name' ] . ' ' . $data[ 'surname' ];
                $user -> save();
                self::handleBan( $data );
                return [ 'result' => true ];
            }
        }
	}
}