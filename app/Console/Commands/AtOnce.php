<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


class AtOnce extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tennis:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this -> call( 'bitrix:reserve' );
        $this -> call( 'bitrix:history' );
        $this -> call( 'tennis:discount' );
        if ( date( 'i' ) == 30 ) {
            $this -> call( 'tennis:backup' );
        }
    }
}
