<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/' , function () {
    return redirect( 'login' );
});

Auth::routes();

Route::group([ 'middleware' => [ 'auth' , 'web' , 'Admin' , 'App\Http\Middleware\Active' ] ] , function () {
	Route::get( 'backup' , 'BackupController@index' );
	Route::get( 'stats' , 'StatisticController@index' );
	// RESOURCES
	Route::resources([
		// 'orders' => 'OrderController',
		'tickets' => 'TicketController',
		'services' => 'ServiceController',
		'types' => 'TypeController',
		'categories' => 'CategoryController',
		'qualities' => 'QualityController',
		'users' => 'UserController',
		'arenas' => 'ArenaController',
		'courts' => 'CourtController',
		'seats' => 'SeatController',
		'subscriptions' => 'SubscriptionController',
		'announcements' => 'TournamentAnnoucementController',
		'tourcategories' => 'TournamentCategoryController',
		'trainers' => 'TrainerController',
		'news' => 'NewsController',
	]);
	// RESOURCES
	// DESTROYS
	Route::get( 'orders/destroy/{id}' , 'OrderController@destroy' ) -> name( 'orders.destroy' );
	Route::get( 'services/destroy/{id}' , 'ServiceController@destroy' ) -> name( 'services.destroy' );
	Route::get( 'types/destroy/{id}' , 'TypeController@destroy' ) -> name( 'types.destroy' );
	Route::get( 'categories/destroy/{id}' , 'CategoryController@destroy' ) -> name( 'categories.destroy' );
	Route::get( 'qualities/destroy/{id}' , 'QualityController@destroy' ) -> name( 'qualities.destroy' );
	Route::get( 'users/destroy/{id}' , 'UserController@destroy' ) -> name( 'users.destroy' );
	Route::get( 'arenas/destroy/{id}' , 'ArenaController@destroy' ) -> name( 'arenas.destroy' );
	Route::get( 'courts/destroy/{id}' , 'CourtController@destroy' ) -> name( 'courts.destroy' );
	Route::get( 'seats/destroy/{id}' , 'SeatController@destroy' ) -> name( 'seats.destroy' );
	Route::get( 'subscriptions/destroy/{id}' , 'SubscriptionController@destroy' ) -> name( 'subscriptions.destroy' );
	Route::get( 'announcements/destroy/{id}' , 'TournamentAnnoucementController@destroy' ) -> name( 'announcements.destroy' );
	Route::get( 'tourcategories/destroy/{id}' , 'TournamentCategoryController@destroy' ) -> name( 'tourcategories.destroy' );
	Route::get( 'trainers/destroy/{id}' , 'TrainerController@destroy' ) -> name( 'trainers.destroy' );
	Route::get( 'news/destroy/{id}' , 'NewsController@destroy' ) -> name( 'news.destroy' );
	// DESTROYS
	// OTHER
	Route::post( 'tickets.answer/{id}' , 'TicketController@answer' ) -> name( 'tickets.answer' );
	Route::get( 'editor' , 'CourtController@editor' ) -> name( 'editor' );
	// Route::get( 'boughtSeats' , 'BoughtSeatController@index' ) -> name( 'boughtSeats.index' );
	// Route::get( 'boughtSeats/{date}' , 'BoughtSeatController@index' ) -> name( 'boughtSeats' );
	Route::get( 'app_info' , 'AppInfoController@index' ) -> name( 'app.info' );
	Route::get( 'app_cards' , 'AppInfoController@cards' ) -> name( 'app.cards' );
	Route::post( 'app_info_post' , 'AppInfoController@store' ) -> name( 'app.info.post' );
	Route::get( 'home' , function () {
		return redirect() -> route( 'app.info' );
	});
	// OTHER
});
// Route::get( 'payee' , 'TestController@payee' );
// Route::get( 'mutate' , 'TestController@mutate' );
// Route::get( 'bug/users' , 'BugController@users' );
// Route::get( 'bug/reserves' , 'BugController@reserves' );
// Route::get( 'bug/canceled' , 'BugController@canceled' );
// Route::get( 'bug/reserved' , 'BugController@reserved' );
// Route::get( 'bug/paid' , 'BugController@paid' );
// Route::get( 'bug/compareHistory' , 'BugController@compareHistory' );
// Route::get( 'bug/test' , 'BugController@test' );
// Route::get( 'bug/checkWrongCourts' , 'BugController@checkWrongCourts' );
// Route::get( 'bug/cancelReserveByOrder/{orderId}' , 'BugController@cancelReserveByOrder' );
// Route::get( 'bug/getPaymentInfo/{orderId}' , 'BugController@getPaymentInfo' );
// Route::get( 'bug/cancelCheck' , 'BugController@cancelCheck' );
// Route::get( 'payStats' , 'BugController@payStats' );
// Route::get( 'push' , 'BugController@push' );
// Route::get( 'discount' , 'BugController@discount' );
// Route::get( 'testNotification' , 'TestController@testNotification' );
// Route::get( 'bug/sendMofo' , 'BugController@sendMofo' );
// Route::get( 'bug/someMailSend/{orderId}' , 'BugController@someMailSend' );
Route::get( 'bug/getMyUserTests' , 'BugController@getMyUserTests' );
// Route::get( 'bug/sendByOrderId/{orderId}' , 'BugController@sendByOrderId' );
