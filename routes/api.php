<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get( 'sms' , 'TestController@smss' );
// Route::put( '/testDating' , function (Request $request) {
//     dd($request->all());
// });

// Route::get( 'news/{limit}' , 'Api\NewsController@index' );
// Route::get( 'news/{id}' , 'Api\NewsController@show' );
// Route::get( 'arena/{date}' , 'Api\CourtController@index' );
// Route::post( 'personalTrainingOrder' , 'Api\SchoolController@personalTrainingOrder' );
// Route::post( 'personalChildTrainingOrder' , 'Api\SchoolController@personalChildTrainingOrder' );
// Route::post( 'editor' , 'CourtController@editorPost' ) -> name( 'editor.post' );
// Route::post( 'boughtSeats' , 'CourtController@boughtSeatsPost' ) -> name( 'boughtSeats.post' );


Route::get( 'tmail/{id}' , 'Api\PaymentController@testMail' );

// Route::group(['middleware' => 'auth:api'], function(){
// Route::post('details', 'API\UserController@details');


// Route::get( 'trainers' , 'Api\OtherController@trainers' );
// Route::get( 'announcements' , 'TournamentAnnoucementController@showAll' );
// Route::get( 'announcements/{id}' , 'TournamentAnnoucementController@show' );





// Route::get( '1c.courts' , 'Api\BitrixController@courts' );


// Route::get( 'anonymousProfiles' , 'Api\ProfileController@anonymousProfiles' );
// Route::get( 'anonymousProfiles/{type}' , 'Api\ProfileController@anonymousProfiles' );



// Route::group([ 'middleware' => [ 'auth:api' ] ] , function () {
	// Route::post( 'resetPasswordAuth' , 'Api\UserController@resetPasswordAuth' );
	// Route::get( 'balance' , 'Api\UserController@balance' );
	// Route::post( 'help' , 'Api\UserController@help' );
	// Route::post( 'balance' , 'Api\UserController@requestBalance' );
	// Route::get( 'refCode' , 'Api\UserController@refCode' );
	// Route::get( 'services' , 'Api\OtherController@services' );
	// Route::get( 'service/{categoryId}/{typeId}' , 'Api\OtherController@getService' );
	// Route::get( 'typesAndCats' , 'Api\OtherController@typesAndCats' );
	// Route::get( 'categories' , 'Api\OtherController@categories' );
	// Route::get( 'orders' , 'Api\OtherController@orders' );
	// Route::post( 'registerOrder' , 'Api\OtherController@registerOrder' );
	// Route::get( 'referral' , 'Api\ReferralController@referral' );
	// Route::get( 'catServices/{categoryId}' , 'Api\OtherController@getCatServices' );
	// Route::get( 'seatsHistory' , 'Api\OtherController@seatsHistory' );
	// Route::get( 'subscriptionsHistory' , 'Api\OtherController@subscriptionsHistory' );
// });




// GUEST

// AUTHENTICATION
Route::group([ 'middleware' => [ 'ban' ] ] , function () {
	Route::post( 'register' , 'Api\UserController@register' );
	Route::post( 'login' , 'Api\UserController@login' );
	Route::post( 'resetPassword' , 'Api\UserController@resetPassword' );
	Route::post( 'checkForgotPasswordCode' , 'Api\UserController@checkForgotPasswordCode' );
	Route::post( 'storeResetPassword' , 'Api\UserController@storeResetPassword' );
	Route::post( 'requestCode' , 'Api\UserController@requestCode' );
	Route::post( 'checkCode' , 'Api\UserController@checkCode' );
});
// AUTHENTICATION
Route::get( 'subscriptions' , 'Api\OtherController@getSubscriptions' );
Route::get( 'subscriptions/new' , 'Api\OtherController@getSubscriptionsNewVersion' );
Route::post( 'feedback' , 'Api\OtherController@feedback' );
Route::get( 'rules' , 'Api\OtherController@rules' );
Route::get( 'terms' , 'Api\OtherController@terms' );
// Route::get( 'mobileMap' , 'Api\OtherController@mobileMap' );
Route::get( 'appInfo' , 'AppInfoController@show' );
Route::get( 'getHall/{hallId}/{date}' , 'Api\HallController@index' );
Route::get( 'getHalls' , 'Api\HallController@getHalls' );
Route::get( 'getHallMainImages' , 'Api\HallController@getHallMainImages' );
Route::get( 'getHallImages/{hallId}' , 'Api\HallController@getHallImages' );
Route::get( 'getClubImages' , 'Api\HallController@getClubImages' );


// GUEST


// AUTH


Route::group([ 'middleware' => [ 'auth:api' , 'ban' ] ] , function () {

	// Route::post( 'settings/requestEmailChangeCode' , 'Api\SettingController@requestEmailChangeCode' );
	// Route::post( 'settings/checkEmailChangeCode' , 'Api\SettingController@checkEmailChangeCode' );

	Route::get( 'user' , 'Api\OtherController@getUser' );
	Route::get( 'profiles/{type}' , 'Api\ProfileController@getProfiles' );
	Route::post( 'profiles/create/{type}' , 'Api\ProfileController@createProfile' );
	Route::post( 'profiles/delete' , 'Api\ProfileController@deleteProfile' );
	Route::post( 'settings/name' , 'Api\SettingController@updateName' );
	Route::post( 'settings/email' , 'Api\SettingController@updateEmail' );
	Route::post( 'settings/phone' , 'Api\SettingController@updatePhone' );
	Route::post( 'settings/password' , 'Api\SettingController@updatePassword' );
	Route::get( 'profile/hallsHistory' , 'Api\ProfileController@hallsHistory' );
	Route::get( 'profile/purchaseHistory' , 'Api\ProfileController@purchaseHistory' );
	Route::get( 'profile/subscriptionsHistory' , 'Api\ProfileController@subscriptionsHistory' );
	Route::get( 'profile/cards' , 'Api\ProfileController@cards' );
	Route::get( 'profile/cards/remove/{cardId}' , 'Api\ProfileController@removeCard' );
	Route::get( 'profile/addCard' , 'Api\PaymentController@addCard' );
	Route::post( 'payCart' , 'Api\PaymentController@payCart' );
	Route::post( 'setExpoPushToken' , 'Api\OtherController@setExpoPushToken' );
	Route::get( 'canBuyKidSubscription' , 'Api\OtherController@canBuyKidSubscription' );
	Route::post( 'hasDiscountSubscription' , 'Api\OtherController@hasDiscountSubscription' );
	Route::post( 'logPaymentUrls' , 'Api\OtherController@logPaymentUrls' );
	Route::get( 'needCleanCart' , 'Api\OtherController@needCleanCart' );
	Route::get( 'clearCleanCart' , 'Api\OtherController@clearCleanCart' );
});


// AUTH


Route::get( 'pay/{confirmation_token}/isCard' , 'Api\PaymentController@showPaymentCard' );
Route::get( 'pay/{confirmation_token}' , 'Api\PaymentController@showPayment' );
Route::any( 'payment' , 'Api\PaymentController@YAPayment' );
Route::post( '1c' , 'Api\BitrixController@inhale' );
Route::get( 'paymentConfirm' , 'Api\PaymentController@loading' );
Route::get( 'paymentConfirmRedir' , 'TestController@paymentConfirmRedir' );
Route::get( 'test' , 'TestController@payee' );
Route::get( 'testSub' , 'TestController@testSub' );
Route::get( 'testPay' , 'TestController@testPay' );
Route::get( 'redoBitrixHistory' , 'TestController@redoBitrixHistory' );
Route::get( '/applePay' , 'TestController@applePay' );
// Route::get( 'sendFifteenPush' , 'TestController@sendFifteenPush' );